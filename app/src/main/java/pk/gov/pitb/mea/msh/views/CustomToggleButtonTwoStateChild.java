package pk.gov.pitb.mea.msh.views;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TableRow;
import android.widget.ToggleButton;

public class CustomToggleButtonTwoStateChild extends ToggleButton implements CompoundButton.OnCheckedChangeListener, View.OnLongClickListener {

	private int colorBackgroundEmpty = 0;
	private int colorBackgroundChecked = 0;
	private int colorBackgroundUnChecked = 0;

	public CustomToggleButtonTwoStateChild(boolean bIsOdd) {
		super(MainContainer.mContext);
		this.colorBackgroundEmpty = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(R.color.row_body_background_even);
		this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even;
		this.colorBackgroundUnChecked = bIsOdd ? R.drawable.toggle_cross_odd : R.drawable.toggle_cross_even;
	}

	public void customiseToggleButton(TableRow.LayoutParams paramsToggleButton) {
		try {
			setLayoutParams(paramsToggleButton);
			setBackgroundColor(colorBackgroundEmpty);
			setTextOn("");
			setTextOff("");
			setText("");
			setOnCheckedChangeListener(this);
			setOnLongClickListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		try {
			if (isChecked) {
				setBackgroundResource(colorBackgroundChecked);
			} else {
				setBackgroundResource(colorBackgroundUnChecked);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onLongClick(View v) {
		try {
			setChecked(false);
			setBackgroundResource(0);
			setBackgroundColor(colorBackgroundEmpty);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		try {
			super.setEnabled(enabled);
			if (!enabled) {
				setChecked(false);
				setBackgroundResource(0);
				setBackgroundColor(colorBackgroundEmpty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}