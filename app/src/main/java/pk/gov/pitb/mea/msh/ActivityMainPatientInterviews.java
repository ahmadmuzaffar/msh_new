package pk.gov.pitb.mea.msh;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSendData;
import pk.gov.pitb.mea.msh.dialogs.DialogInformation;
import pk.gov.pitb.mea.msh.dialogs.DialogUnsentActivities;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallPatientInterviews;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackPatientInterviews;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.LocationTracker;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.patientinterview.PatientInterviewPage1;
import pk.gov.pitb.mea.msh.patientinterview.PatientInterviewPage2;


public class ActivityMainPatientInterviews extends Activity implements HandlerActivityCallPatientInterviews {

    private MainContainer mGlobals;
    private LocationTracker mLocationTracker;
    private DialogInformation dialogFacilityIncharge;
    private EditText titleFragment;
    private Fragment fragmentCurrent;
    private int fragmentPositionCurrent;
    private boolean bSaveData;

    @Override
    protected void onResume() {
        super.onResume();
        MainContainer.setContextOnResume(ActivityMainPatientInterviews.this);
       // mGlobals = Globals.getUsage(ActivityMainPatientInterviews.this);
        mLocationTracker.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationTracker.onStart();
    }

    @Override
    public void onBackPressed() {
        ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).parseObject();
        String json = mGlobals.mJsonObjectFormData.toString();
        if (json.equals(mGlobals.mSPEditor.getLastSavedJSONPatientInterviews())){
            AlertDialogs.OnDialogClickListener OnYesClick = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    bSaveData = false;
                    finish();
                }
            };
            AlertDialogs.getInstance().showDialogYesNo("Changes will be saved", "Are You Sure You Want to Exit Application?",OnYesClick,
                    null,true);
        }
        else {
            AlertDialogs.OnDialogClickListener onSavedClicked = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveData();
                    bSaveData = false;
                    finish();
                }
            };

            AlertDialogs.OnDialogClickListener onDiscardClicked = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mGlobals.mSPEditor.clearLastSavedDataPatientInterviews();
                    bSaveData = false;
                    finish();
                }
            };

            AlertDialogs.getInstance().showDialogThreeButtons("Unsaved Changes", "Do you want to save data?", "Save and Exit?", onSavedClicked,
                    "Discard Data", onDiscardClicked, "Cancel", null, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationTracker.onDestroy();
        /*if (bSaveData){
            saveData();
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_interviews);
        initActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_health_council, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_save:
                saveData();
                Toast.makeText(getApplicationContext(), "Data Saved Successfully", Toast.LENGTH_LONG).show();
                return true;
            case R.id.item_sync:
                if (mGlobals.isInternetAvailable()){
                    showMessageOnSync();
                } else {
                    AlertDialogs.getInstance().showDialogOK("Internet not Available", "Please connect to Internet to sync application", null, true);
                }
                return true;
            case R.id.item_info:
                dialogFacilityIncharge.showFacilityDialog(null);
                return true;
            case R.id.item_unsent:
                showDialogUnsent();
                return true;
            case R.id.item_exit:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void saveData(){
        ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).parseObject();
        mGlobals.mSPEditor.setLastFragmentPositionPatientInterviews("" + fragmentPositionCurrent);
        mGlobals.mSPEditor.setLastSavedFacilityPatientInterviews(mGlobals.mObjectFacility.getJSON().toString());
        mGlobals.mSPEditor.setLastSavedJSONPatientInterviews(mGlobals.mJsonObjectFormData.toString());
    }

    private void initActivity(){
        try {
            MainContainer.initializeOnStartup(ActivityMainPatientInterviews.this); // just for testing
            MainContainer.setContextOnResume(ActivityMainPatientInterviews.this);

            mGlobals.activityCallBackPatientInterviews= ActivityMainPatientInterviews.this;
            mLocationTracker = new LocationTracker(mGlobals.mContext);
            fragmentCurrent = null;
            bSaveData = true;
            initActionBar();
            initBody();
            String json = mGlobals.mSPEditor.getLastSavedJSONPatientInterviews();
            if (json.length() > 0) {

                mGlobals.mJsonObjectFormData = new JSONObject(json);
            } else {
                mGlobals.mJsonObjectFormData = new JSONObject();
            }
            selectItem(Integer.parseInt(mGlobals.mSPEditor.getLastFragmentPositionPatientInterviews()));
            ((FrameLayout) findViewById(R.id.frame_layout_patient_interviews)).setVisibility(View.VISIBLE);
            mGlobals.mObjectFacilityData = mGlobals.mDbAdapter
                    .selectFacilityData(mGlobals.mObjectFacility.facility_id, mGlobals.mObjectFacility.type_id);
            /*dialogFacilityIncharge = new DialogInformation(mGlobals.mObjectFacilityData.getObjectStaffByRole(Constants.ROLE_FACILITY_INCHARGE));
            if (mGlobals.arrayListOpenedActivities != null) {
                mGlobals.arrayListOpenedActivities.clear();
            }*/
            mGlobals.arrayListOpenedActivities = null;
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initActionBar(){
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(null);
        actionBar.setBackgroundDrawable(new ColorDrawable(mGlobals.mContext.getResources().getColor(R.color.row_heading_background)));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME);
    }

    private void initBody(){
        titleFragment = (EditText) findViewById(R.id.title_fragment_patient_interviews);
        RelativeLayout.LayoutParams lpTitle = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                (int) (mGlobals.mScreenWidth * 0.075));
        lpTitle.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lpTitle.setMargins(0, 0, 0, 0);
        titleFragment.setLayoutParams(lpTitle);
        titleFragment.setPadding(18, 0, 18, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fragmentCurrent.onActivityResult(requestCode, resultCode, data);
    }


    private void selectItem(int position) {
        switch (position) {
            case 0:
                fragmentCurrent = new /*PatientExperience*/ PatientInterviewPage1();
                titleFragment.setText("Patient Information");
                break;
            case 1:
                fragmentCurrent = new /*PatientInformation*/ PatientInterviewPage2();
                titleFragment.setText("Patient Experience");
                break;
            /*case 2:
                fragmentCurrent = new PatientDemographic();
                titleFragment.setText("Patient Demographics");
                break;*/

            default:
                fragmentCurrent = null;
                break;
        }
        if (fragmentCurrent != null) {
            fragmentCurrent.setRetainInstance(true);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_patient_interviews, fragmentCurrent, "Fragment" + fragmentPositionCurrent);
            transaction.addToBackStack("Fragment" + position);
            transaction.commit();
            ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).onFragmentShown();
            fragmentPositionCurrent = position;
        }
    }


    private void showDialogUnsent() {
        ArrayList<ClassDataActivity> listUnsent = mGlobals.mDbAdapter.selectAllActivities(Constants.AT_PATIENT_INTERVIEWS);
        if (listUnsent.size() > 0) {
            DialogUnsentActivities dialogUnsentActivities = new DialogUnsentActivities();
            dialogUnsentActivities.showUnsentDialog(listUnsent);
        } else {
            Toast.makeText(mGlobals.mContext, "No Unsent Activity to Display", Toast.LENGTH_LONG).show();
        }
    }

    private void showMessageOnSync() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mGlobals.mContext);
            builder.setTitle("All Entered Data will be Lost");
            builder.setMessage("Are You Sure You want to Sync Application?");
            //builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mGlobals.resetMainContainer();
                    Intent intent = new Intent(ActivityMainPatientInterviews.this, ActivityMain.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.setCancelable(false);
            builder.create();
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void showPreviousFragment() {
        selectItem(fragmentPositionCurrent - 1);
    }

    @Override
    public void showNextFragment() {
        ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).parseObject();
        selectItem(fragmentPositionCurrent + 1);
    }

    @Override
    public void submitData() {
        ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).parseObject();
		AlertDialogs.OnDialogClickListener onYesClicked = new AlertDialogs.OnDialogClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					AsyncTaskSendData asyncTaskSendData = new AsyncTaskSendData(mLocationTracker, null);
					asyncTaskSendData.execute();
					bSaveData = false;
					mGlobals.mSPEditor.clearLastSavedDataPatientInterviews();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		AlertDialogs.getInstance().showDialogYesNo("Patients Information Complete", "Do you want to send information to server?", onYesClicked, null,
				true);
    }

    @Override
    public void showLastFragment() {
        ((HandlerFragmentCallBackPatientInterviews) fragmentCurrent).parseObject();
        selectItem(2);
    }
}
