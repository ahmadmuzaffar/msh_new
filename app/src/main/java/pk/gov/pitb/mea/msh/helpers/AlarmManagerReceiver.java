package pk.gov.pitb.mea.msh.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.receiver.ReceiverCheckForUpdates;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmManagerReceiver {
	private Context mContext = null;

	public AlarmManagerReceiver(Context mContext) {
		this.mContext = mContext;
	}

	public void setTimeTrigger() {
		try {
			SharedPreferencesEditor spEditor = new SharedPreferencesEditor(mContext);
			if (spEditor.isCheckUpdateNeeded()) {
				Calendar calendarCurrent = Calendar.getInstance();
				Calendar calendarNextUpdate = (Calendar) calendarCurrent.clone();
				String lastSyncDateTime = spEditor.getLastCheckUpdateDate();
				if (lastSyncDateTime.length() > 0) {
					SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP, Locale.ENGLISH);
					calendarCurrent.add(Calendar.MINUTE, 2);
					calendarNextUpdate.setTime(sdf.parse(lastSyncDateTime));
					calendarNextUpdate.add(Calendar.HOUR_OF_DAY, Constants.UPDATE_HOUR_INTERVAL);
					if (calendarNextUpdate.after(calendarCurrent)) {
						spEditor.setIsSyncing("false");
					} else {
						calendarNextUpdate = (Calendar) calendarCurrent.clone();
						calendarNextUpdate.add(Calendar.MINUTE, 3);
					}
				} else {
					calendarNextUpdate.add(Calendar.MINUTE, 5);
				}
				Intent nextIntent = new Intent(mContext, ReceiverCheckForUpdates.class);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, nextIntent, 0);
				AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
				alarmManager.set(AlarmManager.RTC_WAKEUP, calendarNextUpdate.getTimeInMillis(), pendingIntent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}