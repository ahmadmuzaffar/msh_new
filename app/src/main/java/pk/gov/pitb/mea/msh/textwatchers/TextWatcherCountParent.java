package pk.gov.pitb.mea.msh.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherCountParent implements TextWatcher {

	private EditText[] editTextChilds = null;

	public TextWatcherCountParent(EditText... editTextChilds) {
		this.editTextChilds = editTextChilds;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		for (EditText etChild : editTextChilds) {
			etChild.setText("");
		}
	}

	public void afterTextChanged(Editable s) {
	}
}