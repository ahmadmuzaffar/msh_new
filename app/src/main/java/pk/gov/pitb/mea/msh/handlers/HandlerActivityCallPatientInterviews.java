package pk.gov.pitb.mea.msh.handlers;

/**
 * Created by murtaza on 10/25/2016.
 */
public interface HandlerActivityCallPatientInterviews {
    public void showPreviousFragment();

    public void showNextFragment();

    public void submitData();

    public void showLastFragment();
}
