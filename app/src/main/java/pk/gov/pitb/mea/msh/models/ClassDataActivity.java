package pk.gov.pitb.mea.msh.models;

import java.util.ArrayList;

import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.content.Context;
import android.telephony.TelephonyManager;

public class ClassDataActivity {

	private final String KEY_APP_VERSION_NO = "app_version_no";
	private final String KEY_IMEI_NUMBER = "imei_number";
	private final String KEY_GPS_LOCATION = "gps_location";
	private final String KEY_NETWORK_LOCATION = "network_location";
	private Context context = null;
	public int pk_id;
	public String app_version_no, activity_type, sending_date_time, json_data;
	public ArrayList<ClassDataPicture> arrayListPictures;

	public ClassDataActivity(Context context) {
		this.context = context;
		setEmptyValues();
	}

	public void setEmptyValues() {
		pk_id = -1;
		app_version_no = "";
		activity_type = "";
		sending_date_time = "";
		json_data = "";
	}

	public JSONObject getDefaultAppJSONObject(String gpsLocation, String networkLocation) {
		JSONObject jsonObject = new JSONObject();
		try {
			TelephonyManager telephonyManager = (TelephonyManager) MainContainer.mContext.getSystemService(Context.TELEPHONY_SERVICE);
			jsonObject = addValueInJson(jsonObject, KEY_APP_VERSION_NO, context.getResources().getString(R.string.app_version_name));
			jsonObject = addValueInJson(jsonObject, KEY_IMEI_NUMBER, telephonyManager.getDeviceId());
			jsonObject = addValueInJson(jsonObject, KEY_GPS_LOCATION, gpsLocation);
			jsonObject = addValueInJson(jsonObject, KEY_NETWORK_LOCATION, networkLocation);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	private JSONObject addValueInJson(JSONObject jsonObject, String key, String value) {
		try {
			if (jsonObject.has(key)) {
				jsonObject.remove(key);
			}
			jsonObject.put(key, value.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	@SuppressWarnings("unused")
	private String getStringFromJson(String json, String key) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			return jsonObject.has(key) ? jsonObject.getString(key).trim() : "";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}