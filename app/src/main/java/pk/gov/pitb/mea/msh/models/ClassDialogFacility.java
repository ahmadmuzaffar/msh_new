package pk.gov.pitb.mea.msh.models;

import org.json.JSONObject;

public class ClassDialogFacility {

	public String district_id, district_name, tehsil_id, tehsil_name, facility_type, facility_name, facility_id, form_type, incharge_name, phone_number, visit_type, type_id, type_name;

	public ClassDialogFacility() {
		setEmptyValues();
	}

	public void setEmptyValues() {
		district_id = "";
		district_name = "";
		tehsil_id = "";
		tehsil_name = "";
		facility_type = "";
		facility_name = "";
		facility_id = "";
		form_type = "";
		incharge_name = "";
		phone_number = "";
		visit_type = "";
		type_id = "";
		type_name = "";
	}

	public void setFromJSON(JSONObject jsonObject) {
		if (jsonObject == null) {
			setEmptyValues();
		} else {
			try {

				district_id = jsonObject.has("district_id") ? jsonObject.getString("district_id") : "";
				district_name = jsonObject.has("district_name") ? jsonObject.getString("district_name") : "";
				tehsil_id = jsonObject.has("tehsil_id") ? jsonObject.getString("tehsil_id") : "";
				tehsil_name = jsonObject.has("tehsil_name") ? jsonObject.getString("tehsil_name") : "";
				form_type = jsonObject.has("form_type") ? jsonObject.getString("form_type") : "";
				incharge_name = jsonObject.has("incharge_name") ? jsonObject.getString("incharge_name") : "";
				facility_id = jsonObject.has("facility_id") ? jsonObject.getString("facility_id") : "";
				facility_type = jsonObject.has("facility_type") ? jsonObject.getString("facility_type") : "";
				facility_name = jsonObject.has("facility_name") ? jsonObject.getString("facility_name") : "";
				visit_type = jsonObject.has("visit_type") ? jsonObject.getString("visit_type") : "";
				type_id = jsonObject.has("type_id") ? jsonObject.getString("type_id") : "";
				type_name = jsonObject.has("type_name") ? jsonObject.getString("type_name") : "";
				phone_number = jsonObject.has("phone_number") ? jsonObject.getString("phone_number") : "";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public JSONObject getJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("district_id", /*tehsil*/district_id);
			jsonObject.put("district_name", district_name);
			jsonObject.put("tehsil_id", /*facility*/tehsil_id);
			jsonObject.put("tehsil_name", tehsil_name);
			jsonObject.put("facility_id", facility_id);
			jsonObject.put("facility_name", facility_name);
			jsonObject.put("facility_type", /*tehsil*/facility_type);
			jsonObject.put("form_type", form_type);
			jsonObject.put("phone_number", /*facility*/phone_number);
			jsonObject.put("visit_type", visit_type);
			jsonObject.put("type_id", type_id);
			jsonObject.put("type_name", type_name);
			jsonObject.put("incharge_name", incharge_name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

}