package pk.gov.pitb.mea.msh.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;

import static pk.gov.pitb.mea.msh.helpers.MainContainer.mContext;

public class Utilities
{
    public static byte[] getBytes(Bitmap bitmap)
    {
        try {
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            resizedBitmap.compress(CompressFormat.JPEG, 80, stream);
            return stream.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public static byte[] getBytes(Bitmap resizedBitmap ,String picturePath)
    {
    	File file=new File(picturePath);
//    	bitmap=BitmapFactory.decodeFile(picturePath);
    	
    	//Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm");
       // String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
    	String dateTime=sdf.format(file.lastModified());
        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(24);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200 ,resizedBitmap.getHeight() - 10, tPaint);
    	
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 80, stream);
        return stream.toByteArray();
    }
    public static byte[] getSimpleBytes(Bitmap bitmap)
    {	
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 80, stream);
        return stream.toByteArray();
    }
    public static Bitmap getImage(byte[] image)
    {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}


