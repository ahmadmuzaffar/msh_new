package pk.gov.pitb.mea.msh.httpworker;

import pk.gov.pitb.mea.msh.R;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class BroadcastReceiverCheckUpdates extends BroadcastReceiver {

	private Context mContext = null;
	private AlarmManagerSetTimeTrigger alarmManagerSetTimeTrigger = null;
	private SharedPreferencesOfflineData mSharedPreferencesOfflineData = null;
	/* Notification */
	private int notificationID = -1;
	private NotificationCompat.Builder notificationBuilder = null;
	private NotificationManager notificationManager = null;
	/* HttpRequests */
	private HttpRequestGetPlayStoreVersion httpRequestGetPlayStoreVersion = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (isNetConnected(context) && setLastChecked(true, context)) {
			mContext = context;
			if (mSharedPreferencesOfflineData == null) {
				mSharedPreferencesOfflineData = new SharedPreferencesOfflineData(mContext);
			}
			alarmManagerSetTimeTrigger = new AlarmManagerSetTimeTrigger(mContext, mSharedPreferencesOfflineData);
			httpRequestGetPlayStoreVersion = new HttpRequestGetPlayStoreVersion(mContext);
			startThreadOnNetConnected();
		}
	}

	private void startThreadOnNetConnected() {
		notificationID = 6324;
		notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationBuilder = new NotificationCompat.Builder(mContext);
		notificationBuilder.setContentTitle("Sending Offline Data").setContentText("Sending in progress").setSmallIcon(R.drawable.icon);
		notificationBuilder.setAutoCancel(true);
		notificationBuilder.setOngoing(true);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					notificationManager.notify(notificationID, notificationBuilder.build());
					Intent resultIntent = null;
					PendingIntent resultPendingIntent = null;
					notificationBuilder.setContentTitle("Checking for Updates");
					notificationBuilder.setContentText("Please Wait...");
					notificationBuilder.setProgress(0, 0, true);
					notificationManager.notify(notificationID, notificationBuilder.build());
					if (httpRequestGetPlayStoreVersion.checkPlayStoreVersion()) {
						String playStoreVersion = httpRequestGetPlayStoreVersion.getPlayStoreVersion().trim();
						if (playStoreVersion.length() > 0) {
							mSharedPreferencesOfflineData.setPlayStoreVersion(playStoreVersion);
						}
						if (httpRequestGetPlayStoreVersion.isUpdateAvailable()) {
							notificationBuilder.setContentTitle("Update Available");
							notificationBuilder.setContentText("An update is available on PlayStore\nUpdate now").setProgress(0, 0, false);
							resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName()));
							resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							resultPendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
							notificationBuilder.setContentIntent(resultPendingIntent);
						}
						notificationBuilder.setOngoing(false);
						notificationManager.notify(notificationID, notificationBuilder.build());
					} else {
						notificationManager.cancel(notificationID);
					}
					setLastChecked(false, mContext);
				} catch (Exception e) {
					notificationManager.cancel(notificationID);
					setLastChecked(false, mContext);
				}
			}
		}).start();
	}

	private synchronized boolean setLastChecked(boolean bIsStart, Context context) {
		if (mSharedPreferencesOfflineData == null) {
			mSharedPreferencesOfflineData = new SharedPreferencesOfflineData(context);
		}
		if (!bIsStart) {
			mSharedPreferencesOfflineData.setIsSyncing("false");
			mSharedPreferencesOfflineData.setLastUpdate();
			alarmManagerSetTimeTrigger.setTimeTrigger();
			return true;
		} else {
			boolean bIsRunning = !mSharedPreferencesOfflineData.getIsSyncing();
			if (bIsRunning) {
				bIsRunning = mSharedPreferencesOfflineData.isUpdateNeeded();
				bIsRunning = bIsRunning ? mSharedPreferencesOfflineData.setIsSyncing("true") : false;
			}
			return bIsRunning;
		}
	}

	private boolean isNetConnected(Context context) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo(); // In air plan mode it will be null
			if (netInfo != null && netInfo.isConnected()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}