package pk.gov.pitb.mea.msh.views;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.widget.CompoundButton;
import android.widget.TableRow;
import android.widget.ToggleButton;

public class CustomToggleButtonSingleStateIndependent extends ToggleButton implements CompoundButton.OnCheckedChangeListener {

	private int colorBackgroundEmpty = 0;
	private int colorBackgroundChecked = 0;

	public CustomToggleButtonSingleStateIndependent(boolean bIsOdd, int type) {
		super(MainContainer.mContext);
		this.colorBackgroundEmpty = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		switch (type) {
			case 2:
				this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd_2 : R.drawable.toggle_tick_even_2;
			break;
			case 1:
			default:
				this.colorBackgroundChecked = bIsOdd ? R.drawable.toggle_tick_odd : R.drawable.toggle_tick_even;
			break;
		}
	}

	public void customiseToggleButton(TableRow.LayoutParams paramsToggleButton) {
		try {
			setLayoutParams(paramsToggleButton);
			setBackgroundColor(colorBackgroundEmpty);
			setTextOn("");
			setTextOff("");
			setText("");
			setOnCheckedChangeListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		try {
			if (isChecked) {
				setBackgroundResource(colorBackgroundChecked);
			} else {
				setBackgroundResource(0);
				setBackgroundColor(colorBackgroundEmpty);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}