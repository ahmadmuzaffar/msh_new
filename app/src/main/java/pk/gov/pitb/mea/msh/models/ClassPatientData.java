package pk.gov.pitb.mea.msh.models;

public class ClassPatientData {

	public String name, mobileNumber, fathersName;

	public ClassPatientData() {
		reset();
	}

	public ClassPatientData(ClassPatientData object) {
		name = object.name;
		mobileNumber = object.mobileNumber;
		fathersName = object.fathersName;
	}

	public void reset() {
		name = "";
		mobileNumber = "";
		fathersName = "";
	}
}