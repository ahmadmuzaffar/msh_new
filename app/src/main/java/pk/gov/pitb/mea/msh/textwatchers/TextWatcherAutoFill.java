package pk.gov.pitb.mea.msh.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherAutoFill implements TextWatcher {

	private boolean parentToChildClear;
	private EditText editTextParent = null;
	private EditText[] arrayEditTextChildsEditable = null;

	private final String errorMessageCountExceed = "%s cannot be more than %s";
	private final String errorMessageCountParentEmpty = "%s cannot be entered before %s";

	public TextWatcherAutoFill(EditText editTextParent,EditText[] arrayEditTextChildsEditable ) {
		this.editTextParent = editTextParent;
		this.arrayEditTextChildsEditable = arrayEditTextChildsEditable;

	}

	@Override
	public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
		if (charSequence.toString().equals("0")){
			for (int j = 0 ; j < arrayEditTextChildsEditable.length; j++){
				arrayEditTextChildsEditable[j].setText("0");
			}
		}
		else if (charSequence.toString().equals("999")){
			for (int j = 0 ; j < arrayEditTextChildsEditable.length; j++){
				arrayEditTextChildsEditable[j].setText("999");
			}
		}
	}

	@Override
	public void afterTextChanged(Editable editable) {

	}
}