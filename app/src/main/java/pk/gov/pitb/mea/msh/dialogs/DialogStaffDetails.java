package pk.gov.pitb.mea.msh.dialogs;

import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassStaff;
import pk.gov.pitb.mea.msh.textwatchers.TextWatcherCNIC;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

public class DialogStaffDetails implements View.OnClickListener {

	public static abstract class HandlerDialogCallBack {

		public abstract void onButtonPositive();

		public abstract void onButtonNeutral();

		public abstract void onButtonNegative();
	}

	private View viewClicked;
	private HandlerDialogCallBack handlerDialogCallBack;
	private ClassStaff objectStaffOld;
	private ClassStaff objectStaffNew;
	private TableLayout tableLayoutDialog;
	private CustomEditText editTextName;
	private CustomEditText editTextMobile;
	private CustomEditText editTextCNIC;
	private CustomEditText editTextComments;
	private boolean bIsCommentsRequired;
	private boolean bIsValidName, bIsValidMobile, bIsValidCNIC, bIsValidComments;
	private RadioButton radioButtonAbsent;
	private Spinner spinnerAbsence;

	public DialogStaffDetails(Spinner spinnerAbsence, HandlerDialogCallBack handlerDialogCallBack) {
		this.spinnerAbsence = spinnerAbsence;
		this.handlerDialogCallBack = handlerDialogCallBack;
		if (this.handlerDialogCallBack == null) {
			this.handlerDialogCallBack = new HandlerDialogCallBack() {

				@Override
				public void onButtonPositive() {
				}

				@Override
				public void onButtonNeutral() {
				}

				@Override
				public void onButtonNegative() {
				}
			};
		}
		this.bIsValidName = true;
		this.bIsValidCNIC = true;
		this.bIsValidMobile = true;
		this.bIsValidComments = true;
	}

	public void showStaffDetailsDialog() {
		try {
			if (spinnerAbsence != null) {
				this.bIsCommentsRequired = MainContainer.isCommentsRequired((ClassScreenItem) spinnerAbsence.getSelectedItem());
			} else {
				this.bIsCommentsRequired = false;
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			// builder.setTitle("Enter Details");

			LinearLayout linearLayout = new LinearLayout(MainContainer.mContext);
			tableLayoutDialog = new TableLayout(MainContainer.mContext);
			tableLayoutDialog.setPadding((int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02),
					(int) (MainContainer.mScreenWidth * .02));

			TableRow.LayoutParams paramsHeading = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .90), TableRow.LayoutParams.WRAP_CONTENT);
			paramsHeading.span = 2;
			TableRow.LayoutParams paramsLabel = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .248), TableRow.LayoutParams.MATCH_PARENT);
			TableRow.LayoutParams paramsValue = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .65), TableRow.LayoutParams.WRAP_CONTENT);
			paramsLabel.setMargins(0, (int) (MainContainer.mScreenHeight * .002), 0, 0);
			paramsValue.setMargins((int) (MainContainer.mScreenWidth * .002), (int) (MainContainer.mScreenHeight * .002), 0, 0);
			addTableRow("Enter Details", null, paramsHeading);
			editTextName = addTableRow("Name", objectStaffNew.staff_name, paramsLabel, paramsValue);
			editTextName.setInputTypeTextEditable();
			editTextName.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			editTextCNIC = addTableRow("CNIC", objectStaffNew.staff_cnic, paramsLabel, paramsValue);
			editTextCNIC.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			editTextCNIC.addTextChangedListener(new TextWatcherCNIC(editTextCNIC));
			editTextMobile = addTableRow("Mobile", objectStaffNew.staff_mobile, paramsLabel, paramsValue);
			editTextMobile.setInputTypeNumberEditable(Constants.MAX_LENGTH_MOBILE_NUMBER);
			editTextMobile.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			if (bIsCommentsRequired) {
				editTextComments = addTableRow("Comments", objectStaffNew.comments, paramsLabel, paramsValue);
				editTextComments.setInputTypeTextEditable();
				editTextComments.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
				editTextComments.setHint("Enter Comments");
			}

			editTextName.requestFocus();
			linearLayout.setOrientation(LinearLayout.VERTICAL);
			linearLayout.addView(tableLayoutDialog);
			editTextName.setHint("Enter Staff's Name");
			editTextCNIC.setHint("Enter Staff's CNIC");
			editTextMobile.setHint("Enter Staff's Mobile");
			if (!bIsValidName) {
				editTextName.setError("Please Enter Name");
			}
			if (!bIsValidCNIC) {
				if (objectStaffNew.staff_mobile.length() > 0) {
					editTextCNIC.setError("Please Enter Valid CNIC");
				} else {
					editTextCNIC.setError("Please Enter CNIC");
				}
			}
			if (!bIsValidMobile) {
				if (objectStaffNew.staff_mobile.length() > 0) {
					editTextMobile.setError("Please Enter Valid Mobile Number");
				} else {
					editTextMobile.setError("Please Enter Mobile Number");
				}
			}
			if (bIsCommentsRequired && !bIsValidComments) {
				editTextComments.setError("Please Enter Comments");
			}
			builder.setView(linearLayout);
			builder.setCancelable(false);
			builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					if (isValid()) {
						objectStaffOld.copyValues(objectStaffNew);
						viewClicked.setTag(objectStaffOld);
						if (viewClicked instanceof EditText) {
							((EditText) viewClicked).setText(objectStaffOld.staff_name);
						}
						if (radioButtonAbsent == null || radioButtonAbsent.isChecked()) {
							handlerDialogCallBack.onButtonPositive();
						} else {
							handlerDialogCallBack.onButtonNeutral();
						}
					} else {
						showStaffDetailsDialog();
					}
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {
					handlerDialogCallBack.onButtonNegative();
				}
			});
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isValid() {
		try {
			objectStaffNew.staff_name = editTextName.getText().toString().trim();
			objectStaffNew.staff_mobile = editTextMobile.getText().toString().trim();
			objectStaffNew.staff_cnic = editTextCNIC.getText().toString().trim();

			if (objectStaffNew.staff_name.length() == 0) {
				bIsValidName = false;
			} else {
				bIsValidName = true;
			}
			if (objectStaffNew.staff_mobile.length() > 0 && objectStaffNew.staff_mobile.length() < Constants.MIN_LENGTH_MOBILE_NUMBER) {
				bIsValidMobile = false;
			} else {
				bIsValidMobile = true;
			}
			if (objectStaffNew.staff_cnic.length() > 0 && objectStaffNew.staff_cnic.length() < Constants.LENGTH_CNIC) {
				bIsValidCNIC = false;
			} else {
				bIsValidCNIC = true;
			}
			if (bIsCommentsRequired) {
				objectStaffNew.comments = editTextComments.getText().toString().trim();
				if (objectStaffNew.comments.length() > 0) {
					bIsValidComments = true;
				} else {
					bIsValidComments = false;
				}
			} else {
				objectStaffNew.comments = "";
			}

			if (!(bIsValidName && bIsValidCNIC && bIsValidMobile)) {
				Toast.makeText(MainContainer.mContext, "Please Enter All Details to Continue", Toast.LENGTH_LONG).show();
				return false;
			} else if (bIsCommentsRequired && !bIsValidComments) {
				Toast.makeText(MainContainer.mContext, "Please Enter All Details to Continue", Toast.LENGTH_LONG).show();
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private CustomEditText addTableRow(String label, String value, TableRow.LayoutParams... paramsCol) {
		try {
			boolean bIsOdd = tableLayoutDialog.getChildCount() % 2 == 1;
			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextLabel = null;
			CustomEditText editTextValue = null;
			if (value == null) {
				editTextLabel = new CustomEditText(label);
				editTextLabel.setGravity(Gravity.CENTER);
				editTextLabel.customiseEditText(paramsCol[0]);
				tableRow.addView(editTextLabel);
			} else {
				editTextLabel = new CustomEditText(bIsOdd, label);
				editTextValue = new CustomEditText(bIsOdd);
				editTextValue.setText(value);
				if (value.length() > 0) {
					editTextValue.setSelection(value.length());
				}
				editTextLabel.customiseEditText(paramsCol[0]);
				editTextValue.customiseEditText(paramsCol[1]);
				tableRow.addView(editTextLabel);
				tableRow.addView(editTextValue);
			}
			tableLayoutDialog.addView(tableRow);
			return editTextValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onClick(View view) {
		try {
			viewClicked = view;
			objectStaffOld = (ClassStaff) view.getTag();
			if (objectStaffOld == null) {
				objectStaffOld = new ClassStaff();
			}
			objectStaffNew = new ClassStaff(objectStaffOld);
			showStaffDetailsDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}