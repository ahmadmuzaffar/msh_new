package pk.gov.pitb.mea.msh.dialogs;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassPatientData;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

public class DialogGetPatientDetailsType1 implements View.OnClickListener {

	private ArrayList<ClassPatientData> arrayListPatientPrevious = null;
	private ArrayList<ClassPatientData> arrayListPatientNew = null;
	private TableLayout tableLayoutDialog = null;
	private EditText editTextPosition = null;
	private EditText editTextTotalNumber = null;

	public DialogGetPatientDetailsType1(EditText editTextPosition, EditText editTextTotalNumber) {
		this.editTextPosition = editTextPosition;
		this.editTextTotalNumber = editTextTotalNumber;
	}

	public void showFacilityDialog() {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			builder.setTitle("Enter Details of " + editTextPosition.getText().toString().trim());

			tableLayoutDialog = new TableLayout(MainContainer.mContext);
			tableLayoutDialog.setPadding((int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02),
					(int) (MainContainer.mScreenWidth * .02));

			TableRow.LayoutParams paramsSerialNum = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0), TableRow.LayoutParams.WRAP_CONTENT);
			TableRow.LayoutParams paramsName = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .45), TableRow.LayoutParams.WRAP_CONTENT);
			TableRow.LayoutParams paramsMobileNumber = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .445), TableRow.LayoutParams.WRAP_CONTENT);
			paramsName.setMargins(0, 0, (int) (MainContainer.mScreenWidth * .005), 0);
			addTableRow(null, tableLayoutDialog, paramsSerialNum, paramsName, paramsMobileNumber);
			for (ClassPatientData object : arrayListPatientNew) {
				addTableRow(object, tableLayoutDialog, paramsSerialNum, paramsName, paramsMobileNumber);
			}

			builder.setView(tableLayoutDialog);
			builder.setCancelable(false);
			builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					if (isValid()) {
						arrayListPatientPrevious.clear();
						editTextPosition.setTag(arrayListPatientNew);
						setParentClickable(true);
					} else {
						showFacilityDialog();
					}
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {
					setParentClickable(true);
				}
			});
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isValid() {
		try {
			boolean valid = true;
			for (int i = 1; i < tableLayoutDialog.getChildCount(); i++) {
				TableRow tableRow = (TableRow) tableLayoutDialog.getChildAt(i);
				EditText name = (EditText) tableRow.getChildAt(1);
				EditText mobileNum = (EditText) tableRow.getChildAt(2);
				ClassPatientData object = arrayListPatientNew.get(i - 1);
				object.name = name.getText().toString().trim();
				object.mobileNumber = mobileNum.getText().toString().trim();
				if (object.name.length() == 0 || object.mobileNumber.length() == 0 || object.mobileNumber.length() < Constants.MIN_LENGTH_MOBILE_NUMBER) {
					valid = false;
				}
			}
			if (!valid) {
				Toast.makeText(MainContainer.mContext, "Please Enter All Details to Continue", Toast.LENGTH_LONG).show();
			}
			return valid;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void addTableRow(ClassPatientData object, TableLayout tableLayoutDialog, TableRow.LayoutParams... paramsCol) {
		try {
			TableRow tableRow = new TableRow(MainContainer.mContext);
			int index = tableLayoutDialog.getChildCount() - 1;
			boolean bIsOdd = index % 2 == 0;

			CustomEditText editTextSerialNum = null;
			CustomEditText editTextName = null;
			CustomEditText editTextMobileNumber = null;
			if (object == null) {
				editTextSerialNum = new CustomEditText("S. No.");
				editTextName = new CustomEditText("Name");
				editTextMobileNumber = new CustomEditText("Mobile Number");
				editTextSerialNum.setGravity(Gravity.CENTER);
				editTextName.setGravity(Gravity.CENTER);
				editTextMobileNumber.setGravity(Gravity.CENTER);
			} else {
				editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
				editTextName = new CustomEditText(bIsOdd);
				editTextMobileNumber = new CustomEditText(bIsOdd);
				editTextName.setInputTypeTextEditable();
				editTextMobileNumber.setInputTypeNumberEditable(Constants.MAX_LENGTH_MOBILE_NUMBER);
				editTextName.setHint("Patient's Name");
				editTextMobileNumber.setHint("Patient's Mobile Number");
				editTextName.setText(object.name);
				editTextMobileNumber.setText(object.mobileNumber);
				if (object.name.length() > 0) {
					editTextName.setSelection(object.name.length());
				}
			}
			editTextSerialNum.customiseEditText(paramsCol[0]);
			editTextName.customiseEditText(paramsCol[1]);
			editTextMobileNumber.customiseEditText(paramsCol[2]);

			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextName);
			tableRow.addView(editTextMobileNumber);
			tableLayoutDialog.addView(tableRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onClick(View view) {
		try {
			setParentClickable(false);
			String text = editTextTotalNumber.getText().toString();
			int size = text.length() > 0 ? Integer.parseInt(text) : 0;
			if (size > 2) {
				size = 2;
			}
			arrayListPatientPrevious = (ArrayList<ClassPatientData>) editTextPosition.getTag();
			if (arrayListPatientPrevious == null) {
				arrayListPatientPrevious = new ArrayList<ClassPatientData>();
			}
			arrayListPatientNew = new ArrayList<ClassPatientData>(size);
			for (int i = 0; i < size; i++) {
				if (i < arrayListPatientPrevious.size()) {
					arrayListPatientNew.add(new ClassPatientData(arrayListPatientPrevious.get(i)));
				} else {
					arrayListPatientNew.add(new ClassPatientData());
				}
			}
			for (int i = size; i < arrayListPatientPrevious.size(); i++) {
				arrayListPatientPrevious.remove(i--);
			}
			if (size > 0) {
				showFacilityDialog();
			} else {
				Toast.makeText(MainContainer.mContext, "No Cases To Display", Toast.LENGTH_LONG).show();
				setParentClickable(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setParentClickable(boolean bIsClickable) {
		if (bIsClickable) {
			editTextPosition.setOnClickListener(new DialogGetPatientDetailsType1(editTextPosition, editTextTotalNumber));
		} else {
			editTextPosition.setOnClickListener(null);
		}
	}
}