package pk.gov.pitb.mea.msh.models;

public class ClassSchool {
	public int school_id ;
	public String mea_name = "";
	public String mea_district = "";
	public String emis_code = "";
	public String school_name = "";
	public String school_district = "";
	public String school_is_stipend = "";
	public String school_is_non_salaried = "";
	public String school_json_data = "";
	public ClassSchool() {
		reset();
	}
	public void reset(){
		school_id = -1;
		mea_name = "";
		mea_district = "";
		emis_code = "";
		school_name = "";
		school_district = "";
		school_is_stipend = "";
		school_is_non_salaried = "";
		school_json_data = "";
	}
}