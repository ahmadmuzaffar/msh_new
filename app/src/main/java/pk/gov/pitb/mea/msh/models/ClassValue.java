package pk.gov.pitb.mea.msh.models;

public class ClassValue {

	public String id, value;

	public ClassValue() {
		reset();
	}

	public void reset() {
		id = "";
		value = "";
	}
}