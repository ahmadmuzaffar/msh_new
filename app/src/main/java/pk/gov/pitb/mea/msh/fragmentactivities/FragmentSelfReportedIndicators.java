package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class FragmentSelfReportedIndicators extends Fragment implements InterfaceFragmentCallBack {

	private ArrayList<ClassScreenItem> arrayListIndicators;

	private View parentView;
	private LinearLayout linearLayoutMain;
	private TextView textViewIndicators;
	private TableLayout tableLayoutIndicators;

	@Override
	public void parseObject() {
		try {
			JSONArray jsonArrayIndicators = new JSONArray();
			for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
				View child = tableLayoutIndicators.getChildAt(i);
				ClassScreenItem object = arrayListIndicators.get(i - 1);
				JSONObject jsonObjectChild = new JSONObject();

				TableRow tableRow = (TableRow) child;
				EditText number = (EditText) tableRow.getChildAt(2);

				jsonObjectChild.put("sri_id", object.item_id);
				jsonObjectChild.put("indicator", object.item_name);
				jsonObjectChild.put("number", number.getText().toString().trim());

				jsonArrayIndicators.put(jsonObjectChild);
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("indicators", jsonArrayIndicators);
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS);
				JSONArray jsonArray = jsonObject.getJSONArray("indicators");
				for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
					View child = tableLayoutIndicators.getChildAt(i);
					JSONObject jsonObjectChild = jsonArray.getJSONObject(i-1);

					TableRow tableRow = (TableRow) child;
					EditText number = (EditText) tableRow.getChildAt(2);
					number.setText(jsonObjectChild.getString("number"));
				}
			}
		}
		catch (Exception e){

		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_SELF_REPORTED_INDICATORS;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListIndicators = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			textViewIndicators = new TextView(MainContainer.mContext);
			tableLayoutIndicators = new TableLayout(MainContainer.mContext);

			linearLayoutMain.addView(textViewIndicators);
			linearLayoutMain.addView(tableLayoutIndicators);

			TableRow.LayoutParams[] paramsArrayTableRowColumnsIndicators = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumnsIndicators[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), TableRow.LayoutParams.WRAP_CONTENT);
			paramsArrayTableRowColumnsIndicators[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.4), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsIndicators[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.35), TableRow.LayoutParams.MATCH_PARENT);
			for (int i = 1; i < paramsArrayTableRowColumnsIndicators.length; i++) {
				paramsArrayTableRowColumnsIndicators[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewIndicators, "Hospital Self Reported Indicators for Previous Month");
			uiContainer.generateTableHeader(tableLayoutIndicators, paramsArrayTableRowColumnsIndicators, "No.", "Indicators", "Number");
			generateTableIndicators(paramsArrayTableRowColumnsIndicators);

			if (tableLayoutIndicators.getChildCount() > 1) {
				textViewIndicators.setVisibility(View.VISIBLE);
				tableLayoutIndicators.setVisibility(View.VISIBLE);
			} else {
				textViewIndicators.setVisibility(View.GONE);
				tableLayoutIndicators.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableIndicators(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (int index = 0; index < arrayListIndicators.size(); index++) {
			boolean bIsOdd = index % 2 == 0;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + (index + 1));
			CustomEditText editTextIndicator = new CustomEditText(bIsOdd, arrayListIndicators.get(index).item_name);
			CustomEditText editTextNumber = new CustomEditText(bIsOdd);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextIndicator.customiseEditText(paramsArrayTableRowColumns[1]);
			editTextNumber.customiseEditText(paramsArrayTableRowColumns[2]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableLayoutIndicators.addView(tableRow);

			editTextNumber.setInputTypeNumberEditable();

			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextIndicator);
			tableRow.addView(editTextNumber);
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 1; i < tableLayoutIndicators.getChildCount(); i++) {
				View child = tableLayoutIndicators.getChildAt(i);
				TableRow tableRow = (TableRow) child;
				EditText number = (EditText) tableRow.getChildAt(2);

				if (number.getText().toString().trim().length() == 0) {
					bIsValid = false;
				}
			}
			if (!bIsValid) {
				errorMessage = "Please fill all values to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}