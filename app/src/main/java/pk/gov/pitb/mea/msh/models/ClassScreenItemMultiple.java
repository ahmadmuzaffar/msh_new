package pk.gov.pitb.mea.msh.models;

import java.util.ArrayList;

public class ClassScreenItemMultiple {

	public String label;
	public ArrayList<ClassScreenItem> arrayListFields;

	public ClassScreenItemMultiple() {
		reset();
	}

	public ClassScreenItemMultiple(String label, ArrayList<ClassScreenItem> arrayListFields) {
		this.label = label;
		this.arrayListFields = arrayListFields;
	}

	public void reset() {
		label = "";
		arrayListFields = null;
	}

	@Override
	public String toString() {
		return label;
	}
}