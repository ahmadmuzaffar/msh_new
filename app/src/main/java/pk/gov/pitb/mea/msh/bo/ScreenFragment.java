package pk.gov.pitb.mea.msh.bo;

import java.util.Locale;

public class ScreenFragment {

	public String name;
	public boolean bIsMandatory;
	public int page;

	@Override
	public boolean equals(Object o) {
		return o.toString().toLowerCase(Locale.ENGLISH).equals(name.toLowerCase(Locale.ENGLISH));
	}

	public ScreenFragment(String name, boolean bIsMandatory) {
		this.name = name;
		this.bIsMandatory = bIsMandatory;
	}

	public ScreenFragment(String name, boolean bIsMandatory, int page) {
		super();
		this.name = name;
		this.bIsMandatory = bIsMandatory;
		this.page = page;
	}

	@Override
	public String toString() {
		return name;
	}
}