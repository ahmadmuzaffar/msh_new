package pk.gov.pitb.mea.msh.helpers;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * 
 * @author REHAN created on Jun 1, 2015
 */
public class AlertDialogs {

	private static AlertDialogs instance = null;

	private AlertDialogs() {
	}

	public static AlertDialogs getInstance() {
		if (instance == null) {
			instance = new AlertDialogs();
		}
		return instance;
	}

	public void showDialogYesNo(String title, String message, OnDialogClickListener onClickPositiveButton,
			OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(/*Globals.getInstance().mContext*/MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton("Yes", onClickPositiveButton);
			builder.setNegativeButton("No", onClickNegativeButton);
			builder.setCancelable(bIsCancelable);
			builder.create().show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showDialogOK(String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(/*Globals.getInstance().mContext*/MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton("OK", onClickPositiveButton);
			builder.setCancelable(bIsCancelable);
			builder.create().show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showDialogOneButton(String title, String message, String textPositive, OnDialogClickListener onClickPositiveButton,
			boolean bIsCancelable) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(/*Globals.getInstance().mContext*/MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton(textPositive, onClickPositiveButton);
			builder.setCancelable(bIsCancelable);
			builder.create().show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showDialogTwoButtons(String title, String message, String textPositive, OnDialogClickListener onClickPositiveButton,
			String textNegative, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(/*Globals.getInstance().mContext*/MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton(textPositive, onClickPositiveButton);
			builder.setNegativeButton(textNegative, onClickNegativeButton);
			builder.setCancelable(bIsCancelable);
			builder.create().show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showDialogThreeButtons(String title, String message, String textPositive, OnDialogClickListener onClickPositiveButton,
			String textNegative, OnDialogClickListener onClickNegativeButton, String textNeutral, OnDialogClickListener onClickNeutralButton,
			boolean bIsCancelable) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(/*Globals.getInstance().mContext*/MainContainer.mContext);
			if (title != null) {
				builder.setTitle(title);
			}
			builder.setMessage(message);
			builder.setPositiveButton(textPositive, onClickPositiveButton);
			builder.setNegativeButton(textNegative, onClickNegativeButton);
			builder.setNeutralButton(textNeutral, onClickNeutralButton);
			builder.setCancelable(bIsCancelable);
			builder.create().show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public interface OnDialogClickListener extends DialogInterface.OnClickListener {
	}
}