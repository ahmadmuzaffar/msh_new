package pk.gov.pitb.mea.msh.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSendData;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassStaff;
import pk.gov.pitb.mea.msh.views.CustomEditText;

public class DialogInformation {

	private EditText editTextLabelTableOtherInformation;
	private TableLayout tableLayoutOtherInformation;
	private AsyncTaskSendData asyncTaskSendData;
	private CustomEditText editTextName;
	private CustomEditText editTextMobile;
	private boolean bIsValidName, bIsValidMobile, bIsValidCNIC;
	private String inchargeName, phoneNumber;
	private ClassStaff objectFacilityInchargeOld;
	private ClassStaff objectFacilityInchargeNew;

	public DialogInformation() {
		this.asyncTaskSendData = null;
		this.bIsValidName = true;
		this.bIsValidMobile = true;
		this.inchargeName = MainContainer.mObjectFacility.incharge_name;
		this.phoneNumber = MainContainer.mObjectFacility.phone_number;
	}

	public DialogInformation(ClassStaff objectFacilityInchargeOld) {
		/*this.mGlobals = Globals.getInstance();*/
		this.objectFacilityInchargeOld = objectFacilityInchargeOld;
		this.objectFacilityInchargeNew = new ClassStaff(this.objectFacilityInchargeOld);
		this.bIsValidName = true;
		this.bIsValidCNIC = true;
		this.bIsValidMobile = true;
		this.asyncTaskSendData = null;
	}


	@SuppressLint("InflateParams")
	public void showFacilityDialog(AsyncTaskSendData asyncTask) {
		try {
			this.asyncTaskSendData = asyncTask;
			SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);

			View viewLayout = MainContainer.mActivity.getLayoutInflater().inflate(R.layout.dialog_facility_incharge, null);
			editTextLabelTableOtherInformation = (EditText) viewLayout.findViewById(R.id.label_table_other_info);
			tableLayoutOtherInformation = (TableLayout) viewLayout.findViewById(R.id.tablelayout_other_info);

			viewLayout.setPadding((int) (MainContainer.mScreenWidth * 0.002), (int) (MainContainer.mScreenWidth * 0.002), (int) (MainContainer.mScreenWidth * 0.002),
					(int) (MainContainer.mScreenWidth * 0.002));
			LinearLayout.LayoutParams paramsChild = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * 0.9), LinearLayout.LayoutParams.WRAP_CONTENT);
			paramsChild.setMargins(0, 0, 0, 0);
			editTextLabelTableOtherInformation.setLayoutParams(paramsChild);
			tableLayoutOtherInformation.setLayoutParams(paramsChild);

			editTextLabelTableOtherInformation.setPadding((int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02),
					(int) (MainContainer.mScreenWidth * .02));

			TableRow.LayoutParams paramsLabel = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .45), TableRow.LayoutParams.MATCH_PARENT);
			TableRow.LayoutParams paramsValue = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .45), TableRow.LayoutParams.WRAP_CONTENT);
			paramsLabel.setMargins(0, 0, (int) (MainContainer.mScreenWidth * .002), 0);
			paramsValue.setMargins(0, 0, 0, 0);

			addOtherDetail("Version Name", MainContainer.mContext.getResources().getString(R.string.app_version_name), paramsLabel, paramsValue);
			addOtherDetail("Application Structure Version", sharedPreferencesEditor.getStructuralVersion(), paramsLabel, paramsValue);
			addOtherDetail("Facility Data Version", sharedPreferencesEditor.getFacilityVersion(), paramsLabel, paramsValue);
			addOtherDetail("District", MainContainer.mDbAdapter.getDistrictName(MainContainer.mObjectFacility.district_id), paramsLabel, paramsValue);
			addOtherDetail("Tehsil", MainContainer.mDbAdapter.getTehsilName(MainContainer.mObjectFacility.tehsil_id), paramsLabel, paramsValue);
			addOtherDetail("IMEI Number", MainContainer.mImeiNumber, paramsLabel, paramsValue);
			addOtherDetail("Facility Type", MainContainer.mObjectFacility.facility_type, paramsLabel, paramsValue);
			addOtherDetail("Facility", MainContainer.mObjectFacility.facility_name, paramsLabel, paramsValue);
			addOtherDetail("Form Type", MainContainer.mObjectFacility.form_type, paramsLabel, paramsValue);
			editTextName = addOtherDetail("Incharge Name", inchargeName, paramsLabel, paramsValue);
			editTextMobile = addOtherDetail("Phone Number", phoneNumber, paramsLabel, paramsValue);
			editTextName.setInputTypeTextEditable();
			editTextName.setHint("Enter Facility Incharge's Name");
			editTextName.setEnabled(true);
			editTextName.setSelection(inchargeName.length());
			editTextMobile.setInputTypeNumberEditable(Constants.MAX_LENGTH_MOBILE_NUMBER);
			editTextMobile.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			editTextMobile.setHint("Enter Phone Number");
			editTextMobile.setEnabled(true);

			if (!bIsValidName) {
				editTextName.setError("Please Enter Name");
			}
			if (!bIsValidMobile) {
				if (phoneNumber.length() > 0) {
					editTextMobile.setError("Please Enter Valid Mobile Number");
				} else {
					editTextMobile.setError("Please Enter Mobile Number");
				}
			}
			builder.setView(viewLayout);
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					if (isValid()) {
						MainContainer.mObjectFacility.incharge_name = inchargeName;
						MainContainer.mObjectFacility.phone_number = phoneNumber;
						if (asyncTaskSendData != null) {
							AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
							builder.setMessage("Are you sure you want to send the information?");
							builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialogInterface, int id) {
									try {
										MainContainer.mSPEditor.clearLastSavedDataMonitoring();
										asyncTaskSendData.execute();
									} catch (Exception e) {
										e.printStackTrace();
										Toast.makeText(MainContainer.mContext, "Application Error", Toast.LENGTH_LONG).show();
									}
								}
							});
							builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialogInterface, int id) {
									showFacilityDialog(asyncTaskSendData);
								}
							});
							builder.show();
						}
					} else {
						showFacilityDialog(asyncTaskSendData);
					}
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					asyncTaskSendData = null;
					bIsValidName = true;
					bIsValidMobile = true;
					inchargeName = MainContainer.mObjectFacility.incharge_name;
					phoneNumber = MainContainer.mObjectFacility.phone_number;
				}
			});
			builder.create();
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isValid() {
		try {
			inchargeName = editTextName.getText().toString().trim();
			phoneNumber = editTextMobile.getText().toString().trim();

			if (inchargeName.length() == 0) {
				bIsValidName = false;
			} else {
				bIsValidName = true;
			}
			if (phoneNumber.length() < Constants.MIN_LENGTH_MOBILE_NUMBER) {
				bIsValidMobile = false;
			} else {
				bIsValidMobile = true;
			}
			if (!(bIsValidName && bIsValidMobile)) {
				Toast.makeText(MainContainer.mContext, "Please Enter All Details to Continue", Toast.LENGTH_LONG).show();
			}
			return (bIsValidName && bIsValidMobile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private CustomEditText addOtherDetail(String label, String value, TableRow.LayoutParams... paramsCol) {
		try {
			TableRow tableRow = new TableRow(MainContainer.mContext);
			int index = tableLayoutOtherInformation.getChildCount() - 1;
			boolean bIsOdd = index % 2 == 0;

			CustomEditText editTextLabel = new CustomEditText(bIsOdd, label);
			CustomEditText editTextValue = new CustomEditText(bIsOdd, value);
			editTextLabel.customiseEditText(paramsCol[0]);
			editTextValue.customiseEditText(paramsCol[1]);
			editTextValue.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

			tableRow.addView(editTextLabel);
			tableRow.addView(editTextValue);
			tableLayoutOtherInformation.addView(tableRow);
			return editTextValue;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}