package pk.gov.pitb.mea.msh.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ClassFacilityData {

	public int pk_id;
	public String facility_id;
	public String facility_type_code;
	public String facility_type;
	public String facility_name;
	public String tehsil_id;
	public String tehsil_name;
	public String district_id;
	public String district_name;
	public String facility_nutrition_second_type;
	public String facility_sentinel_second_type;
	public String json_arrays_data;
	public ArrayList<ClassStaff> arrayListStaff;

	public ClassFacilityData() {
		setEmptyValues();
	}

	public void setEmptyValues() {
		pk_id = -1;
		facility_id = "";
		facility_type_code = "";
		facility_type = "";
		facility_name = "";
		tehsil_id = "";
		tehsil_name = "";
		district_id = "";
		district_name = "";
		facility_nutrition_second_type = "";
		facility_sentinel_second_type = "";
		json_arrays_data = "";
		arrayListStaff = null;
	}

	public ArrayList<ClassFacilityItem> getListSanctionedStaff() {
		try {
			JSONObject jsonObjectArrays = new JSONObject(json_arrays_data);
			JSONObject jsonObjectSanctionedStaff = jsonObjectArrays.getJSONObject("sanctioned_staff");
			String valueArraySanctionedStaff = jsonObjectSanctionedStaff.has("sanctioned_staff") ? jsonObjectSanctionedStaff.getString("sanctioned_staff") : "null";
			if (valueArraySanctionedStaff.equals("null")) {
				valueArraySanctionedStaff = "[]";
			}
			JSONArray jsonArraySanctionedStaff = new JSONArray(valueArraySanctionedStaff);
			return getListFacilityItem(jsonArraySanctionedStaff, "position_id", "sanctioned_staff");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean getIsSanctionedStaffEditable() {
		try {
			JSONObject jsonObjectArrays = new JSONObject(json_arrays_data);
			JSONObject jsonObjectSanctionedStaff = jsonObjectArrays.getJSONObject("sanctioned_staff");
			String is_editable = jsonObjectSanctionedStaff.has("is_editable") ? jsonObjectSanctionedStaff.getString("is_editable") : "yes";
			return is_editable.equals("yes");
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public ArrayList<ClassFacilityItem> getListTotalAvailableMedEquip() {
		try {
			JSONObject jsonObjectArrays = new JSONObject(json_arrays_data);
			JSONObject jsonObjectTotalMedicalEquipment = jsonObjectArrays.getJSONObject("total_available_medical_equipment");
			String valueArrayTotalMedicalEquipment = jsonObjectTotalMedicalEquipment.has("total_available_medical_equipment") ? jsonObjectTotalMedicalEquipment
					.getString("total_available_medical_equipment") : "null";
			if (valueArrayTotalMedicalEquipment.equals("null")) {
				valueArrayTotalMedicalEquipment = "[]";
			}
			JSONArray jsonArrayTotalMedicalEquipment = new JSONArray(valueArrayTotalMedicalEquipment);
			return getListFacilityItem(jsonArrayTotalMedicalEquipment, "med_equipment_id", "total_available");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean getIsTotalAvailableMedEquipEditable() {
		try {
			JSONObject jsonObjectArrays = new JSONObject(json_arrays_data);
			JSONObject jsonObjectSanctionedStaff = jsonObjectArrays.getJSONObject("total_available_medical_equipment");
			String is_editable = jsonObjectSanctionedStaff.has("is_editable") ? jsonObjectSanctionedStaff.getString("is_editable") : "yes";
			return is_editable.equals("yes");
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public ClassStaff getStaffByDesignationId(String designationId) {
		try {
			for (ClassStaff object : arrayListStaff) {
				if (object.staff_designation_id.equalsIgnoreCase(designationId)) {
					return new ClassStaff(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public ClassStaff getObjectStaffByRole(String keyRole) {
		try {
			ArrayList<ClassStaff> listStaff = arrayListStaff;
			for (ClassStaff object : listStaff) {
				if (object.staff_role.equalsIgnoreCase(keyRole))
					return object;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private ArrayList<ClassFacilityItem> getListFacilityItem(JSONArray jsonArray, String keyId, String keyValue) {
		ArrayList<ClassFacilityItem> listItems = new ArrayList<ClassFacilityItem>() {

			private static final long serialVersionUID = 1L;

			@Override
			public ClassFacilityItem get(int index) {
				if (index == -1)
					return new ClassFacilityItem();
				else
					return super.get(index);
			}
		};
		try {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				ClassFacilityItem object = new ClassFacilityItem();
				object.fitem_id = jsonObject.has(keyId) ? jsonObject.getString(keyId) : "";
				object.fitem_name = jsonObject.has(keyValue) ? jsonObject.getString(keyValue) : "";
				listItems.add(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listItems;
	}
}