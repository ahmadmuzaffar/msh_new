package pk.gov.pitb.mea.msh.handlers;

public interface InterfaceActivityCallBack {

	public void changeButtonNextState(boolean bState);
	public void changeButtonPreviousState(boolean bState);
	public void changeButtonSubmitState(boolean bState);
}
