package pk.gov.pitb.mea.msh.models;

import pk.gov.pitb.mea.msh.helpers.MainContainer;

public class ClassScreenData {

	public int pk_id;
	public String type_id, type_name, screen_id, screen_name, section_id, section_name, section_value;

	public ClassScreenData() {
		setEmptyValues();
	}

	public void setEmptyValues() {
		pk_id = -1;
		type_id = "";
		type_name = "";
		screen_id = "";
		screen_name = "";
		section_id = "";
		section_name = "";
		section_value = "";
	}

	public void setDefaultValues() {
		pk_id = -1;
		type_id = MainContainer.mObjectFacility.type_id;
		type_name = MainContainer.mObjectFacility.type_name;
		screen_id = "";
		screen_name = "";
		section_id = "";
		section_name = "";
		section_value = "";
	}

	public ClassScreenData(ClassScreenData copyObject) {
		super();
		this.pk_id = copyObject.pk_id;
		this.type_id = copyObject.type_id;
		this.type_name = copyObject.type_name;
		this.screen_id = copyObject.screen_id;
		this.screen_name = copyObject.screen_name;
		this.section_id = copyObject.section_id;
		this.section_name = copyObject.section_name;
		this.section_value = copyObject.section_value;
	}

}