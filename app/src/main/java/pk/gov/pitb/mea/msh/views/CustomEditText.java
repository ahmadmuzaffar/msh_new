package pk.gov.pitb.mea.msh.views;

import org.xmlpull.v1.XmlPullParser;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TableRow;

public class CustomEditText extends EditText {

	private int colorBackground;
	private int colorText;

	public CustomEditText() {
		super(MainContainer.mContext, ATTRIBUTE_SET);
		this.colorBackground = MainContainer.mContext.getResources().getColor(R.color.row_body_background_single);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
	}

	public CustomEditText(boolean bIsOdd) {
		super(MainContainer.mContext, ATTRIBUTE_SET);
		// this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
		// R.color.row_body_background_even);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
		this.changeBackground(bIsOdd);
	}

	public CustomEditText(boolean bIsOdd, String text) {
		super(MainContainer.mContext, ATTRIBUTE_SET);
		// this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
		// R.color.row_body_background_even);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
		this.setText(text);
		this.changeBackground(bIsOdd);

		if (text.equals("")) {
			this.setInputTypeNumberEditable();
		} else {
			try {
				Integer.parseInt(text);
				this.setInputTypeNumberNonEditable();
			} catch (Exception e) {
				this.setInputTypeTextNonEditable();
			}
		}
	}

	public CustomEditText(String text) {
		super(MainContainer.mContext, ATTRIBUTE_SET);
		this.colorBackground = MainContainer.mContext.getResources().getColor(R.color.row_heading_background);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_heading_text);
		this.setText(text);
		this.setInputTypeTextNonEditable();
		this.setGravity(Gravity.CENTER);
	}

	@Override
	public void setEnabled(boolean enabled) {
		// super.setEnabled(enabled);
		setClickable(enabled);
		setFocusable(enabled);
		setFocusableInTouchMode(enabled);
	}

	public void changeBackground(boolean bIsOdd) {
		this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		setBackgroundColor(colorBackground);
	}

	public void customiseEditText(TableRow.LayoutParams paramsEdittext) {
		try {
			setLayoutParams(paramsEdittext);
			setBackgroundColor(colorBackground);
			setTextColor(colorText);
			setPadding(10, 10, 10, 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customiseEditText(TableRow.LayoutParams paramsEdittext,String value) {
		try {
			setLayoutParams(paramsEdittext);
			setBackgroundColor(colorBackground);
			setTextColor(colorText);
			setPadding(5, 5, 5, 5);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setInputTypeNumberNonEditable() {
		setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
		setGravity(Gravity.CENTER);
		setEnabled(false);
		setHint("Enter Number");
	}

	public void setInputTypeTextNonEditable() {
		setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		setGravity(Gravity.CENTER_VERTICAL);
		setEnabled(false);
	}
	public void setInputTypeNumberEditableWithLimit(int maxLimit) {
		setInputTypeNumberEditable(maxLimit);
		setHint("Enter Number");
	}
	public void setInputTypeNumberEditable() {
		setInputTypeNumberEditable(7);
		setHint("Enter Number");
	}

	public void setInputTypeNumberEditable(int limit) {
		setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
		setGravity(Gravity.CENTER);
		setFilters(new InputFilter[] { new InputFilter.LengthFilter(limit) });
	}

	public void setInputTypeTextEditableNoLimit() {
		setInputTypeTextEditable(-1);
	}

	public void setInputTypeTextEditable() {
		setInputTypeTextEditable(50);
	}

	private void setInputTypeTextEditable(int limit) {
		setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		setGravity(Gravity.CENTER_VERTICAL);
		InputFilter inputFilterSpecialCharacters = new InputFilter() {

			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				for (int i = start; i < end; i++) {
					if (!Character.isLetterOrDigit(source.charAt(i)) && !Character.isWhitespace(source.charAt(i))) {
						return "";
					}
				}
				return null;
			}
		};
		if (limit == -1) {
			setFilters(new InputFilter[] { inputFilterSpecialCharacters });
		} else {
			setFilters(new InputFilter[] { new InputFilter.LengthFilter(limit), inputFilterSpecialCharacters });
		}
	}

	public void animateError(String errorMessage) {
		Animation shake = AnimationUtils.loadAnimation(MainContainer.mContext, R.anim.shake);
		startAnimation(shake);
		setError(errorMessage);
	}

	public void setError(String errorMessage) {
		String stringHtml = String.format("<font color='%s'; background-color='%s'>%s</font>", "white", "black", errorMessage);
		// setError(Html.fromHtml(stringHtml));
		Drawable errorIcon = getResources().getDrawable(R.drawable.ic_action_error);
		errorIcon.setBounds(new Rect(0, 0, errorIcon.getIntrinsicWidth(), errorIcon.getIntrinsicHeight()));
		setError(Html.fromHtml(stringHtml), errorIcon);
	}

	private static AttributeSet ATTRIBUTE_SET = null;

	public static void initializeAttributeSet(Activity activity, Context context) {
		try {
			int res = context.getResources().getIdentifier("layout_edittext", "layout", activity.getPackageName());
			XmlPullParser parser = context.getResources().getXml(res);
			int state = 0;
			do {
				try {
					state = parser.next();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (state == XmlPullParser.START_TAG) {
					if (parser.getName().equals("EditText")) {
						ATTRIBUTE_SET = Xml.asAttributeSet(parser);
						break;
					}
				}
			} while (state != XmlPullParser.END_DOCUMENT);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}