package pk.gov.pitb.mea.msh.httpworker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesOfflineData {
    private final String FM_SYNC_DATE_TIME = "dd-MM-yyyy HH:mm:ss";
    /* SHARED PREFERENCES CONSTANTS */
    private final String PREF_FILE_SYNC = "FileSync";
    private final String PREF_PENDING_INDENT = "isIntentPending";
    private final String PREF_IS_SYNCING = "IsSyncing";
    private final String PREF_DATA_OUTDATED = "IsDataOutdated";
    private final String PREF_UPDATE_CHECKED = "UpdateChecked";
    private final String PREF_PLAYSTORE_VERSION = "PlayStoreVersion";
    private Context context = null;
    private Locale locale = null;
    
    public SharedPreferencesOfflineData(Context context) {
	this.context = context;
	this.locale = Locale.ENGLISH;
    }
    
    public boolean setIsIntentPending(String isIntentPending) {
	return putStringValue(PREF_PENDING_INDENT, isIntentPending);
    }
    
    public boolean getIsIntentPending() {
	try {
	    return getStringValue(PREF_PENDING_INDENT, "false").equals("true") ? true : false;
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }
    
    public boolean setIsDataOutdated(String isDataOutdated) {
	return putStringValue(PREF_DATA_OUTDATED, isDataOutdated);
    }
    
    public boolean setIsSyncing(String isSyncing) {
	return putStringValue(PREF_IS_SYNCING, isSyncing);
    }
    
    public boolean getIsSyncing() {
	try {
	    return getStringValue(PREF_IS_SYNCING, "false").equals("false") ? false : true;
	} catch (Exception e) {
	    e.printStackTrace();
	    return true;
	}
    }
    
    public boolean setPlayStoreVersion(String playStoreVersion) {
	return putStringValue(PREF_PLAYSTORE_VERSION, playStoreVersion);
    }
    
    private boolean putStringValue(String keys, String values) {
	try {
	    SharedPreferences.Editor editor = context.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
	    editor.putString(keys, values);
	    return editor.commit();
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }
    
    // private boolean putStringValues(String[] keys, String[] values) {
    // try {
    // SharedPreferences.Editor editor = context.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE).edit();
    // for (int i = 0; i < keys.length; i++) {
    // editor.putString(keys[i], values[i]);
    // }
    // return editor.commit();
    // } catch (Exception e) {
    // e.printStackTrace();
    // return false;
    // }
    // }
    private String getStringValue(String key, String defaultValue) {
	try {
	    SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_SYNC, Context.MODE_PRIVATE);
	    return sharedPreferences.getString(key, defaultValue);
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
    
    public boolean isUpdateNeeded() {
	try {
	    SimpleDateFormat sdf = new SimpleDateFormat(FM_SYNC_DATE_TIME, locale);
	    String currentDateTime = sdf.format(Calendar.getInstance().getTime());
	    Date currentDate = sdf.parse(currentDateTime);
	    String lastSyncDateTime = getStringValue(PREF_UPDATE_CHECKED, "");
	    if (lastSyncDateTime.length() > 0) {
		Date lastSyncDate = sdf.parse(lastSyncDateTime);
		long diff = currentDate.getTime() - lastSyncDate.getTime();
		if ((diff / (12 * 60 * 60 * 1000)) > 1) {
		    return true;
		}
	    } else {
		return true;
	    }
	    return false;
	} catch (Exception e) {
	    e.printStackTrace();
	    return true;
	}
    }
    
    public boolean setLastUpdate() {
	try {
	    SimpleDateFormat sdf = new SimpleDateFormat(FM_SYNC_DATE_TIME, locale);
	    return putStringValue(PREF_UPDATE_CHECKED, sdf.format(Calendar.getInstance().getTime()));
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }
}