package pk.gov.pitb.mea.msh.asynctasks;

import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.handlers.HandlerDataSync;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.httpworker.HttpRequestSyncApplication;
import android.app.ProgressDialog;
import android.os.AsyncTask;

public class AsyncTaskSyncApplication extends AsyncTask<Void, Void, Void> {

	private HandlerDataSync handlerDataSync;
	private HandlerAction handlerActionStructure;
	private HandlerAction handlerActionFacility;
	private HttpRequestSyncApplication httpRequestSyncApplication;
	private ProgressDialog uProgressDialog;

	public AsyncTaskSyncApplication(HandlerDataSync handlerDataSync) {
		super();
		this.handlerDataSync = handlerDataSync;
		httpRequestSyncApplication = new HttpRequestSyncApplication(MainContainer.mContext);
	}

	public void execute() throws Exception {
		execute((Void) null);
	}

	@Override
	protected void onPreExecute() {
		try {
			uProgressDialog = new ProgressDialog(MainContainer.mContext);
			uProgressDialog.setCancelable(false);
			uProgressDialog.setTitle("Syncing Application");
			uProgressDialog.setMessage("Please Wait...");
			uProgressDialog.setIndeterminate(true);
			uProgressDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			handlerActionStructure = httpRequestSyncApplication.httpRequestSyncStructure();
			if (handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_SUCCESS || handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_ERROR) {
				handlerActionFacility = httpRequestSyncApplication.httpRequestSyncFacilities();
			} else {
				handlerActionFacility = new HandlerAction();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		try {
			SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
			if (handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_EXCEPTION || handlerActionFacility.handlerActionCode == Constants.AC_HTTP_REQUEST_EXCEPTION) {
				MainContainer.mDbAdapter.resetApplication();
				sharedPreferencesEditor.resetVersions();
				handlerDataSync.onSyncError(null);
			} else if (handlerActionStructure.handlerActionCode == Constants.AC_INTERNET_NOT_CONNECTED || handlerActionFacility.handlerActionCode == Constants.AC_INTERNET_NOT_CONNECTED) {
				if (handlerActionFacility.handlerActionCode == Constants.AC_INTERNET_NOT_CONNECTED) {
					MainContainer.mDbAdapter.resetApplication();
					sharedPreferencesEditor.resetVersions();
				}
				handlerDataSync.onNoInternet(false);
			} else if (handlerActionStructure.handlerActionCode == Constants.AC_INTERNET_NOT_WORKING || handlerActionFacility.handlerActionCode == Constants.AC_INTERNET_NOT_WORKING) {
				if (handlerActionFacility.handlerActionCode == Constants.AC_INTERNET_NOT_WORKING) {
					MainContainer.mDbAdapter.resetApplication();
					sharedPreferencesEditor.resetVersions();
				}
				handlerDataSync.onNoInternet(true);
			} else if (handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_NO_DATA || handlerActionFacility.handlerActionCode == Constants.AC_HTTP_REQUEST_NO_DATA) {
				MainContainer.mDbAdapter.resetApplication();
				sharedPreferencesEditor.resetVersions();
				handlerDataSync.onSyncSuccessful(false, "No Data Found");
			} else if (handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_ERROR || handlerActionFacility.handlerActionCode == Constants.AC_HTTP_REQUEST_ERROR) {
				if (handlerActionStructure.handlerErrorCode == Constants.ERROR_CODE_STRUCTURE_VERSION_UP_TO_DATE
						&& handlerActionFacility.handlerErrorCode == Constants.ERROR_CODE_FACILITY_VERSION_UP_TO_DATE) {
					handlerDataSync.onSyncSuccessful(true, "Application is Already Up to Date");
				} else if (handlerActionStructure.handlerErrorCode == Constants.ERROR_CODE_STRUCTURE_VERSION_UP_TO_DATE) {
					handlerDataSync.onSyncSuccessful(true, "Application Successfully Updated");
				} else if (handlerActionFacility.handlerErrorCode == Constants.ERROR_CODE_FACILITY_VERSION_UP_TO_DATE) {
					handlerDataSync.onSyncSuccessful(true, "Application Successfully Updated");
				} else {
					handlerDataSync.onSyncError(handlerActionStructure);
				}
			} else if (handlerActionStructure.handlerActionCode == Constants.AC_HTTP_REQUEST_SUCCESS || handlerActionFacility.handlerActionCode == Constants.AC_HTTP_REQUEST_SUCCESS) {
				handlerDataSync.onSyncSuccessful(true, "Synced Successfully");
			}
		} catch (Exception e) {
			handlerDataSync.onSyncError(null);
			e.printStackTrace();
		}
		if (uProgressDialog != null && uProgressDialog.isShowing()) {
			uProgressDialog.dismiss();
		}
	}
}