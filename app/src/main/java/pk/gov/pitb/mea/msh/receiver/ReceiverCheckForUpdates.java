package pk.gov.pitb.mea.msh.receiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.helpers.AlarmManagerReceiver;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.httpworker.HttpRequestGetPlayStoreVersion;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class ReceiverCheckForUpdates extends BroadcastReceiver {

	private Context mContext;
	private SharedPreferencesEditor spEditor;
	private SimpleDateFormat sdf;

	private synchronized boolean isCheckingForUpdateInProgress(boolean bIsStart, Context context) {
		if (spEditor == null) {
			spEditor = new SharedPreferencesEditor(context);
			sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP, Locale.ENGLISH);
		}
		if (!bIsStart) {
			spEditor.setIsCheckingForUpdate("false");
			spEditor.setLastCheckUpdateDate(sdf.format(Calendar.getInstance().getTime()));
			AlarmManagerReceiver alarmManagerReceiver = new AlarmManagerReceiver(MainContainer.mContext);
			alarmManagerReceiver.setTimeTrigger();
			return true;
		} else {
			boolean bIsRunning = !spEditor.getIsCheckingForUpdate();
			bIsRunning = bIsRunning ? spEditor.setIsCheckingForUpdate("true") : false;
			if (bIsRunning && spEditor.getLastCheckUpdateDate().length() == 0) {
				spEditor.setLastCheckUpdateDate(sdf.format(Calendar.getInstance().getTime()));
			}
			return bIsRunning;
		}
	}

	private boolean isNetConnected(Context context) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnected()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (isNetConnected(context) && isCheckingForUpdateInProgress(true, context)) {
			mContext = context;
			if (spEditor == null) {
				spEditor = new SharedPreferencesEditor(mContext);
				sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP, Locale.ENGLISH);
			}
			try {
				startThreadOnNetConnected();
			} catch (Exception e) {
				e.printStackTrace();
				isCheckingForUpdateInProgress(false, mContext);
			}
		}
	}

	private void startThreadOnNetConnected() throws Exception {
		new Thread(new Runnable() {

			@Override
			public void run() {
				int notificationID = 111;
				NotificationCompat.Builder notificationBuilder = null;
				NotificationManager notificationManager = null;
				try {
					HttpRequestGetPlayStoreVersion getPlayStoreVersion = new HttpRequestGetPlayStoreVersion(mContext);
					String result = getPlayStoreVersion.getPlayStoreVersion();
					if (result != null && result.length() > 0) {
						SharedPreferencesEditor spEditor = new SharedPreferencesEditor(mContext);
						spEditor.setAppPlayStoreVersion(result);
						if (spEditor.isAppUpdateNeeded(mContext.getResources().getString(R.string.app_version_name))) {
							notificationID = 778;
							notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
							notificationBuilder = new NotificationCompat.Builder(mContext);
							notificationBuilder.setContentTitle("Update Available");
							notificationBuilder.setSmallIcon(R.drawable.ic_launcher_small);
							notificationBuilder.setAutoCancel(true);

							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName()));
							PendingIntent pendingIntent = PendingIntent.getActivity(mContext, notificationID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
							notificationBuilder.setContentIntent(pendingIntent);

							if (spEditor.isAppForceUpdateNeeded(mContext.getResources().getString(R.string.app_version_name))) {
								notificationBuilder.setContentText("A newer version of application is available. Please update you app to proceed");
								notificationBuilder.setOngoing(true);
							} else {
								notificationBuilder.setContentText("A newer version of application is available. Please update you app");
								notificationBuilder.setOngoing(false);
							}
						}
					}
					notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
					notificationManager.notify(notificationID, notificationBuilder.build());
				} catch (Exception e) {
					e.printStackTrace();
					if (notificationManager != null) {
						notificationManager.cancel(notificationID);
					}
				}
				isCheckingForUpdateInProgress(false, mContext);
			}
		}).start();
	}
}
