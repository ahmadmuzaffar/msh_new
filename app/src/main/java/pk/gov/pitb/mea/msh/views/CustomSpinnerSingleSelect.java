package pk.gov.pitb.mea.msh.views;

import java.util.ArrayList;
import java.util.Arrays;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinner;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerItem;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableRow;

public class CustomSpinnerSingleSelect extends Spinner {

	private boolean bIsOdd = false;
	private int drawableBackgroundEnable = 0;
	private int drawableBackgroundDisable = 0;
	private ArrayList<String> arrayListValuesString;
	private ArrayList<ClassScreenItem> arrayListValuesItem;

	public CustomSpinnerSingleSelect(boolean bIsOdd, int type) {
		super(MainContainer.mContext);
		changeBackground(bIsOdd, type);
	}

	public void changeBackground(boolean bIsOdd, int type) {
		this.bIsOdd = bIsOdd;
		switch (type) {
			case 1:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_1_1 : R.drawable.spinner_1_1;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_1_1 : R.drawable.spinner_1_1;
			break;
			case 2:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_2 : R.drawable.spinner_even_enable_2;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_2 : R.drawable.spinner_even_disable_2;
			break;
			case 3:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_3 : R.drawable.spinner_even_enable_3;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_3 : R.drawable.spinner_even_disable_3;
			break;
			case 4:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_4 : R.drawable.spinner_even_enable_4;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_4 : R.drawable.spinner_even_disable_4;
			break;
			case 5:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_5 : R.drawable.spinner_even_enable_5;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_5 : R.drawable.spinner_even_disable_5;
			break;

			default:
			break;
		}
		if (isEnabled()) {
			setBackgroundResource(drawableBackgroundEnable);
		} else {
			setBackgroundResource(drawableBackgroundDisable);
		}
		try {
			SpinnerAdapter spinnerAdapter = getAdapter();
			if (spinnerAdapter != null) {
				if (spinnerAdapter instanceof ArrayAdapterSpinner) {
					((ArrayAdapterSpinner) spinnerAdapter).changeBackground(bIsOdd);
				} else if (spinnerAdapter instanceof ArrayAdapterSpinnerItem) {
					((ArrayAdapterSpinnerItem) spinnerAdapter).changeBackground(bIsOdd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customiseSpinner(TableRow.LayoutParams paramsSpinner) {
		try {
			setLayoutParams(paramsSpinner);
			setPadding(0, 0, (int) (paramsSpinner.width * 0.2), 0);
			setBackgroundResource(drawableBackgroundEnable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setAdapter(String[] listValues) {
		setAdapter((ArrayList<String>) Arrays.asList(listValues));
	}

	public void setAdapter(ArrayList<String> listValues) {
		try {
			arrayListValuesString = new ArrayList<String>(listValues);
			ArrayAdapterSpinner arrayAdapter = new ArrayAdapterSpinner(MainContainer.mContext, android.R.layout.simple_list_item_1, listValues, bIsOdd);
			arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setSelectionById(String value) {
		try {
			if (arrayListValuesString != null) {
				super.setSelection(arrayListValuesString.indexOf(value));
			}
			if (arrayListValuesItem != null) {
				for (int i = 0; i < arrayListValuesItem.size(); i++) {
					ClassScreenItem object = arrayListValuesItem.get(i);
					if (object.item_id.equalsIgnoreCase(value)) {
						super.setSelection(i);
						return;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setSelectionByValue(String value) {
		try {
			if (arrayListValuesString != null) {
				super.setSelection(arrayListValuesString.indexOf(value));
			}
			if (arrayListValuesItem != null) {
				for (int i = 0; i < arrayListValuesItem.size(); i++) {
					ClassScreenItem object = arrayListValuesItem.get(i);
					if (object.item_name.equalsIgnoreCase(value)) {
						super.setSelection(i);
						return;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getSelectionId() {
		try {
			if (arrayListValuesString != null) {
				return getSelectedItem().toString();
			}
			if (arrayListValuesItem != null) {
				return (arrayListValuesItem.get(getSelectedItemPosition())).item_id;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getSelectionValue() {
		try {
			if (arrayListValuesString != null) {
				return getSelectedItem().toString();
			}
			if (arrayListValuesItem != null) {
				return (arrayListValuesItem.get(getSelectedItemPosition())).item_name;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public void setAdapterItem(ArrayList<ClassScreenItem> listValues) {
		try {
			arrayListValuesItem = new ArrayList<ClassScreenItem>(listValues);
			ArrayAdapterSpinnerItem arrayAdapter = new ArrayAdapterSpinnerItem(MainContainer.mContext, android.R.layout.simple_list_item_1, arrayListValuesItem, bIsOdd);
			arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setAdapterItem(ArrayList<ClassScreenItem> listValues, ArrayAdapterSpinnerItem arrayAdapter) {
		try {
			arrayListValuesItem = new ArrayList<ClassScreenItem>(listValues);
			setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}