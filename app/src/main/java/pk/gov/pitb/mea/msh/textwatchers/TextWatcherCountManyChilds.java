package pk.gov.pitb.mea.msh.textwatchers;

import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

public class TextWatcherCountManyChilds {

	private boolean parentToChildClear;
	private EditText editTextParent = null;
	private EditText[] arrayEditTextChildsEditable = null;
	private EditText editTextChildAutoFill = null;
	private String labelEditTextParent = null;
	private String[] arrayLabelsChilds = null;
	private final String errorMessageCountExceed = "%s cannot be more than %s";
	private final String errorMessageCountParentEmpty = "%s cannot be entered before %s";
	private boolean childCountLessNotMoreThanMaxCount;
	
	public TextWatcherCountManyChilds(EditText editTextParent, EditText editTextChildAutoFill, String labelEditTextParent, EditText[] arrayEditTextChildsEditable, String[] arrayLabelsChilds,boolean childCountLessNotMoreThanMaxCount ) {
		this.editTextParent = editTextParent;
		this.editTextChildAutoFill = editTextChildAutoFill;
		this.labelEditTextParent = labelEditTextParent;
		this.arrayEditTextChildsEditable = arrayEditTextChildsEditable;
		this.arrayLabelsChilds = arrayLabelsChilds;
		this.childCountLessNotMoreThanMaxCount = childCountLessNotMoreThanMaxCount;
	}

	public void setTextWatcherChilds() {
		for (int i = 0; i < arrayEditTextChildsEditable.length; i++) {
			arrayEditTextChildsEditable[i].addTextChangedListener(new TextWatcherMaxCount(arrayEditTextChildsEditable[i], arrayLabelsChilds[i]));
		}
	}
	public void setTextWatcherParent(EditText editTextMaxCounterChild) {
		editTextParent.addTextChangedListener(new TextWatcherParent(editTextParent,labelEditTextParent,editTextMaxCounterChild));
	}
	private class TextWatcherMaxCount implements TextWatcher {

		private EditText editTextCurrent = null;
		private String nameEditTextCurrent = null;
		
		
		public TextWatcherMaxCount(EditText editTextCurrent, String nameEditTextCurrent) {
			this.editTextCurrent = editTextCurrent;
			this.nameEditTextCurrent = nameEditTextCurrent;
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			int indexCurrent = 0;
			int filledCount  = 0; 
			String text = editTextParent.getText().toString().trim();
			if (text.length() > 0 && !parentToChildClear) {
				text = editTextCurrent.getText().toString().trim();
				boolean bIsAllFilled = true;
				final int max = Integer.parseInt(editTextParent.getText().toString());
				int total = 0;
				int current = text.length() > 0 ? Integer.parseInt(text) : 0;
				for (EditText etChild : arrayEditTextChildsEditable) {
					text = etChild.getText().toString().trim();
					if (text.length() > 0) {
						total += Integer.parseInt(text);
						filledCount ++;
					} else {
						bIsAllFilled = false;
					}
				}
				if (editTextChildAutoFill != null) {
					if (bIsAllFilled) {
						text = editTextChildAutoFill.getText().toString().trim();
						if (text.length() > 0) {
							total += Integer.parseInt(text);
						}
					} else {
						editTextChildAutoFill.setText("");
					}
				}
				if (total > max) {
					Toast.makeText(MainContainer.mContext, String.format(MainContainer.mLocale, errorMessageCountExceed, nameEditTextCurrent, labelEditTextParent), Toast.LENGTH_SHORT).show();
					current = max - total + current;
					editTextCurrent.setText("" + current);
					editTextCurrent.setSelection(editTextCurrent.getText().toString().length());
				}else if (total == max) {
					for (EditText etChild : arrayEditTextChildsEditable) {
						text = etChild.getText().toString().trim();
						if (text.length() == 0) {
							etChild.setText("0");
						}
					}
				}else if(!childCountLessNotMoreThanMaxCount && editTextCurrent.getText().toString().trim().length() > 0) {
					current = max - total;
					for (EditText etChild : arrayEditTextChildsEditable) {
						text = etChild.getText().toString().trim();
						if (text.length() == 0 && filledCount ==2 && !etChild.equals(editTextCurrent)) {
							etChild.setText("" + current);
							etChild.setSelection(etChild.getText().toString().length());
						}
					}
				}else if(!childCountLessNotMoreThanMaxCount && editTextCurrent.getText().toString().trim().length() == 0) {
					for (EditText etChild : arrayEditTextChildsEditable) {
						text = etChild.getText().toString().trim();
						if (text.length() > 0) {
							etChild.setText("");

						}
					}

				}
			} else if (editTextCurrent.getText().toString().trim().length() > 0) {
				Toast.makeText(MainContainer.mContext, String.format(MainContainer.mLocale, errorMessageCountParentEmpty, arrayLabelsChilds[indexCurrent], labelEditTextParent), Toast.LENGTH_SHORT)
						.show();
				editTextCurrent.setText("");
			}
		}

		public void afterTextChanged(Editable s) {
		}
	}
	private class TextWatcherParent implements TextWatcher {

		private EditText editTextParent;
		private String nameEditTextCurrent = null;
		private EditText editTextMaxCounterChild= null;
		

		public TextWatcherParent(EditText editTextParent,String nameEditTextCurrent,EditText editTextMaxCounterChild) {
			this.editTextParent = editTextParent;
			this.nameEditTextCurrent = nameEditTextCurrent;
			this.editTextMaxCounterChild = editTextMaxCounterChild;
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			parentToChildClear =true;
			String text = editTextParent.getText().toString().trim();
			for (EditText etChild : arrayEditTextChildsEditable) {
				if (text.length() > 0 && text.equals("0")) {
					etChild.setText("0");
				} else {
					etChild.setText("");
				}
			}
			if (text.length() > 0 && text.equals("0")) {
				editTextMaxCounterChild.setText("0");
			} else {
				editTextMaxCounterChild.setText("");
			}
			parentToChildClear =false;
		}
		public void afterTextChanged(Editable s) {
		}
	}

}