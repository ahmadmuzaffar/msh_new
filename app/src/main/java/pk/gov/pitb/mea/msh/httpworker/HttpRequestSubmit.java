package pk.gov.pitb.mea.msh.httpworker;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Locale;

import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.ImageUtils;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.models.ClassDataPicture;

public class HttpRequestSubmit {

    public HandlerAction httpRequestSubmitPicture(ClassDataPicture object) {
        HandlerAction handlerAction = new HandlerAction();
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams httpParams = httpClient.getParams();
            httpParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5 * 60 * 1000);
            httpParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 2 * 60 * 1000);
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httpPost = new HttpPost(Constants.URL_UPLOAD_PICTURE);
            multipartEntity.addPart("picture_name", new StringBody(object.picture_parameter));
            multipartEntity.addPart("picture_data", new StringBody(ImageUtils.getInstance().encodeToBase64String(object.picture_data)));
            httpPost.setEntity(multipartEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            String apiResult = EntityUtils.toString(httpResponse.getEntity());
            parseResponseSubmit(apiResult, handlerAction);
        } catch (SocketException e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
        } catch (Exception e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_EXCEPTION;
        }
        return handlerAction;
    }

    public HandlerAction httpRequestSubmitData(ClassDataActivity object) {
        @SuppressWarnings("unused")
        String data = object.json_data.toString();
        HandlerAction handlerAction = new HandlerAction();
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpParams httpParams = httpClient.getParams();
            httpParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5 * 60 * 1000);
            httpParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 2 * 60 * 1000);
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpPost httpPost = null;
            if (object.activity_type.equals(Constants.AT_HEALTH_COUNCIL)) {
                httpPost = new HttpPost(Constants.URL_SUBMIT_HEALTH_COUNCIL);
            } else if (object.activity_type.equals(Constants.AT_NUTRIENTS)) {
                httpPost = new HttpPost(Constants.URL_SUBMIT_NUTRIENTS);
            } else if (object.activity_type.equals(Constants.AT_HEPATITIS)) {
                httpPost = new HttpPost(Constants.URL_SUBMIT_HEPATITIS);
            } else if (object.activity_type.equals(Constants.AT_PATIENT_INTERVIEWS)) {
                httpPost = new HttpPost(Constants.URL_SUBMIT_PATIENT_INTERVIEW);
            } else {
                httpPost = new HttpPost(Constants.URL_SUBMIT_MONITORING);
            }


            multipartEntity.addPart("data_string", new StringBody(object.json_data));
            if (object.arrayListPictures != null) {
                for (ClassDataPicture picture : object.arrayListPictures) {
                    if (picture.picture_data != null && picture.picture_data.length > 0 && !picture.picture_status.equals(Constants.PICTURE_STATUS_SENT)) {
                        handlerAction = httpRequestSubmitPicture(picture);
                        if (handlerAction.handlerActionCode == Constants.AC_HTTP_REQUEST_SUCCESS) {
                            picture.picture_status = Constants.PICTURE_STATUS_SENT;
                        } else {
                            return handlerAction;
                        }
                    }
                }
            }

            httpPost.setEntity(multipartEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            String apiResult = EntityUtils.toString(httpResponse.getEntity());
            parseResponseSubmit(apiResult, handlerAction);
        } catch (SocketException e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
        } catch (Exception e) {
            e.printStackTrace();
            handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_EXCEPTION;
        }
        return handlerAction;
    }

    private void parseResponseSubmit(String apiResponse, HandlerAction handlerAction) throws Exception {
        JSONObject jsonObject = new JSONObject(apiResponse);
        String status = jsonObject.has("status") ? jsonObject.getString("status").toLowerCase(Locale.ENGLISH) : "";

        if (jsonObject.has("message")) {
            handlerAction.handlerActionString = jsonObject.getString("message");
        } else if (jsonObject.has("msg")) {
            handlerAction.handlerActionString = jsonObject.getString("msg");
        } else if (jsonObject.has("error")) {
            handlerAction.handlerActionString = jsonObject.getString("error");
        } else {
            handlerAction.handlerActionString = "";
        }

        if (jsonObject.has("error_code")) {
            handlerAction.handlerErrorCode = Integer.parseInt(jsonObject.getString("error_code"));
        } else {
            handlerAction.handlerErrorCode = -1;
        }

        if (status.equals("success")) {
            handlerAction.handlerActionString = handlerAction.handlerActionString.length() > 0 ? handlerAction.handlerActionString : "Facility Information Added Successfully";
            handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_SUCCESS;
        } else {
            handlerAction.handlerActionString = handlerAction.handlerActionString.length() > 0 ? handlerAction.handlerActionString : "Error Occured, Activity Saved\nPlease Try Again Later";
            handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_ERROR;
        }
    }
}