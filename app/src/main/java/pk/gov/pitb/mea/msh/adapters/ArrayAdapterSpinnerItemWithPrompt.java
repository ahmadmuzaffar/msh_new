package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpinnerItemWithPrompt extends ArrayAdapter<ClassScreenItem> {

	public ArrayAdapterSpinnerItemWithPrompt(Context context, int resource, ClassScreenItem[] objects) {
		super(context, resource, objects);
	}

	public ArrayAdapterSpinnerItemWithPrompt(Context context, int resource, ArrayList<ClassScreenItem> objects) {
		super(context, resource, objects);
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		if (textView != null) {
			textView.setTextColor(Color.BLACK);
			if (position == 0) {
				textView.setHint(textView.getText().toString());
				textView.setText("");
				ClassScreenItem object = getItem(position);
				textView.setText(object.item_name);
			}
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(Color.BLACK);
			ClassScreenItem object = getItem(position);
			textView.setText(object.item_name);
			// if (position == 0) {
			// textView.setHint(textView.getText().toString());
			// textView.setText("");
			// }
		}
		return view;
	}

	@Override
	public boolean isEnabled(int position) {
		return position == 0 ? false : true;
	}
}