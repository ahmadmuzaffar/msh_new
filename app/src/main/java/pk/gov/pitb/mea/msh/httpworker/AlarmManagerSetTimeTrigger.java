package pk.gov.pitb.mea.msh.httpworker;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmManagerSetTimeTrigger {

	private final int REQUEST_CODE = 10;
	private final int TIME_DAY = 7;
	private final int TIME_NIGHT = 19;
	private Context mContext = null;
	private SharedPreferencesOfflineData mSharedPreferencesOfflineData = null;

	public AlarmManagerSetTimeTrigger(Context mContext, SharedPreferencesOfflineData mSharedPreferencesOfflineData) {
		this.mContext = mContext;
		this.mSharedPreferencesOfflineData = mSharedPreferencesOfflineData;
	}

	public void setTimeTrigger() {
		try {
			Calendar calNow = Calendar.getInstance();
			Calendar calSet = (Calendar) calNow.clone();
			if (calNow.get(Calendar.HOUR_OF_DAY) > TIME_DAY && calNow.get(Calendar.HOUR_OF_DAY) < TIME_NIGHT) {
				calSet.set(Calendar.HOUR_OF_DAY, TIME_NIGHT);
			} else {
				calSet.set(Calendar.HOUR_OF_DAY, TIME_DAY);
			}
			calSet.set(Calendar.MINUTE, 0);
			calSet.set(Calendar.SECOND, 0);
			calSet.set(Calendar.MILLISECOND, 0);
			if (calSet.compareTo(calNow) <= 0) {
				calSet.add(Calendar.DATE, 1);
			}
			if (mSharedPreferencesOfflineData.getIsIntentPending()) {
				removePreviousIntent();
			}
			Intent nextIntent = new Intent(mContext, BroadcastReceiverCheckUpdates.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, REQUEST_CODE, nextIntent, 0);
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.set(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(), pendingIntent);
			mSharedPreferencesOfflineData.setIsIntentPending("true");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean removePreviousIntent() {
		try {
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			Intent removeIntent = new Intent(mContext, BroadcastReceiverCheckUpdates.class);
			PendingIntent pendingUpdateIntent = PendingIntent.getBroadcast(mContext, REQUEST_CODE, removeIntent, PendingIntent.FLAG_NO_CREATE);
			alarmManager.cancel(pendingUpdateIntent);
			mSharedPreferencesOfflineData.setIsIntentPending("false");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}