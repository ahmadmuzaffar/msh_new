package pk.gov.pitb.mea.msh;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.asynctasks.AsyncTaskSendData;
import pk.gov.pitb.mea.msh.dialogs.DialogInformation;
import pk.gov.pitb.mea.msh.dialogs.DialogUnsentActivities;
import pk.gov.pitb.mea.msh.handlers.HandlerActivityCallHealthCouncil;
import pk.gov.pitb.mea.msh.handlers.HandlerFragmentCallBackHealthCouncil;
import pk.gov.pitb.mea.msh.healthcouncil.HealthCouncilPage1;
import pk.gov.pitb.mea.msh.healthcouncil.HealthCouncilPage2;
import pk.gov.pitb.mea.msh.healthcouncil.HealthCouncilPage3;
import pk.gov.pitb.mea.msh.healthcouncil.HealthCouncilPage4;
import pk.gov.pitb.mea.msh.healthcouncil.HealthCouncilPage5;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.LocationTracker;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;


public class ActivityMainHealthCouncil extends Activity implements HandlerActivityCallHealthCouncil {

    /*private Globals MainContainer;*/
    private LocationTracker mLocationTracker;
    private DialogInformation dialogFacilityIncharge;
    private EditText titleFragment;
    private Fragment fragmentCurrent;
    private int fragmentPositionCurrent;
    private boolean bSaveData;

    @Override
    protected void onResume() {
        super.onResume();
        /*MainContainer = Globals.getUsage(ActivityMainHealthCouncil.this);*/
        MainContainer.setContextOnResume(ActivityMainHealthCouncil.this);
        mLocationTracker.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationTracker.onStart();
    }

    @Override
    public void onBackPressed() {
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
        String json = pk.gov.pitb.mea.msh.helpers.MainContainer.mJsonObjectFormData.toString();
        if (json.equals(pk.gov.pitb.mea.msh.helpers.MainContainer.mSPEditor.getLastSavedJSONHealthCouncil())){
            AlertDialogs.OnDialogClickListener OnYesClick = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    bSaveData = false;
                    finish();
                }
            };
            AlertDialogs.getInstance().showDialogYesNo("Changes will be saved", "Are You Sure You Want to Exit Application?",OnYesClick,
                    null,true);
        }
        else {
            AlertDialogs.OnDialogClickListener onSavedClicked = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveData();
                    bSaveData = false;
                    finish();
                }
            };

            AlertDialogs.OnDialogClickListener onDiscardClicked = new AlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    pk.gov.pitb.mea.msh.helpers.MainContainer.mSPEditor.clearLastSavedDataHealthCouncil();
                    bSaveData = false;
                    finish();
                }
            };

            AlertDialogs.getInstance().showDialogThreeButtons("Unsaved Changes", "Do you want to save data?", "Save and Exit?", onSavedClicked,
                    "Discard Data", onDiscardClicked, "Cancel", null, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationTracker.onDestroy();
        /*if (bSaveData){
            saveData();
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_council);
        initActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_health_council, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_save:
                saveData();
                Toast.makeText(getApplicationContext(), "Data Saved Successfully", Toast.LENGTH_LONG).show();
                return true;
            case R.id.item_sync:
                if (pk.gov.pitb.mea.msh.helpers.MainContainer.isInternetAvailable()){
                    showMessageOnSync();
                } else {
                    AlertDialogs.getInstance().showDialogOK("Internet not Available", "Please connect to Internet to sync application", null, true);
                }
                return true;
            case R.id.item_info:
                dialogFacilityIncharge.showFacilityDialog(null);
                return true;
            case R.id.item_unsent:
                showDialogUnsent();
                return true;
            case R.id.item_exit:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void saveData(){
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
        MainContainer.mSPEditor.setLastFragmentPositionHealthCouncil("" + fragmentPositionCurrent);
        MainContainer.mSPEditor.setLastSavedFacilityHealthCouncil(pk.gov.pitb.mea.msh.helpers.MainContainer.mObjectFacility.getJSON().toString());
        MainContainer.mSPEditor.setLastSavedJSONHealthCouncil(pk.gov.pitb.mea.msh.helpers.MainContainer.mJsonObjectFormData.toString());
    }

    private void initActivity(){
        try {
            /*pk.gov.pitb.mea.msh.helpers.MainContainer = Globals.getUsage(ActivityMainHealthCouncil.this);*/
            MainContainer.initializeOnStartup(ActivityMainHealthCouncil.this); // just for testing
            MainContainer.setContextOnResume(ActivityMainHealthCouncil.this);
            MainContainer.activityCallBackHealthCouncil= ActivityMainHealthCouncil.this;
            mLocationTracker = new LocationTracker(MainContainer.mContext);
            fragmentCurrent = null;
            bSaveData = true;
            initActionBar();
            initBody();
            String json = MainContainer.mSPEditor.getLastSavedJSONHealthCouncil();
            if (json.length() > 0) {

                MainContainer.mJsonObjectFormData = new JSONObject(json);
            } else {
                MainContainer.mJsonObjectFormData = new JSONObject();
            }
            selectItem(Integer.parseInt(pk.gov.pitb.mea.msh.helpers.MainContainer.mSPEditor.getLastFragmentPositionHealthCouncil()));
            ((FrameLayout) findViewById(R.id.frame_layout_health_council)).setVisibility(View.VISIBLE);
            /*pk.gov.pitb.mea.msh.helpers.MainContainer.mObjectFacilityData = pk.gov.pitb.mea.msh.helpers.MainContainer.mDbAdapter
                    .selectFacilityData(MainContainer.mObjectFacility.*//*facility*//*facility_id, MainContainer.mObjectFacility.type_id);
            */
            MainContainer.mObjectFacilityData = MainContainer.mDbAdapter.selectFacilityData(MainContainer.mObjectFacility.facility_id, MainContainer.mObjectFacility.type_id);
            dialogFacilityIncharge = new DialogInformation();
            /* Dialog Facility Line below has been edited for testing purposes */
            //dialogFacilityIncharge = new DialogInformation(MainContainer.mObjectFacilityData.getObjectStaffByRole(Constants.ROLE_FACILITY_INCHARGE));
            if (pk.gov.pitb.mea.msh.helpers.MainContainer.arrayListOpenedActivities != null) {
                pk.gov.pitb.mea.msh.helpers.MainContainer.arrayListOpenedActivities.clear();
            }
            pk.gov.pitb.mea.msh.helpers.MainContainer.arrayListOpenedActivities = null;
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initActionBar(){
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(null);
        actionBar.setBackgroundDrawable(new ColorDrawable(pk.gov.pitb.mea.msh.helpers.MainContainer.mContext.getResources().getColor(R.color.row_heading_background)));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME);
    }

    private void initBody(){
        titleFragment = (EditText) findViewById(R.id.title_fragment_health_council);
        RelativeLayout.LayoutParams lpTitle = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                (int) (pk.gov.pitb.mea.msh.helpers.MainContainer.mScreenWidth * 0.075));
        lpTitle.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lpTitle.setMargins(0, 0, 0, 0);
        titleFragment.setLayoutParams(lpTitle);
        titleFragment.setPadding(18, 0, 18, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fragmentCurrent.onActivityResult(requestCode, resultCode, data);
    }


    private void selectItem(int position) {
        switch (position) {
            case 0:
                fragmentCurrent = new HealthCouncilPage1();
                titleFragment.setText("Health Council Form Page1");
                break;
            case 1:
                fragmentCurrent = new HealthCouncilPage2();
                titleFragment.setText("Health Council Form Page2");
                break;
            case 2:
                fragmentCurrent = new HealthCouncilPage3();
                titleFragment.setText("Health Council Form Page3");
                break;
            case 3:
                fragmentCurrent = new HealthCouncilPage4();
                titleFragment.setText("Health Council Form Page4");
                break;
            case 4:
                fragmentCurrent = new HealthCouncilPage5();
                titleFragment.setText("Health Council Form Page5");
                break;

            default:
                fragmentCurrent = null;
                break;
        }
        if (fragmentCurrent != null) {
            fragmentCurrent.setRetainInstance(true);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_health_council, fragmentCurrent, "Fragment" + fragmentPositionCurrent);
            transaction.addToBackStack("Fragment" + position);
            transaction.commit();
            ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).onFragmentShown();
            fragmentPositionCurrent = position;
        }
    }


    private void showDialogUnsent() {
        ArrayList<ClassDataActivity> listUnsent = pk.gov.pitb.mea.msh.helpers.MainContainer.mDbAdapter.selectAllActivities(Constants.AT_HEALTH_COUNCIL);
        if (listUnsent.size() > 0) {
            DialogUnsentActivities dialogUnsentActivities = new DialogUnsentActivities();
            dialogUnsentActivities.showUnsentDialog(listUnsent);
        } else {
            Toast.makeText(pk.gov.pitb.mea.msh.helpers.MainContainer.mContext, "No Unsent Activity to Display", Toast.LENGTH_LONG).show();
        }
    }

    private void showMessageOnSync() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(pk.gov.pitb.mea.msh.helpers.MainContainer.mContext);
            builder.setTitle("All Entered Data will be Lost");
            builder.setMessage("Are You Sure You want to Sync Application?");
            //builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainContainer./*resetInstance()*/resetMainContainer();
                    Intent intent = new Intent(ActivityMainHealthCouncil.this, ActivityMain.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.setCancelable(false);
            builder.create();
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void showPreviousFragment(int i) {
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
        selectItem(fragmentPositionCurrent - i);
    }

    @Override
    public void showNextFragment() {
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
        selectItem(fragmentPositionCurrent + 1);
    }

    @Override
    public void submitData() {
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
		AlertDialogs.OnDialogClickListener onYesClicked = new AlertDialogs.OnDialogClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					AsyncTaskSendData asyncTaskSendData = new AsyncTaskSendData(mLocationTracker, null);
					asyncTaskSendData.execute();
					bSaveData = false;
					pk.gov.pitb.mea.msh.helpers.MainContainer.mSPEditor.clearLastSavedDataHealthCouncil();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		AlertDialogs.getInstance().showDialogYesNo("Patients Information Complete", "Do you want to send information to server?", onYesClicked, null,
				true);
    }

    @Override
    public void showLastFragment() {
        ((HandlerFragmentCallBackHealthCouncil) fragmentCurrent).parseObject();
        selectItem(4);
    }
}
