package pk.gov.pitb.mea.msh.models;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class ClassPicture {

	public int picture_id, picture_fk;
	public int buttonEnable, buttonDisable, width, height;
	public String pictureName, pictureParameter;
	public ImageButton imageButton;
	public TextView textView;
	public TextView textViewPartner;
	public byte[] byteArrayPicture;
	public int[] margins;
	public EditText editText;
	public EditText editTextPartner;

	public ClassPicture() {
		reset();
	}

	public ClassPicture(int buttonEnable, int buttonDisable, int width, int height, String pictureName, String pictureParamter, int[] margins) {
		this.buttonEnable = buttonEnable;
		this.buttonDisable = buttonDisable;
		this.width = width;
		this.height = height;
		this.pictureName = pictureName;
		this.pictureParameter = pictureParamter;
		this.margins = margins;
		this.imageButton = null;
		this.textView = null;
		this.textViewPartner = null;
		this.byteArrayPicture = null;
	}



	public void reset() {
		buttonEnable = buttonDisable = -1;
		width = height = 0;
		pictureName = pictureParameter = null;
		imageButton = null;
		byteArrayPicture = null;
		margins = null;
	}
}