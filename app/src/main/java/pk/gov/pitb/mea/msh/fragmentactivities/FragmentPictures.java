package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.ImageUtils;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDataPicture;

import static pk.gov.pitb.mea.msh.helpers.Constants.DB_SCREEN_NAME_PICTURES;
import static pk.gov.pitb.mea.msh.helpers.Utilities.getImage;

public class FragmentPictures extends Fragment implements InterfaceFragmentCallBack {

	private static final String FORMAT_TIME_STAMP_PICTURE = "yyyyMMddHHmmss";
	private static final int REQUEST_CODE_PICTURE_1 = 1;
	private static final int REQUEST_CODE_PICTURE_2 = 2;

	private View parentView = null;
	private ImageView imageViewPicture1;
	private ImageView imageViewPicture2;

	private SimpleDateFormat sdfPicture;
	private ClassDataPicture picture1;
	private ClassDataPicture picture2;

	@Override
	public void parseObject() {
		try {
			if (MainContainer.mObjectActivity.arrayListPictures == null) {
				MainContainer.mObjectActivity.arrayListPictures = new ArrayList<ClassDataPicture>();
			}
			MainContainer.mObjectActivity.arrayListPictures.clear();
			MainContainer.mObjectActivity.arrayListPictures.add(picture1);
			MainContainer.mObjectActivity.arrayListPictures.add(picture2);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("picture_1", picture1.picture_parameter + ".jpg");
			jsonObject.put("picture_2", picture2.picture_parameter + ".jpg");
			MainContainer.mJsonObjectFormData.remove(DB_SCREEN_NAME_PICTURES);
			MainContainer.mJsonObjectFormData.put(DB_SCREEN_NAME_PICTURES, jsonObject);

			// Saving Pictures
			MainContainer.mDbAdapter.insertActivitySave(MainContainer.mObjectActivity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_pictures, container, false);
			sdfPicture = new SimpleDateFormat(FORMAT_TIME_STAMP_PICTURE, Locale.ENGLISH);
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void loadSavedData() {
		if (MainContainer.mJsonObjectFormData.has(DB_SCREEN_NAME_PICTURES)){
			try {
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(DB_SCREEN_NAME_PICTURES);
				String pictureJson1 = jsonObject.getString("picture_1");
				String pictureJson2 = jsonObject.getString("picture_2");
				pictureJson1 = pictureJson1.replace(".jpg","");
				pictureJson2 = pictureJson2.replace(".jpg","");

				ClassDataPicture classDataPicture1 = MainContainer.mDbAdapter.selectSavedPictures(pictureJson1);
				ClassDataPicture classDataPicture2 = MainContainer.mDbAdapter.selectSavedPictures(pictureJson2);
				if (classDataPicture1!=null){
					if (classDataPicture1.picture_data != null) {
						Bitmap bitmapPictureTaken = getImage(classDataPicture1.picture_data);
						imageViewPicture1.setImageBitmap(bitmapPictureTaken);
						imageViewPicture1.setScaleType(ImageView.ScaleType.CENTER_CROP);
						picture1 = classDataPicture1;
					}
				}
				if (classDataPicture2!=null){
					if (classDataPicture2.picture_data != null) {
						Bitmap bitmapPictureTaken = getImage(classDataPicture2.picture_data);
						imageViewPicture2.setImageBitmap(bitmapPictureTaken);
						imageViewPicture2.setScaleType(ImageView.ScaleType.CENTER_CROP);
						picture2 = classDataPicture2;
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private void generateBody() {
		try {
			imageViewPicture1 = (ImageView) parentView.findViewById(R.id.imagebutton_picture_1);
			imageViewPicture2 = (ImageView) parentView.findViewById(R.id.imagebutton_picture_2);

			LinearLayout.LayoutParams paramsTakePicture1 = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * .45), (int) (MainContainer.mScreenWidth * .45));
			paramsTakePicture1.setMargins(0, 0, 0, 0);
			picture1 = new ClassDataPicture();
			picture1.picture_name = "picture_1";
			imageViewPicture1.setLayoutParams(paramsTakePicture1);
			imageViewPicture1.setImageResource(R.drawable.imgbtn_take_picture);
			imageViewPicture1.setScaleType(ImageView.ScaleType.FIT_XY);
			imageViewPicture1.setBackgroundColor(Color.TRANSPARENT);
			imageViewPicture1.setPadding(0, 0, 0, 0);
			imageViewPicture1.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					if (picture1.picture_data == null) {
						startActivityForResult(ImageUtils.getInstance().getIntentCamera(), REQUEST_CODE_PICTURE_1);
					} else {
						ImageUtils.getInstance().showDialogPicture(picture1.picture_data, "Picture 1");
					}
				}

			});
			imageViewPicture1.setOnLongClickListener(new View.OnLongClickListener() {

				public boolean onLongClick(View v) {
					if (picture1.picture_data != null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
						builder.setTitle("Delete Taken Picture");
						builder.setMessage("Are you sure you want to delete this Picture?");
						builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
								picture1.picture_parameter = null;
								picture1.picture_data = null;
								imageViewPicture1.setImageResource(R.drawable.imgbtn_take_picture);
								imageViewPicture1.setScaleType(ImageView.ScaleType.FIT_XY);
								imageViewPicture1.setBackgroundColor(Color.TRANSPARENT);
							}
						});
						builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
							}
						});
						builder.create().show();
						return true;
					}
					return false;
				}
			});

			LinearLayout.LayoutParams paramsTakePicture2 = new LinearLayout.LayoutParams((int) (MainContainer.mScreenWidth * .45), (int) (MainContainer.mScreenWidth * .45));
			paramsTakePicture2.setMargins(0, (int) (MainContainer.mScreenWidth * .02), 0, 0);
			picture2 = new ClassDataPicture();
			picture2.picture_name = "picture_2";
			imageViewPicture2.setLayoutParams(paramsTakePicture2);
			imageViewPicture2.setImageResource(R.drawable.imgbtn_take_picture);
			imageViewPicture2.setScaleType(ImageView.ScaleType.FIT_XY);
			imageViewPicture2.setBackgroundColor(Color.TRANSPARENT);
			imageViewPicture2.setPadding(0, 0, 0, 0);
			imageViewPicture2.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					if (picture2.picture_data == null) {
						startActivityForResult(ImageUtils.getInstance().getIntentCamera(), REQUEST_CODE_PICTURE_2);
					} else {
						ImageUtils.getInstance().showDialogPicture(picture2.picture_data, "Picture 2");
					}
				}

			});
			imageViewPicture2.setOnLongClickListener(new View.OnLongClickListener() {

				public boolean onLongClick(View v) {
					if (picture2.picture_data != null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
						builder.setTitle("Delete Taken Picture");
						builder.setMessage("Are you sure you want to delete this Picture?");
						builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
								picture2.picture_parameter = null;
								picture2.picture_data = null;
								imageViewPicture2.setImageResource(R.drawable.imgbtn_take_picture);
								imageViewPicture2.setScaleType(ImageView.ScaleType.FIT_XY);
								imageViewPicture2.setBackgroundColor(Color.TRANSPARENT);
							}
						});
						builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
							}
						});
						builder.create().show();
						return true;
					}
					return false;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_CODE_PICTURE_1:
			try {
				picture1.picture_data = ImageUtils.getInstance().setImageOnActivityResult(ImageUtils.TAKE_IMAGE_CAMERA, resultCode, data, imageViewPicture1);
				picture1.picture_parameter = sdfPicture.format(Calendar.getInstance().getTime()) + "_" + MainContainer.mObjectFacility.facility_id + "_" + "1";
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
				Toast.makeText(MainContainer.mContext, "Error loading image. Please delete some pictures and try again", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(MainContainer.mContext, "Error loading image. Please try again", Toast.LENGTH_LONG).show();
			}
			break;
		case REQUEST_CODE_PICTURE_2:
			try {
				picture2.picture_data = ImageUtils.getInstance().setImageOnActivityResult(ImageUtils.TAKE_IMAGE_CAMERA, resultCode, data, imageViewPicture2);
				picture2.picture_parameter = sdfPicture.format(Calendar.getInstance().getTime()) + "_" + MainContainer.mObjectFacility.facility_id + "_" + "2";
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
				Toast.makeText(MainContainer.mContext, "Error loading image. Please delete some pictures and try again", Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(MainContainer.mContext, "Error loading image. Please try again", Toast.LENGTH_LONG).show();
			}
			break;
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			// if (picture1.picture_data == null || picture2.picture_data == null) {
			// bIsValid = false;
			// }
			if (!bIsValid) {
				errorMessage = "Please take all pictures to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}