package pk.gov.pitb.mea.msh.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherTwentyCharacters implements TextWatcher {

	private EditText editText = null;

	public TextWatcherTwentyCharacters(EditText editText) {
		this.editText = editText;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		try {
			String text = editText.getText().toString();
			if (Integer.parseInt(text)>20){
				editText.setText("20");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void afterTextChanged(Editable s) {
	}
}