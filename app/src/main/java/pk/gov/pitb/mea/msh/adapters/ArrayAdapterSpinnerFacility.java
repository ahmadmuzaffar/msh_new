package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpinnerFacility extends ArrayAdapter<ClassScreenItem> {

	private int colorBackground;
	private int colorText;
	private ArrayList<String> arrayListSentData;

	public ArrayAdapterSpinnerFacility(Context context, ArrayList<ClassScreenItem> objects, ArrayList<String> arrayListSentData, int colorText, int colorBackground) {
		super(context, android.R.layout.simple_spinner_item, objects);
		this.arrayListSentData = arrayListSentData;
		this.colorBackground = colorBackground;
		this.colorText = colorText;
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		ClassScreenItem objectFacility = getItem(position);
		if (textView != null) {
			if (position == 0) {
				textView.setTypeface(null, Typeface.BOLD);
				textView.setText(objectFacility.item_name);
			} else {
				textView.setTypeface(null, Typeface.NORMAL);
				textView.setText(objectFacility.item_name + " (" + objectFacility.item_id + ")");
			}
			if (colorBackground != -1 && arrayListSentData.contains(objectFacility.item_id)) {
				textView.setBackgroundColor(colorBackground);
				textView.setTextColor(colorText);
			} else {
				textView.setBackgroundColor(-1);
				textView.setTextColor(Color.BLACK);
			}
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		ClassScreenItem objectFacility = getItem(position);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(Color.WHITE);
			textView.setHintTextColor(colorText);
			textView.setText(objectFacility.item_name);
			// if (position == 0) {
			// textView.setHint(textView.getText().toString());
			// textView.setText("");
			// }
		}
		return view;
	}

	@Override
	public boolean isEnabled(int position) {
		return position == 0 ? false : true;
	}
}