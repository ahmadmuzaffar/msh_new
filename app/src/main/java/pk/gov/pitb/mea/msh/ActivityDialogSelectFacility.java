package pk.gov.pitb.mea.msh;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerFacilityScreen;
import pk.gov.pitb.mea.msh.adapters.ArrayAdapterSpinnerScreenItem;
import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDialogFacility;
import pk.gov.pitb.mea.msh.models.ClassFacilityData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;

public class ActivityDialogSelectFacility extends Activity {

	private String[] ARRAY_FORM_TYPE = new String[] { "Form Type", "Form A", "Form B" };
	private Button buttonTabMonitoring;
	private Button buttonTabHealthCouncil;
	private Button buttonTabNutrients;
	private Button buttonTabHepatitis;
	private Button buttonTabPatientInterviews;
	private Spinner spinnerDistrict;
	private Spinner spinnerTehsil;
	private Spinner spinnerFacilityName;
	private Spinner spinnerFormType;
	private TableRow trFormtype;
	private EditText editTextFacilityType;
	private EditText editTextName;
	private EditText editTextPhoneNumber;
	private Button buttonOk;
	private Button buttonCancel;
	private Button buttonSync;
	private int colorOdd, colorEven;
	private String savedJSON;
	private String savedJSONHealthCouncil, savedJSONHepatitis , savedJSONNutrients, savedJSONMonitoring,savedPatientInterviews;
	private SharedPreferencesEditor sharedPreferencesEditor;



	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MainContainer.setContextOnResume(ActivityDialogSelectFacility.this);

		savedJSONMonitoring = MainContainer.mSPEditor.getLastSavedFacilityMonitoring();
		if (savedJSONMonitoring.length() > 0) {
			AlertDialogs.OnDialogClickListener onDiscard = new AlertDialogs.OnDialogClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					MainContainer.mSPEditor.clearLastSavedDataMonitoring();
					buttonTabMonitoring.setEnabled(false);
					MainContainer.bIsActivityHealthCouncil = false;
					MainContainer.bIsActivityNutrients = false;
					MainContainer.bIsActivityHepatitis = false;
					MainContainer.bIsActivityPatientInterviews = false;
					trFormtype.setVisibility(View.VISIBLE);
					/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_MONITORING);
					setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_MONITORING, colorEven);
					setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);
					spinnerVisitType.setEnabled(true);*/
					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					buttonTabHealthCouncil.setEnabled(true);
					buttonTabHepatitis.setEnabled(true);
					buttonTabNutrients.setEnabled(true);
				}
			};
			AlertDialogs.OnDialogClickListener onContinue = new AlertDialogs.OnDialogClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					MainContainer.bIsActivityHealthCouncil = false;
					MainContainer.bIsActivityNutrients = false;
					MainContainer.bIsActivityHepatitis = false;
					MainContainer.bIsActivityPatientInterviews = false;

					Intent intent = new Intent();
					intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);
					intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_TRUE);
					intent.putExtra(Constants.EXTRAS_LOAD, Constants.EXTRAS_MONITERING);
					setResult(Activity.RESULT_OK, intent);
					finish();
				}
			};
			AlertDialog.Builder builder = null;
			try {
				builder = new AlertDialog.Builder(new ContextThemeWrapper(MainContainer.mContext,
						android.R.style.Theme_Holo_Light_Dialog_NoActionBar));
			} catch (Exception e) {
				e.printStackTrace();
				builder = null;
			}
			if (builder == null) {
				builder = new AlertDialog.Builder(MainContainer.mContext);
			}
			builder.setTitle("You have saved Monitoring form");
			builder.setMessage("Your last working Monitoring form is saved. Do you want to load it now?");
			builder.setPositiveButton("Load Form", onContinue);
			builder.setNegativeButton("Discard Data", onDiscard);
			builder.setCancelable(false);
			builder.create().show();
		}



	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_select_facility);
		initializeApplication();
	}

	private void initializeApplication() {
		try {
			MainContainer.setContextOnResume(ActivityDialogSelectFacility.this);
			MainContainer.mObjectFacility = null;
			savedJSONHealthCouncil=MainContainer.mSPEditor.getLastSavedFacilityHealthCouncil();
			savedJSONHepatitis=MainContainer.mSPEditor.getLastSavedFacilityHepatitis();
			savedJSONNutrients=MainContainer.mSPEditor.getLastSavedFacilityNutrients();
			savedPatientInterviews = MainContainer.mSPEditor.getLastSavedFacilityPatientInterviews();
			setFinishOnTouchOutside(false);

			MainContainer.bIsActivityHealthCouncil = false;
			MainContainer.bIsActivityNutrients = false;
			MainContainer.bIsActivityHepatitis = false;
			MainContainer.bIsActivityPatientInterviews = false;
			sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);


			generateBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	public void generateBody() {
		try {
			buttonTabMonitoring = (Button) findViewById(R.id.dialog_tab_monitoring);
			buttonTabHealthCouncil = (Button) findViewById(R.id.dialog_tab_health_council);
			buttonTabNutrients = (Button) findViewById(R.id.dialog_tab_nutrients);
			buttonTabHepatitis = (Button) findViewById(R.id.dialog_tab_hepatitis);
			buttonTabPatientInterviews = (Button) findViewById(R.id.dialog_tab_patient_interviews);
			spinnerDistrict = (Spinner) findViewById(R.id.dialog_facility_district);
			spinnerTehsil = (Spinner) findViewById(R.id.dialog_facility_tehsil);
			spinnerFacilityName = (Spinner) findViewById(R.id.dialog_facility_name);
			spinnerFormType = (Spinner) findViewById(R.id.dialog_form_type);

			trFormtype = (TableRow) findViewById(R.id.main_form_type);
			editTextFacilityType = (EditText) findViewById(R.id.dialog_facility_type);
			editTextName = (EditText) findViewById(R.id.dialog_facility_incharge_name);
			editTextPhoneNumber = (EditText) findViewById(R.id.dialog_facility_phone_number);
			buttonOk = (Button) findViewById(R.id.dialog_button_ok);
			buttonCancel = (Button) findViewById(R.id.dialog_button_cancel);
			buttonSync = (Button) findViewById(R.id.dialog_button_sync);
			// ----------------------------------------------------------------------------------------------------
			TableLayout tableLayoutDialog = (TableLayout) spinnerDistrict.getParent().getParent();
			tableLayoutDialog.setPadding((int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02), (int) (MainContainer.mScreenWidth * .02),
					(int) (MainContainer.mScreenWidth * .02));
			tableLayoutDialog.setPadding((int) (MainContainer.mScreenWidth * .002), (int) (MainContainer.mScreenWidth * .002), (int) (MainContainer.mScreenWidth * .002),
					(int) (MainContainer.mScreenWidth * .002));
			// ----------------------------------------------------------------------------------------------------
			TableRow.LayoutParams paramsHeading = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .90), (int) (MainContainer.mScreenWidth * .1));
			paramsHeading.span = 2;
			paramsHeading.setMargins(0, 0, 0, 0);
			((EditText) findViewById(R.id.dialog_facility_heading)).setLayoutParams(paramsHeading);

			TableRow.LayoutParams paramsCol0 = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .348), TableRow.LayoutParams.MATCH_PARENT);
			paramsCol0.setMargins(0, 0, (int) (MainContainer.mScreenWidth * .002), 0);
			TableRow.LayoutParams paramsCol1 = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * .55), (int) (MainContainer.mScreenWidth * .08));

			((TableRow) spinnerDistrict.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) spinnerTehsil.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) spinnerFacilityName.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) spinnerFormType.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) editTextFacilityType.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) editTextName.getParent()).getChildAt(0).setLayoutParams(paramsCol0);
			((TableRow) editTextPhoneNumber.getParent()).getChildAt(0).setLayoutParams(paramsCol0);

			((TableRow) spinnerDistrict.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) spinnerTehsil.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) spinnerFacilityName.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) spinnerFormType.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) editTextFacilityType.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) editTextName.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			((TableRow) editTextPhoneNumber.getParent()).getChildAt(0).setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);

			spinnerDistrict.setLayoutParams(paramsCol1);
			spinnerTehsil.setLayoutParams(paramsCol1);
			spinnerFacilityName.setLayoutParams(paramsCol1);
			spinnerFormType.setLayoutParams(paramsCol1);
			editTextFacilityType.setLayoutParams(paramsCol1);
			editTextName.setLayoutParams(paramsCol1);
			editTextPhoneNumber.setLayoutParams(paramsCol1);

			editTextFacilityType.setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			editTextName.setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			editTextPhoneNumber.setPadding((int) (MainContainer.mScreenWidth * .02), 0, (int) (MainContainer.mScreenWidth * .02), 0);
			// ----------------------------------------------------------------------------------------------------
			spinnerDistrict.setBackgroundResource(R.drawable.spinner_1_1);
			spinnerTehsil.setBackgroundResource(R.drawable.spinner_2_1);
			spinnerFacilityName.setBackgroundResource(R.drawable.spinner_3_1);
			spinnerFormType.setBackgroundResource(R.drawable.spinner_2_1);
			editTextPhoneNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
			editTextPhoneNumber.setFilters(new InputFilter[] { new InputFilter.LengthFilter(Constants.MAX_LENGTH_MOBILE_NUMBER) });
			// ----------------------------------------------------------------------------------------------------
			colorOdd = MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd);
			colorEven = MainContainer.mContext.getResources().getColor(R.color.row_body_background_even);
			setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
			setSpinnerAdapter(spinnerFormType, ARRAY_FORM_TYPE, colorEven);


			buttonTabMonitoring.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					buttonTabMonitoring.setEnabled(false);
					MainContainer.bIsActivityHealthCouncil = false;
					MainContainer.bIsActivityNutrients = false;
					MainContainer.bIsActivityHepatitis = false;
					MainContainer.bIsActivityPatientInterviews = false;
					trFormtype.setVisibility(View.VISIBLE);
					/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_MONITORING);
					setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_MONITORING, colorEven);
					setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);
					spinnerVisitType.setEnabled(true);*/
					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					buttonTabHealthCouncil.setEnabled(true);
					buttonTabHepatitis.setEnabled(true);
					buttonTabNutrients.setEnabled(true);
					buttonTabPatientInterviews.setEnabled(true);
				}
			});

			buttonTabNutrients.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					/*buttonTabNutrients.setEnabled(false);
					MainContainer.bIsActivityHealthCouncil = false;
					//trFormtype.setVisibility(View.VISIBLE);
					*//*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_MONITORING);
					setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_MONITORING, colorEven);
					setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);
					spinnerVisitType.setEnabled(true);*//*
					buttonTabHealthCouncil.setEnabled(true);
					buttonTabHepatitis.setEnabled(true);
					buttonTabMonitoring.setEnabled(true);*/
					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					trFormtype.setVisibility(View.GONE);
					if (savedJSONNutrients.length() > 0) {
						AlertDialogs.OnDialogClickListener onDiscard = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								MainContainer.mSPEditor.clearLastSavedDataNutrients();
								buttonTabHepatitis.setEnabled(true);
								MainContainer.bIsActivityHealthCouncil = false;
								MainContainer.bIsActivityNutrients = true;
								MainContainer.bIsActivityHepatitis = false;
								MainContainer.bIsActivityPatientInterviews = false;
								buttonTabHealthCouncil.setEnabled(true);
								buttonTabMonitoring.setEnabled(true);
								buttonTabNutrients.setEnabled(false);
								buttonTabPatientInterviews.setEnabled(true);
							}
						};
						AlertDialogs.OnDialogClickListener onContinue = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent();
								intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_TRUE_NUTRIENTS);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						};
						AlertDialog.Builder builder = null;
						try {
							builder = new AlertDialog.Builder(new ContextThemeWrapper(MainContainer.mContext,
									android.R.style.Theme_Holo_Light_Dialog_NoActionBar));
						} catch (Exception e) {
							e.printStackTrace();
							builder = null;
						}
						if (builder == null) {
							builder = new AlertDialog.Builder(MainContainer.mContext);
						}
						builder.setTitle("You have saved Nutrients form");
						builder.setMessage("Your last working Nutrients form is saved. Do you want to load it now?");
						builder.setPositiveButton("Load Form", onContinue);
						builder.setNegativeButton("Discard Data", onDiscard);
						builder.setNeutralButton("Cancel", null);
						builder.setCancelable(true);
						builder.create().show();
					} else {
						buttonTabHepatitis.setEnabled(true);
						MainContainer.bIsActivityHealthCouncil = false;
						MainContainer.bIsActivityNutrients = true;
						MainContainer.bIsActivityHepatitis = false;
						MainContainer.bIsActivityPatientInterviews = false;
						buttonTabHealthCouncil.setEnabled(true);
						buttonTabMonitoring.setEnabled(true);
						buttonTabNutrients.setEnabled(false);
						buttonTabPatientInterviews.setEnabled(true);

					}
				}
			});

			buttonTabHepatitis.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					/*buttonTabHepatitis.setEnabled(false);
					MainContainer.bIsActivityHealthCouncil = false;
					MainContainer.bIsActivityNutrients = false;
					MainContainer.bIsActivityHepatitis = true;
					buttonTabHealthCouncil.setEnabled(true);
					buttonTabMonitoring.setEnabled(true);
					buttonTabNutrients.setEnabled(true);*/

					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					trFormtype.setVisibility(View.GONE);
					if (savedJSONHepatitis.length() > 0) {
						AlertDialogs.OnDialogClickListener onDiscard = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								MainContainer.mSPEditor.clearLastSavedDataHepatitis();
								buttonTabHepatitis.setEnabled(false);
								MainContainer.bIsActivityHealthCouncil = false;
								MainContainer.bIsActivityNutrients = false;
								MainContainer.bIsActivityHepatitis = true;
								MainContainer.bIsActivityPatientInterviews = false;
								buttonTabHealthCouncil.setEnabled(true);
								buttonTabMonitoring.setEnabled(true);
								buttonTabNutrients.setEnabled(true);
								buttonTabPatientInterviews.setEnabled(true);
							}
						};
						AlertDialogs.OnDialogClickListener onContinue = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent();
								intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_TRUE_HEPATITIS);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						};
						AlertDialog.Builder builder = null;
						try {
							builder = new AlertDialog.Builder(new ContextThemeWrapper(MainContainer.mContext,
									android.R.style.Theme_Holo_Light_Dialog_NoActionBar));
						} catch (Exception e) {
							e.printStackTrace();
							builder = null;
						}
						if (builder == null) {
							builder = new AlertDialog.Builder(MainContainer.mContext);
						}
						builder.setTitle("You have saved Hepatitis form");
						builder.setMessage("Your last working Hepatitis form is saved. Do you want to load it now?");
						builder.setPositiveButton("Load Form", onContinue);
						builder.setNegativeButton("Discard Data", onDiscard);
						builder.setNeutralButton("Cancel", null);
						builder.setCancelable(true);
						builder.create().show();
					} else {
						buttonTabHepatitis.setEnabled(false);
						MainContainer.bIsActivityHealthCouncil = false;
						MainContainer.bIsActivityNutrients = false;
						MainContainer.bIsActivityHepatitis = true;
						MainContainer.bIsActivityPatientInterviews = false;
						buttonTabHealthCouncil.setEnabled(true);
						buttonTabMonitoring.setEnabled(true);
						buttonTabNutrients.setEnabled(true);
						buttonTabPatientInterviews.setEnabled(true);
					}

				}
			});

			buttonTabHealthCouncil.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					trFormtype.setVisibility(View.GONE);
					if (savedJSONHealthCouncil.length() > 0) {
						AlertDialogs.OnDialogClickListener onDiscard = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								MainContainer.mSPEditor.clearLastSavedDataHealthCouncil();
								buttonTabHealthCouncil.setEnabled(false);
								MainContainer.bIsActivityNutrients = false;
								MainContainer.bIsActivityHepatitis = false;
								MainContainer.bIsActivityPatientInterviews = false;
								MainContainer.bIsActivityHealthCouncil = true;
								/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_HEALTH_COUNCIL);
								setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_MONITORING, colorEven);
								setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);*/
								//spinnerVisitType.setEnabled(false);
								buttonTabMonitoring.setEnabled(true);
								buttonTabHepatitis.setEnabled(true);
								buttonTabNutrients.setEnabled(true);
								buttonTabPatientInterviews.setEnabled(true);
							}
						};
						AlertDialogs.OnDialogClickListener onContinue = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent();
								intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
								intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_TRUE_HEALTH_COUNCIL);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						};
						AlertDialog.Builder builder = null;
						try {
							builder = new AlertDialog.Builder(new ContextThemeWrapper(MainContainer.mContext,
									android.R.style.Theme_Holo_Light_Dialog_NoActionBar));
						} catch (Exception e) {
							e.printStackTrace();
							builder = null;
						}
						if (builder == null) {
							builder = new AlertDialog.Builder(MainContainer.mContext);
						}
						builder.setTitle("You have saved Health Council form");
						builder.setMessage("Your last working Health Council form is saved. Do you want to load it now?");
						builder.setPositiveButton("Load Form", onContinue);
						builder.setNegativeButton("Discard Data", onDiscard);
						builder.setNeutralButton("Cancel", null);
						builder.setCancelable(true);
						builder.create().show();
					} else {
						buttonTabHealthCouncil.setEnabled(false);
						MainContainer.bIsActivityNutrients = false;
						MainContainer.bIsActivityHepatitis = false;
						MainContainer.bIsActivityPatientInterviews = false;
						MainContainer.bIsActivityHealthCouncil = true;
						/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_HEALTH_COUNCIL);
						setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_MONITORING, colorEven);
						setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);*/
						//spinnerVisitType.setEnabled(false);
						buttonTabMonitoring.setEnabled(true);
						buttonTabHepatitis.setEnabled(true);
						buttonTabNutrients.setEnabled(true);
						buttonTabPatientInterviews.setEnabled(true);


					}
				}
			});
			buttonTabPatientInterviews.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					setSpinnerAdapter(spinnerDistrict, "Select District", MainContainer.mDbAdapter.selectAllDistricts(), colorOdd);
					trFormtype.setVisibility(View.GONE);
					MainContainer.bIsActivityNutrients = false;
					MainContainer.bIsActivityHepatitis = false;
					MainContainer.bIsActivityHealthCouncil = false;
					MainContainer.bIsActivityPatientInterviews = true;
					if (savedPatientInterviews.length() > 0) {
						AlertDialogs.OnDialogClickListener onDiscard = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								MainContainer.mSPEditor.clearLastSavedDataPatientInterviews();
								buttonTabPatientInterviews.setEnabled(false);
								MainContainer.bIsActivityNutrients = false;
								MainContainer.bIsActivityHepatitis = false;
								MainContainer.bIsActivityHealthCouncil = false;
								MainContainer.bIsActivityPatientInterviews = true;
								/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_PATIENT_INTERVIEWS);
								setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_HEALTH_COUNCIL*//*ARRAY_FACILITY_TYPE_MONITORING*//*, colorEven);
								setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);*/
								//spinnerVisitType.setEnabled(false);
								buttonTabMonitoring.setEnabled(true);
								buttonTabHealthCouncil.setEnabled(true);
								buttonTabHepatitis.setEnabled(true);
								buttonTabNutrients.setEnabled(true);
							}
						};
						AlertDialogs.OnDialogClickListener onContinue = new AlertDialogs.OnDialogClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent();
								intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);

								intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_TRUE_PATIENT_INTERVIEWS);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						};
						AlertDialog.Builder builder = null;
						try {
							builder = new AlertDialog.Builder(new ContextThemeWrapper(MainContainer.mContext,
									android.R.style.Theme_Holo_Light_Dialog_NoActionBar));
						} catch (Exception e) {
							e.printStackTrace();
							builder = null;
						}
						if (builder == null) {
							builder = new AlertDialog.Builder(MainContainer.mContext);
						}
						builder.setTitle("You have saved Patient Interviews form");
						builder.setMessage("Your last working Patient Interviews form is saved. Do you want to load it now?");
						builder.setPositiveButton("Load Form", onContinue);
						builder.setNegativeButton("Discard Data", onDiscard);
						builder.setNeutralButton("Cancel", null);
						builder.setCancelable(true);
						builder.create().show();
					} else {
						buttonTabPatientInterviews.setEnabled(false);
						MainContainer.bIsActivityNutrients = false;
						MainContainer.bIsActivityHepatitis = false;
						MainContainer.bIsActivityHealthCouncil = false;
						MainContainer.bIsActivityPatientInterviews = true;
						/*arrayListSentData = MainContainer.mDbAdapter.selectAllSentDataIds(Constants.AT_PATIENT_INTERVIEWS);
						setSpinnerAdapter(spinnerFacilityType, ARRAY_FACILITY_TYPE_HEALTH_COUNCIL*//*ARRAY_FACILITY_TYPE_MONITORING*//*, colorEven);
						setSpinnerAdapterFacility(spinnerFacility, colorOdd, null);*/
						//spinnerVisitType.setEnabled(false);
						buttonTabMonitoring.setEnabled(true);
						buttonTabHealthCouncil.setEnabled(true);
						buttonTabHepatitis.setEnabled(true);
						buttonTabNutrients.setEnabled(true);

					}
				}
			});
			spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					ArrayList<ClassScreenItem> arrayList = null;
					if (position > 0) {
						arrayList = MainContainer.mDbAdapter.selectAllTehsils(((ClassScreenItem) spinnerDistrict.getSelectedItem()).item_id);
					}
					setSpinnerAdapter(spinnerTehsil, "Select Tehsil", arrayList, colorEven);
					spinnerTehsil.setSelection(0);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
			spinnerTehsil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					ArrayList<ClassScreenItem> arrayList = null;
					if (position > 0) {
						if(MainContainer.bIsActivityNutrients){
							arrayList = MainContainer.mDbAdapter.selectAllFacilities(((ClassScreenItem) spinnerDistrict.getSelectedItem()).item_id,
									((ClassScreenItem) spinnerTehsil.getSelectedItem()).item_id,Constants.FACILITY_SECOND_TYPE_NUTRITION);
						/*}else if(MainContainer.bIsActivityHepatitis){
							arrayList = MainContainer.mDbAdapter.selectAllFacilities(((ClassScreenItem) spinnerDistrict.getSelectedItem()).item_id,
									((ClassScreenItem) spinnerTehsil.getSelectedItem()).item_id,Constants.FACILITY_SECOND_TYPE_HEPATITIS);
						*/}else {
							arrayList = MainContainer.mDbAdapter.selectAllFacilities(((ClassScreenItem) spinnerDistrict.getSelectedItem()).item_id,
									((ClassScreenItem) spinnerTehsil.getSelectedItem()).item_id);
						}
					}
					setSpinnerAdapter(spinnerFacilityName, "Select Facility", arrayList, colorOdd);
					spinnerFacilityName.setSelection(0);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
			spinnerFacilityName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					ClassFacilityData objectFacilityData = null;
					if (position > 0) {
						ClassScreenItem objectScreenItem = (ClassScreenItem) spinnerFacilityName.getSelectedItem();
						objectFacilityData = MainContainer.mDbAdapter.selectFacilityByName(objectScreenItem.item_id, objectScreenItem.item_name);
					}
					if (objectFacilityData != null) {
						editTextFacilityType.setText(objectFacilityData.facility_type);
					} else {
						editTextFacilityType.setText("");
						editTextFacilityType.setHint("N/A");
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
			buttonOk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					if (spinnerDistrict.getSelectedItemPosition() == 0) {
						Toast.makeText(MainContainer.mContext, "Plese Select District to Continue", Toast.LENGTH_SHORT).show();
					} else if (spinnerTehsil.getSelectedItemPosition() == 0) {
						Toast.makeText(MainContainer.mContext, "Plese Select Tehsil to Continue", Toast.LENGTH_SHORT).show();
					} else if (spinnerFacilityName.getSelectedItemPosition() == 0) {
						Toast.makeText(MainContainer.mContext, "Plese Select Facility to Continue", Toast.LENGTH_SHORT).show();
					} else if (spinnerFormType.getSelectedItemPosition() == 0 && trFormtype.getVisibility()==View.VISIBLE /*((!MainContainer.bIsActivityHealthCouncil) || (!MainContainer.bIsActivityNutrients) || (!MainContainer.bIsActivityHepatitis) )*/) {
						Toast.makeText(MainContainer.mContext, "Plese Select Form Type to Continue", Toast.LENGTH_SHORT).show();
					} else if (editTextName.getText().toString().trim().length() == 0) {
						Toast.makeText(MainContainer.mContext, "Plese Enter Name to Continue", Toast.LENGTH_SHORT).show();
					} else if (editTextPhoneNumber.getText().toString().trim().length() == 0) {
						Toast.makeText(MainContainer.mContext, "Plese Enter Phone Number to Continue", Toast.LENGTH_SHORT).show();
					} else if (editTextPhoneNumber.getText().toString().trim().length() > 0 && editTextPhoneNumber.getText().toString().trim().length() < Constants.MIN_LENGTH_MOBILE_NUMBER) {
						Toast.makeText(MainContainer.mContext, "Plese Enter Valid Phone Number to Continue", Toast.LENGTH_SHORT).show();
					} else {
						MainContainer.mObjectFacility = new ClassDialogFacility();
						MainContainer.mObjectFacility.district_id = ((ClassScreenItem) spinnerDistrict.getSelectedItem()).item_id;
						MainContainer.mObjectFacility.tehsil_id = ((ClassScreenItem) spinnerTehsil.getSelectedItem()).item_id;
						MainContainer.mObjectFacility.facility_id = ((ClassScreenItem) spinnerFacilityName.getSelectedItem()).item_id;
						MainContainer.mObjectFacility.facility_name = ((ClassScreenItem) spinnerFacilityName.getSelectedItem()).item_name;
						MainContainer.mObjectFacility.facility_type = editTextFacilityType.getText().toString().trim();
						MainContainer.mObjectFacility.incharge_name = editTextName.getText().toString().trim();
						MainContainer.mObjectFacility.phone_number = editTextPhoneNumber.getText().toString().trim();
						ClassScreenItem objectScreenItem = (ClassScreenItem) spinnerFacilityName.getSelectedItem();
						ClassFacilityData objectFacilityData = null;
						objectFacilityData = MainContainer.mDbAdapter.selectFacilityByName(objectScreenItem.item_id, objectScreenItem.item_name);
						MainContainer.mObjectFacility.type_id = objectFacilityData.facility_type_code;
						MainContainer.mObjectFacility.type_name = objectFacilityData.facility_type;
						MainContainer.mObjectFacilityData = objectFacilityData;

						if (spinnerFormType.getSelectedItemPosition() == 1) {
							MainContainer.mObjectFacility.form_type = Constants.FORM_TYPE_A;
						} else if (spinnerFormType.getSelectedItemPosition() == 2) {
							MainContainer.mObjectFacility.form_type = Constants.FORM_TYPE_B;
						}

						Intent intent = new Intent();
						if (MainContainer.bIsActivityHealthCouncil){
							intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
							intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_FALSE_HEALTH_COUNCIL);
						}
						else if (MainContainer.bIsActivityHepatitis){
							intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_FALSE_HEPATITIS);
							intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
						}
						else if (MainContainer.bIsActivityNutrients){
							intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_FALSE_NUTRIENTS);
							intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
						}else if (MainContainer.bIsActivityPatientInterviews){
							intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_FALSE_PATIENT_INTERVIEWS);
							intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_FALSE);
						}
						else {
							intent.putExtra(Constants.EXTRAS_MONITERING, Constants.VALUE_TRUE);
							intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_TRUE);
						}
						intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_FALSE);

						setResult(Activity.RESULT_OK, intent);
						finish();
					}
				}
			});
			buttonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
					builder.setTitle("All Entered Data will be Lost");
					builder.setMessage("Are You Sure You Want to Exit Application?");
					builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialogInterface, int id) {
							finish();
						}
					});
					builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialogInterface, int id) {
						}
					});
					builder.show();
				}
			});
			buttonSync.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent intent = new Intent();
					intent.putExtra(Constants.EXTRAS_SYNC, Constants.VALUE_TRUE);
					intent.putExtra(Constants.EXTRAS_LOAD, Constants.VALUE_FALSE);
					setResult(Activity.RESULT_OK, intent);
					finish();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSpinnerAdapter(Spinner spinner, String hint, ArrayList<ClassScreenItem> arrayValues, int colorDropdown) {
		if (arrayValues == null) {
			arrayValues = new ArrayList<ClassScreenItem>();
		}
		ClassScreenItem objectSelect = new ClassScreenItem();
		objectSelect.item_id = "-1";
		objectSelect.item_name = hint;
		arrayValues.add(0, objectSelect);
		ArrayAdapterSpinnerScreenItem arrayAdapter = new ArrayAdapterSpinnerScreenItem(MainContainer.mContext, arrayValues, Color.BLACK, -1);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(arrayAdapter);
		arrayAdapter.notifyDataSetChanged();
	}

	private void setSpinnerAdapter(Spinner spinner, String[] arrayValues, int colorDropdown) {
		ArrayAdapterSpinnerFacilityScreen arrayAdapter = new ArrayAdapterSpinnerFacilityScreen(MainContainer.mContext, arrayValues, Color.BLACK, -1);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(arrayAdapter);
		arrayAdapter.notifyDataSetChanged();
	}
}