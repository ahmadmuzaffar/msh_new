package pk.gov.pitb.mea.msh.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.LocationTracker;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.content.Context;
import android.telephony.TelephonyManager;

public class ClassApplicationInformation {

	private final String KEY_APP_VERSION_NO = "KEY_APP_VERSION_NO";
	private final String KEY_IMEI_NUMBER = "KEY_IMEI_NUMBER";
	private final String KEY_GPS_LOCATION = "KEY_GPS_LOCATION";
	private final String KEY_NETWORK_LOCATION = "KEY_NETWORK_LOCATION";
	private final String KEY_SENT_DATE_TIME = "KEY_SENT_DATE_TIME";
	private Context context;
	private LocationTracker locationTracker;
	private JSONObject jsonObject;

	public ClassApplicationInformation(Context context, LocationTracker locationTracker) {
		this.context = context;
		this.locationTracker = locationTracker;
		this.jsonObject = null;
	}

	@Override
	public String toString() {
		try {
			return jsonObject.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean setValues() {
		try {
			locationTracker.getBetterLocation();
			jsonObject = new JSONObject();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			TelephonyManager telephonyManager = (TelephonyManager) MainContainer.mContext.getSystemService(Context.TELEPHONY_SERVICE);
			jsonObject.put(KEY_APP_VERSION_NO, context.getResources().getString(R.string.app_version_name));
			jsonObject.put(KEY_IMEI_NUMBER, telephonyManager.getDeviceId());
			jsonObject.put(KEY_GPS_LOCATION, locationTracker.getGpsLocation());
			jsonObject.put(KEY_NETWORK_LOCATION, locationTracker.getNetworkLocation());
			jsonObject.put(KEY_SENT_DATE_TIME, sdf.format(Calendar.getInstance().getTime()));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}