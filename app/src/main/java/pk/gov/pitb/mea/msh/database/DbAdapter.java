package pk.gov.pitb.mea.msh.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassDataActivity;
import pk.gov.pitb.mea.msh.models.ClassDataPicture;
import pk.gov.pitb.mea.msh.models.ClassFacilityData;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassScreenItemMultiple;
import pk.gov.pitb.mea.msh.models.ClassSentData;
import pk.gov.pitb.mea.msh.models.ClassStaff;

public class DbAdapter {

	private Context mContext;
	private SQLiteDatabase mDb;
	private DbHelper mDbHelper;
	// private final String FORMATE_DATE_SQL = "yyyy-MM-dd HH:mm:ss";

	private final String[] PROJECTION_ACTIVITY;
	private final String[] PROJECTION_PICTURE;
	private final String[] PROJECTION_SCREEN_DATA;
	private final String[] PROJECTION_SCREEN_ITEMS;
	private final String[] PROJECTION_FACILITY_DATA;
	private final String[] PROJECTION_STAFF_DATA;
	private final String[] PROJECTION_SENT_DATA;

	public DbAdapter(Context context) {
		mContext = context;
		mDb = null;
		mDbHelper = new DbHelper(mContext);

		PROJECTION_ACTIVITY = new String[] { mDbHelper.pk_id, mDbHelper.app_version_no, mDbHelper.activity_type, mDbHelper.sending_date_time, mDbHelper.json_data };
		PROJECTION_PICTURE = new String[] { mDbHelper.pk_id, mDbHelper.fk_id, mDbHelper.picture_name, mDbHelper.picture_parameter, mDbHelper.picture_status, mDbHelper.picture_data };
		PROJECTION_SCREEN_DATA = new String[] { mDbHelper.pk_id, mDbHelper.type_id, mDbHelper.type_name, mDbHelper.screen_id, mDbHelper.screen_name, mDbHelper.section_id, mDbHelper.section_name,
				mDbHelper.section_value };
		PROJECTION_SCREEN_ITEMS = new String[] { mDbHelper.pk_id, mDbHelper.item_id, mDbHelper.item_name, mDbHelper.fk_id };
		PROJECTION_FACILITY_DATA = new String[] { mDbHelper.pk_id, mDbHelper.facility_id, mDbHelper.facility_type_code, mDbHelper.facility_type, mDbHelper.facility_name, mDbHelper.tehsil_id,
				mDbHelper.tehsil_name, mDbHelper.district_id, mDbHelper.district_name, mDbHelper.json_arrays_data,mDbHelper.facility_nutrition_second_type,mDbHelper.facility_sentinel_second_type };
		PROJECTION_STAFF_DATA = new String[] { mDbHelper.pk_id, mDbHelper.staff_name, mDbHelper.staff_designation_id, mDbHelper.staff_mobile, mDbHelper.staff_cnic, mDbHelper.staff_role,
				mDbHelper.fk_id };
		PROJECTION_SENT_DATA = new String[] { mDbHelper.pk_id, mDbHelper.sending_date_time, mDbHelper.facility_id };
	}

	public boolean isApplicationInitialized() {
		try {
			return !(isTableEmpty(mDbHelper.TABLE_SCREEN_ITEM, PROJECTION_SCREEN_ITEMS) || isTableEmpty(mDbHelper.TABLE_FACILITY_DATA, PROJECTION_FACILITY_DATA));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean resetApplication() {
		try {
			MainContainer.mSPEditor.resetVersions();
			deleteTableData(mDbHelper.TABLE_SCREEN_DATA, mDbHelper.TABLE_SCREEN_ITEM, mDbHelper.TABLE_FACILITY_DATA, mDbHelper.TABLE_SENT_DATA);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean resetStructure() {
		try {
			deleteTableData(mDbHelper.TABLE_SCREEN_DATA, mDbHelper.TABLE_SCREEN_ITEM);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean resetFacility() {
		try {
			deleteTableData(mDbHelper.TABLE_FACILITY_DATA);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean resetSentData() {
		try {
			deleteTableData(mDbHelper.TABLE_SENT_DATA);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public long insertActivity(ClassDataActivity object) {
		try {
			object.pk_id = (int) insertObject(mDbHelper.TABLE_ACTIVITY_DATA, createContentValuesActivity(object));
			if (object.arrayListPictures != null) {
				for (ClassDataPicture objectPicture : object.arrayListPictures) {
					objectPicture.fk_id = object.pk_id;
					insertPicture(objectPicture);
				}
			}
			return object.pk_id;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public long insertActivitySave(ClassDataActivity object) {
		try {
			deleteSavedPictures();
			/*object.pk_id = (int) insertObject(mDbHelper.TABLE_ACTIVITY_DATA, createContentValuesActivity(object));*/
			if (object.arrayListPictures != null) {
				for (ClassDataPicture objectPicture : object.arrayListPictures) {
					/*objectPicture.fk_id = object.pk_id;*/
					objectPicture.picture_status = Constants.PICTURE_SAVE_DATA;
					insertPicture(objectPicture);
				}
			}
			return object.pk_id;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}



	private ContentValues createContentValuesActivity(ClassDataActivity object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.app_version_no, object.app_version_no);
		cv.put(mDbHelper.activity_type, object.activity_type);
		cv.put(mDbHelper.sending_date_time, object.sending_date_time);
		cv.put(mDbHelper.json_data, object.json_data);
		return cv;
	}

	public long insertPicture(ClassDataPicture object) {
		try {
			return insertObject(mDbHelper.TABLE_PICTURE_DATA, createContentValuesPicture(object));
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	private ContentValues createContentValuesPicture(ClassDataPicture object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.fk_id, object.fk_id);
		cv.put(mDbHelper.picture_name, object.picture_name);
		cv.put(mDbHelper.picture_parameter, object.picture_parameter);
		cv.put(mDbHelper.picture_status, object.picture_status);
		cv.put(mDbHelper.picture_data, object.picture_data);
		return cv;
	}

	public long insertScreenItem(ClassScreenItem object) {
		try {
			return insertObject(mDbHelper.TABLE_SCREEN_ITEM, createContentValuesScreenItem(object));
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	private ContentValues createContentValuesScreenItem(ClassScreenItem object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.item_id, object.item_id);
		cv.put(mDbHelper.item_name, object.item_name);
		cv.put(mDbHelper.fk_id, object.fk_id);
		return cv;
	}

	public ArrayList<ClassScreenItem> getDesignationFacilityIncharge() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_FACILITY_INCHARGE;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_FACILITY_INCHARGE;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "designations_dropdown";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			return MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getDesignation(String designationId) {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_DATA_ON_ABSENCES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "designation_dropdown";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			ArrayList<ClassScreenItem> arrayListDesignations = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
			for (ClassScreenItem item : arrayListDesignations) {
				if (item.item_id.equalsIgnoreCase(designationId)) {
					return item.item_name;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getDistrictName(String districtId) {
		String districtName = "";
		try {
			this.open();
			String format = "%s = %s";
			String whereClause = String.format(format, mDbHelper.district_id, districtId);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, PROJECTION_FACILITY_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					districtName = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.district_name));
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtName;
	}

	public String getTehsilName(String tehsilId) {
		String tehsilName = "";
		try {
			this.open();
			String format = "%s = %s";
			String whereClause = String.format(format, mDbHelper.tehsil_id, tehsilId);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, PROJECTION_FACILITY_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					tehsilName = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.tehsil_name));
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tehsilName;
	}

	public ArrayList<ClassScreenItemMultiple> selectScreenItemsMultiple(ClassScreenData objectData) {
		try {
			ArrayList<ClassScreenItemMultiple> arrayListScreenItemMultiples = new ArrayList<ClassScreenItemMultiple>();
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s'", mDbHelper.type_name, objectData.type_name, mDbHelper.screen_name, objectData.screen_name);
			ArrayList<ClassScreenData> arrayListScreenData = selectScreenData(whereClause);
			for (ClassScreenData obj : arrayListScreenData) {
				whereClause = String.format(MainContainer.mLocale, "%s = %s", mDbHelper.fk_id, obj.pk_id);
				ClassScreenItemMultiple objNew = new ClassScreenItemMultiple();
				objNew.label = obj.section_value;
				objNew.arrayListFields = selectScreenItems(whereClause);
				arrayListScreenItemMultiples.add(objNew);
			}
			return arrayListScreenItemMultiples;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int getMedicinesCount() {
		ClassScreenData objectScreenData = new ClassScreenData();
		objectScreenData.setDefaultValues();
		objectScreenData.screen_id = Constants.DB_SCREEN_ID_EQUIPMENT;
		objectScreenData.screen_name = Constants.DB_SCREEN_NAME_EQUIPMENT;
		return getScreenItemCount(objectScreenData);
	}

	private int getScreenItemCount(ClassScreenData objectData) {
		try {
			int count = 0;
			String countQuery;
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s'", mDbHelper.type_name, objectData.type_name, mDbHelper.screen_name, objectData.screen_name);
			ArrayList<ClassScreenData> arrayListScreenData = selectScreenData(whereClause);
			this.open();
			for (ClassScreenData obj : arrayListScreenData) {
				whereClause = String.format(MainContainer.mLocale, "%s = %s", mDbHelper.fk_id, obj.pk_id);
				countQuery = "SELECT  * FROM " + mDbHelper.TABLE_SCREEN_ITEM + " where " + whereClause;
				Cursor cursor = mDb.rawQuery(countQuery, null);
				count += cursor.getCount();
				cursor.close();
			}
			this.close();
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public ArrayList<ClassScreenItem> selectScreenItems(ClassScreenData object) {
		try {
			object = selectScreenData(object);
			String whereClause = String.format(MainContainer.mLocale, "%s = %s", mDbHelper.fk_id, object.pk_id);
			return selectScreenItems(whereClause);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ArrayList<ClassScreenItem> selectScreenItems(String whereClause) {
		try {
			this.open();
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_SCREEN_ITEM, PROJECTION_SCREEN_ITEMS, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.item_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.item_name));
					object.fk_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.fk_id));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ClassScreenData insertScreenData(ClassScreenData object) {
		try {
			ClassScreenData objectNew = selectScreenData(object);
			if (objectNew == null) {
				insertObject(mDbHelper.TABLE_SCREEN_DATA, createContentValuesScreenData(object));
				object = selectScreenData(object);
				return object;
			} else {
				return objectNew;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ContentValues createContentValuesScreenData(ClassScreenData object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.type_id, object.type_id);
		cv.put(mDbHelper.type_name, object.type_name);
		cv.put(mDbHelper.screen_id, object.screen_id);
		cv.put(mDbHelper.screen_name, object.screen_name);
		cv.put(mDbHelper.section_id, object.section_id);
		cv.put(mDbHelper.section_name, object.section_name);
		cv.put(mDbHelper.section_value, object.section_value);
		return cv;
	}

	public ClassScreenData selectScreenData(ClassScreenData object) {
		try {
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s' and %s = '%s' and %s = '%s'", mDbHelper.type_name, object.type_name, mDbHelper.screen_name,
					object.screen_name, mDbHelper.section_name, object.section_name, mDbHelper.section_value, object.section_value);
			ArrayList<ClassScreenData> arrayListScreenData = selectScreenData(whereClause);
			return arrayListScreenData.size() > 0 ? arrayListScreenData.get(0) : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ArrayList<ClassScreenData> selectScreenData(String whereClause) {
		try {
			this.open();
			ArrayList<ClassScreenData> listObjects = new ArrayList<ClassScreenData>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_SCREEN_DATA, PROJECTION_SCREEN_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenData object = new ClassScreenData();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.type_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.type_id));
					object.type_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.type_name));
					object.screen_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.screen_id));
					object.screen_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.screen_name));
					object.section_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.section_id));
					object.section_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.section_name));
					object.section_value = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.section_value));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ClassStaff insertStaffData(ClassStaff object) {
		try {
			object.pk_id = (int) insertObject(mDbHelper.TABLE_STAFF_DATA, createContentValuesStaffData(object));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	public ClassStaff updateStaffData(ClassStaff object) {
		try {
			this.open();
			String format = "%s = %s";
			String whereClause = String.format(format, mDbHelper.pk_id, object.pk_id);
			long id = mDb.update(mDbHelper.TABLE_STAFF_DATA, createContentValuesStaffData(object), whereClause, null);
			if (id < 1) {
				id = mDb.insert(mDbHelper.TABLE_STAFF_DATA, null, createContentValuesStaffData(object));
			}
			object.pk_id = (int) id;
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	private ContentValues createContentValuesStaffData(ClassStaff object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.fk_id, object.fk_id);
		cv.put(mDbHelper.staff_name, object.staff_name);
		cv.put(mDbHelper.staff_designation_id, object.staff_designation_id);
		cv.put(mDbHelper.staff_mobile, object.staff_mobile);
		cv.put(mDbHelper.staff_cnic, object.staff_cnic);
		cv.put(mDbHelper.staff_role, object.staff_role);
		return cv;
	}

	private ArrayList<ClassStaff> selectStaffDataWhere(String whereClause) {
		try {
			ArrayList<ClassStaff> listObjects = new ArrayList<ClassStaff>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_STAFF_DATA, PROJECTION_STAFF_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassStaff object = new ClassStaff();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.fk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.fk_id));
					object.staff_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.staff_name));
					object.staff_designation_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.staff_designation_id));
					object.staff_mobile = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.staff_mobile));
					object.staff_cnic = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.staff_cnic));
					object.staff_role = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.staff_role));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ClassFacilityData insertFacilityData(ClassFacilityData object) {
		try {
			ClassFacilityData objectNew = selectFacilityData(object.facility_id, object.facility_type_code);
			if (objectNew == null) {
				object.pk_id = (int) insertObject(mDbHelper.TABLE_FACILITY_DATA, createContentValuesFacilityData(object));
				for (ClassStaff objectStaff : object.arrayListStaff) {
					objectStaff.fk_id = object.pk_id;
					objectStaff = insertStaffData(objectStaff);
				}
				return object;
			} else {
				return objectNew;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ContentValues createContentValuesFacilityData(ClassFacilityData object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.facility_id, object.facility_id);
		cv.put(mDbHelper.facility_type_code, object.facility_type_code);
		cv.put(mDbHelper.facility_type, object.facility_type);
		cv.put(mDbHelper.facility_name, object.facility_name);
		cv.put(mDbHelper.tehsil_id, object.tehsil_id);
		cv.put(mDbHelper.tehsil_name, object.tehsil_name);
		cv.put(mDbHelper.district_id, object.district_id);
		cv.put(mDbHelper.district_name, object.district_name);
		cv.put(mDbHelper.json_arrays_data, object.json_arrays_data);
		cv.put(mDbHelper.facility_nutrition_second_type, object.facility_nutrition_second_type);
		cv.put(mDbHelper.facility_sentinel_second_type, object.facility_sentinel_second_type);
		return cv;
	}

	public ArrayList<String> selectFacilityIds(String facilityTypeCode) {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s = '%s'", mDbHelper.facility_type_code, facilityTypeCode);
			return selectSingleColumnValues(true, new String[] { mDbHelper.facility_id }, mDbHelper.TABLE_FACILITY_DATA, whereClause, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getFacilityName(String facilityId) {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s = '%s'", mDbHelper.facility_id, facilityId);
			return selectSingleColumnValues(true, new String[] { mDbHelper.facility_name }, mDbHelper.TABLE_FACILITY_DATA, whereClause, null).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<String> selectFacilityNames(String facilityTypeCode) {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s = '%s'", mDbHelper.facility_type_code, facilityTypeCode);
			return selectSingleColumnValues(true, new String[]{mDbHelper.facility_name}, mDbHelper.TABLE_FACILITY_DATA, whereClause, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<ClassScreenItem> selectFacilityNamesAndIds(String facilityTypeCode) {
		try {
			this.open();
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			String whereClause = String.format(Locale.ENGLISH, "%s = '%s'", mDbHelper.facility_type_code, facilityTypeCode);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, new String[] { mDbHelper.facility_name, mDbHelper.facility_id }, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_name));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ClassFacilityData selectFacilityData(String facilityID, String facilityTypeCode) {
		try {
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s'", mDbHelper.facility_id, facilityID, mDbHelper.facility_type_code, facilityTypeCode);
			ArrayList<ClassFacilityData> arrayListFacilityData = selectFacilityDataWhere(whereClause);
			return arrayListFacilityData.size() > 0 ? arrayListFacilityData.get(0) : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ClassFacilityData selectFacilityByName(String facilityID, String facilityName) {
		try {
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s'", mDbHelper.facility_id, facilityID, mDbHelper.facility_name, facilityName);
			ArrayList<ClassFacilityData> arrayListFacilityData = selectFacilityDataWhere(whereClause);
			return arrayListFacilityData.size() > 0 ? arrayListFacilityData.get(0) : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ArrayList<ClassFacilityData> selectFacilityDataWhere(String whereClause) {
		try {
			this.open();
			ArrayList<ClassFacilityData> listObjects = new ArrayList<ClassFacilityData>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, PROJECTION_FACILITY_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassFacilityData object = new ClassFacilityData();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.facility_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_id));
					object.facility_type_code = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_type_code));
					object.facility_type = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_type));
					object.facility_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_name));
					object.tehsil_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.tehsil_id));
					object.tehsil_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.tehsil_name));
					object.district_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.district_id));
					object.district_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.district_name));
					object.facility_nutrition_second_type = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_nutrition_second_type));
					object.facility_sentinel_second_type = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_sentinel_second_type));
					object.json_arrays_data = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.json_arrays_data));
					whereClause = String.format(MainContainer.mLocale, "%s = %s", mDbHelper.fk_id, object.pk_id);
					object.arrayListStaff = selectStaffDataWhere(whereClause);
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<ClassDataActivity> selectAllActivities(String... activityType) {
		ArrayList<ClassDataActivity> listObjects = new ArrayList<ClassDataActivity>();
		try {
			this.open();
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s'", mDbHelper.activity_type, activityType);
			whereClause = null;
			if (activityType.length > 0) {
				whereClause = String.format(MainContainer.mLocale, "%s = '%s'", mDbHelper.activity_type, activityType[0]);
				for (int i = 1; i < activityType.length; i++) {
					whereClause = whereClause
							+ String.format(MainContainer.mLocale, " or %s = '%s'", mDbHelper.activity_type, activityType[i]);
				}
			}
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_ACTIVITY_DATA, PROJECTION_ACTIVITY, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassDataActivity object = new ClassDataActivity(mContext);
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.app_version_no = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.app_version_no));
					object.activity_type = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.activity_type));
					object.sending_date_time = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.sending_date_time));
					object.json_data = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.json_data));

					object.arrayListPictures = selectAllPictures(object.pk_id);
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listObjects;
	}

	private ArrayList<ClassDataPicture> selectAllPictures(int pk_id) {
		ArrayList<ClassDataPicture> listObjects = new ArrayList<ClassDataPicture>();
		try {
			String whereClause = String.format(MainContainer.mLocale, "%s = %s", mDbHelper.fk_id, pk_id);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_PICTURE_DATA, PROJECTION_PICTURE, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassDataPicture object = new ClassDataPicture();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.fk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.fk_id));
					object.picture_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_name));
					object.picture_parameter = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_parameter));
					object.picture_status = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_status));
					object.picture_data = queryCursor.getBlob(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_data));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listObjects;
	}

	public ClassDataPicture selectSavedPictures(String parameter) {
		ClassDataPicture listObjects = new ClassDataPicture();
		try {
			this.open();
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s' and %s = '%s'", mDbHelper.picture_status, Constants.PICTURE_SAVE_DATA,
					mDbHelper.picture_parameter, parameter);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_PICTURE_DATA, PROJECTION_PICTURE, whereClause, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassDataPicture object = new ClassDataPicture();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.fk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.fk_id));
					object.picture_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_name));
					object.picture_parameter = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_parameter));
					object.picture_status = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_status));
					object.picture_data = queryCursor.getBlob(queryCursor.getColumnIndexOrThrow(mDbHelper.picture_data));
					deletePicture(object.pk_id);
					return object;
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public boolean deleteSavedPictures() {
		try {
			String whereClause = String.format(MainContainer.mLocale, "%s = '%s'", mDbHelper.picture_status, Constants.PICTURE_SAVE_DATA);
			//String whereClause = String.format(Locale.ENGLISH, "%s=%s", mDbHelper.pk_id, pk_id);
			return deleteObject(mDbHelper.TABLE_PICTURE_DATA, whereClause) > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteActivity(int pk_id) {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s=%s", mDbHelper.pk_id, pk_id);
			return deleteObject(mDbHelper.TABLE_ACTIVITY_DATA, whereClause) > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public boolean deletePicture(int pk_id) {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s=%s", mDbHelper.pk_id, pk_id);
			return deleteObject(mDbHelper.TABLE_PICTURE_DATA, whereClause) > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteSavedPicture() {
		try {
			String whereClause = String.format(Locale.ENGLISH, "%s='%s'", mDbHelper.picture_status, Constants.PICTURE_SAVE_DATA);
			return deleteObject(mDbHelper.TABLE_PICTURE_DATA, whereClause) > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public long insertSentData(ClassSentData object) {
		try {
			this.open();
			long id = 0;
			String format = "%s =='%s'";
			String whereClause = String.format(format, mDbHelper.facility_id, object.facility_id);
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_SENT_DATA, PROJECTION_SENT_DATA, whereClause, null, null, null, null, null);
			if (queryCursor == null || !queryCursor.moveToFirst()) {
				id = mDb.insert(mDbHelper.TABLE_SENT_DATA, null, createContentValuesSentData(object));
			} else {
				id = mDb.update(mDbHelper.TABLE_SENT_DATA, createContentValuesSentData(object), whereClause, null);
			}
			this.close();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	private ContentValues createContentValuesSentData(ClassSentData object) {
		ContentValues cv = new ContentValues();
		cv.put(mDbHelper.sending_date_time, object.sending_date_time);
		cv.put(mDbHelper.facility_id, object.facility_id);
		return cv;
	}

	public void deleteSentData(ClassSentData object) {
		try {
			this.open();
			String where = mDbHelper.pk_id + " = " + object.pk_id;
			mDb.delete(mDbHelper.TABLE_SENT_DATA, where, null);
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearOldData() {
		try {
			SimpleDateFormat sdfDateSQL = new SimpleDateFormat(Constants.FORMAT_DATE_APP, MainContainer.mLocale);
			Calendar dateCurrent = Calendar.getInstance();
			SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(MainContainer.mContext);
			String lastClearDate = sharedPreferencesEditor.getLastClearDate();
			if (lastClearDate.length() > 0) {
				Calendar dateLastClear = Calendar.getInstance();
				dateLastClear.setTime(sdfDateSQL.parse(lastClearDate));
				if (dateCurrent.get(Calendar.MONTH) != dateLastClear.get(Calendar.MONTH) || dateCurrent.get(Calendar.YEAR) != dateLastClear.get(Calendar.YEAR)) {
					ArrayList<ClassSentData> listSentData = selectAllSentData();
					for (ClassSentData objectSentData : listSentData) {
						Calendar dateTimeSentData = Calendar.getInstance();
						dateTimeSentData.setTime(sdfDateSQL.parse(objectSentData.sending_date_time));
						if (dateTimeSentData.get(Calendar.MONTH) != dateCurrent.get(Calendar.MONTH) || dateTimeSentData.get(Calendar.YEAR) != dateCurrent.get(Calendar.YEAR)) {
							deleteSentData(objectSentData);
						}
					}
				}
				sharedPreferencesEditor.setLastClearDate(sdfDateSQL.format(dateCurrent.getTime()));
			} else {
				sharedPreferencesEditor.setLastClearDate(sdfDateSQL.format(dateCurrent.getTime()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> selectAllSentDataIds() {
		return selectSingleColumnValues(true, new String[] { mDbHelper.facility_id }, mDbHelper.TABLE_SENT_DATA, null, null);
	}

	public ArrayList<ClassSentData> selectAllSentData() {
		try {
			this.open();
			ArrayList<ClassSentData> listObjects = new ArrayList<ClassSentData>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_SENT_DATA, PROJECTION_SENT_DATA, null, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassSentData object = new ClassSentData();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.sending_date_time = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.sending_date_time));
					object.facility_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_id));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<ClassScreenItem> selectAllDistricts() {
		try {
			this.open();
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			Cursor queryCursor = mDb.query(true, mDbHelper.TABLE_FACILITY_DATA, new String[] { mDbHelper.district_id, mDbHelper.district_name }, null, null, null, null, mDbHelper.district_id, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.pk_id = -1;
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.district_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.district_name));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			Collections.sort(listObjects, new Comparator<ClassScreenItem>() {

				@Override
				public int compare(ClassScreenItem lhs, ClassScreenItem rhs) {
					try {
						return Integer.parseInt(lhs.item_id) - Integer.parseInt(rhs.item_id);
					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				}
			});
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<ClassScreenItem> selectAllTehsils(String districtID) {
		try {
			this.open();
			String where = mDbHelper.district_id + " = " + districtID;
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			Cursor queryCursor = mDb.query(true, mDbHelper.TABLE_FACILITY_DATA, new String[] { mDbHelper.district_id, mDbHelper.tehsil_id, mDbHelper.tehsil_name }, where, null, null, null,
					mDbHelper.tehsil_id, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.pk_id = -1;
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.tehsil_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.tehsil_name));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			Collections.sort(listObjects, new Comparator<ClassScreenItem>() {

				@Override
				public int compare(ClassScreenItem lhs, ClassScreenItem rhs) {
					try {
						return Integer.parseInt(lhs.item_id) - Integer.parseInt(rhs.item_id);
					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				}
			});
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<ClassScreenItem> selectAllFacilities(String districtID, String tehsilID) {
		try {
			this.open();
			String where = mDbHelper.district_id + " = " + districtID + " and " + mDbHelper.tehsil_id + " = " + tehsilID;
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, new String[] { mDbHelper.pk_id, mDbHelper.district_id, mDbHelper.tehsil_id, mDbHelper.facility_id, mDbHelper.facility_name },
					where, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_name));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<ClassScreenItem> selectAllFacilities(String districtID, String tehsilID ,int facility_second_type) {
		try {
			this.open();
			String where = "";
			if(facility_second_type == Constants.FACILITY_SECOND_TYPE_NUTRITION)
			 	where = mDbHelper.district_id + " = " + districtID + " and " + mDbHelper.tehsil_id + " = " + tehsilID + " and " + mDbHelper.facility_nutrition_second_type + " in (1,2,3)";
			else if(facility_second_type == Constants.FACILITY_SECOND_TYPE_HEPATITIS)
				where = mDbHelper.district_id + " = " + districtID + " and " + mDbHelper.tehsil_id + " = " + tehsilID + " and " + mDbHelper.facility_sentinel_second_type + " in (1)";
			ArrayList<ClassScreenItem> listObjects = new ArrayList<ClassScreenItem>();
			Cursor queryCursor = mDb.query(mDbHelper.TABLE_FACILITY_DATA, new String[] { mDbHelper.pk_id, mDbHelper.district_id, mDbHelper.tehsil_id, mDbHelper.facility_id, mDbHelper.facility_name },
					where, null, null, null, null, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					ClassScreenItem object = new ClassScreenItem();
					object.pk_id = queryCursor.getInt(queryCursor.getColumnIndexOrThrow(mDbHelper.pk_id));
					object.item_id = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_id));
					object.item_name = queryCursor.getString(queryCursor.getColumnIndexOrThrow(mDbHelper.facility_name));
					listObjects.add(object);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private DbAdapter open() throws SQLException {
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	private void close() {
		try {
			mDbHelper.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long insertObject(String tableName, ContentValues cvData) {
		try {
			this.open();
			long id = mDb.insert(tableName, null, cvData);
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	private int deleteObject(String tableName, String whereClause) {
		try {
			this.open();
			int nDeleted = mDb.delete(tableName, whereClause, null);
			this.close();
			return nDeleted;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	private boolean isTableEmpty(String tableName, String[] columnsName) {
		try {
			this.open();
			boolean isTableEmpty = true;
			Cursor queryCursor = mDb.query(tableName, columnsName, null, null, null, null, null, null);
			if (queryCursor != null && queryCursor.moveToFirst()) {
				isTableEmpty = false;
			}
			this.close();
			return isTableEmpty;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	private void deleteTableData(String... tableNames) {
		this.open();
		for (String tableName : tableNames) {
			mDb.execSQL("delete from " + tableName);
		}
		this.close();
	}

	private ArrayList<String> selectSingleColumnValues(boolean distinct, String[] projection, String tableName, String whereClause, String orderBy) {
		try {
			this.open();
			ArrayList<String> listObjects = new ArrayList<String>();
			Cursor queryCursor = mDb.query(distinct, tableName, projection, whereClause, null, null, null, orderBy, null);
			if (queryCursor == null) {
				this.close();
				return null;
			} else if (queryCursor.moveToFirst()) {
				do {
					String obj = queryCursor.getString(0).trim();
					if (obj.length() > 0)
						listObjects.add(obj);
				} while (queryCursor.moveToNext());
			}
			queryCursor.close();
			this.close();
			return listObjects;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}