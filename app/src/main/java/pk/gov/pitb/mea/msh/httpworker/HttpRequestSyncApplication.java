package pk.gov.pitb.mea.msh.httpworker;

import android.content.Context;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Locale;

import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import pk.gov.pitb.mea.msh.handlers.HandlerAction;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassFacilityData;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.models.ClassStaff;

public class HttpRequestSyncApplication {

	private SharedPreferencesEditor sharedPreferencesEditor;

	public HttpRequestSyncApplication(Context context) {
		this.sharedPreferencesEditor = new SharedPreferencesEditor(context);
	}

	public HandlerAction httpRequestSyncFacilities() {
		HandlerAction handlerAction = new HandlerAction();
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(String.format(Locale.ENGLISH, Constants.URL_SYNC_FACILITY, MainContainer.mImeiNumber, sharedPreferencesEditor.getFacilityVersion()));
			httpClient.getConnectionManager();
			/*HttpParams httpParams = httpClient.getParams();
			httpParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5 * 60 * 1000);
			httpParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,2 * 60 * 1000 );*/
			HttpResponse httpResponse = httpClient.execute(httpGet);
			//String apiResult = EntityUtils.toString(httpResponse.getEntity());
			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			//BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			//sb.append(reader.readLine() + "\n");
			String line="0";
			while ((line = reader.readLine()) != null)
				sb.append(line+"\n");

			is.close();
			String apiResult =sb.toString();
			JSONObject jsonObject = new JSONObject(apiResult);
			String status = jsonObject.has("status") ? jsonObject.getString("status") : "true";
			if (status.equals("true")) {
				MainContainer.mDbAdapter.resetFacility();
				String facilities = jsonObject.getString("facilities");
				JSONArray jfacilitiesArray = new JSONArray(facilities);

				for (int i = 0; i < jfacilitiesArray.length(); i++) {
					JSONObject json_data = jfacilitiesArray.getJSONObject(i);
					ClassFacilityData objectFacilities = new ClassFacilityData();

					JSONObject jsonObjectArraysData = new JSONObject();
					String valueStaff = json_data.has("Staff") ? json_data.getString("Staff") : "[]";
					JSONArray jArrayStaff = null;
					if (valueStaff.equals("null")) {
						jArrayStaff = new JSONArray();
					} else {
						jArrayStaff = new JSONArray(valueStaff);
					}
					jsonObjectArraysData.put("Staff", jArrayStaff);

					JSONObject jsonObjectSanctionedStaff = json_data.has("sanctioned_staff") ? json_data.getJSONObject("sanctioned_staff") : new JSONObject();
					jsonObjectArraysData.put("sanctioned_staff", jsonObjectSanctionedStaff);

					JSONObject jsonObjectTotalAvailable = json_data.has("total_available_medical_equipment") ? json_data.getJSONObject("total_available_medical_equipment") : new JSONObject();
					jsonObjectArraysData.put("total_available_medical_equipment", jsonObjectTotalAvailable);

					objectFacilities.facility_id = json_data.getString("facility_id");
					objectFacilities.facility_type_code = json_data.getString("facility_type_code");
					objectFacilities.facility_type = json_data.getString("facility_type");
					objectFacilities.facility_name = json_data.getString("facility_name");
					objectFacilities.tehsil_id = json_data.getString("tehsil_id");
					objectFacilities.tehsil_name = json_data.getString("tehsil_name");
					objectFacilities.district_id = json_data.getString("district_id");
					objectFacilities.district_name = json_data.getString("district_name");
					objectFacilities.facility_nutrition_second_type = json_data.getString("nutrition_second_type");
					objectFacilities.facility_sentinel_second_type = json_data.getString("hepatitis_second_type");
					objectFacilities.json_arrays_data = jsonObjectArraysData.toString();
					objectFacilities.arrayListStaff = getListStaff(objectFacilities.json_arrays_data, "Staff");
					MainContainer.mDbAdapter.insertFacilityData(objectFacilities);
				}
				String facilityVersion = jsonObject.getString("facility_version");
				sharedPreferencesEditor.setFacilityVersion(facilityVersion);
				String isPMO = jsonObject.getString("pmo");
				sharedPreferencesEditor.setIsPMO(isPMO);
				handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_SUCCESS;
			} else {
				handlerAction.handlerActionString = jsonObject.has("message") ? jsonObject.getString("message") : handlerAction.handlerActionString;
				handlerAction.handlerErrorCode = jsonObject.has("error_code") ? Integer.parseInt(jsonObject.getString("error_code")) : -1;
				handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_ERROR;
			}
		} catch (IOException e) {
			e.printStackTrace();
			handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
		} catch (Exception e) {
			e.printStackTrace();
			handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_EXCEPTION;
		}
		return handlerAction;
	}

	public HandlerAction httpRequestSyncStructure() {
		HandlerAction handlerAction = new HandlerAction();
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(String.format(Locale.ENGLISH, Constants.URL_SYNC_STRUCTURE, MainContainer.mImeiNumber, sharedPreferencesEditor.getStructuralVersion()));
			httpClient.getConnectionManager();
			HttpResponse httpResponse = httpClient.execute(httpGet);
			String apiResult = EntityUtils.toString(httpResponse.getEntity());

			JSONObject jsonObject = new JSONObject(apiResult);
			String status = jsonObject.has("status") ? jsonObject.getString("status") : "true";
			if (status.equals("true")) {
				MainContainer.mDbAdapter.resetStructure();
				MainContainer.mDbAdapter.resetSentData();
				ClassScreenData objectScreenData = new ClassScreenData();

				apiResult = jsonObject.getString(Constants.DB_TYPE_NAME_DHQ);
				objectScreenData.type_id = Constants.DB_TYPE_ID_DHQ;
				objectScreenData.type_name = Constants.DB_TYPE_NAME_DHQ;
				loadStructureData(apiResult, objectScreenData);
				objectScreenData.setEmptyValues();

				apiResult = jsonObject.getString(Constants.DB_TYPE_NAME_THQ);
				objectScreenData.type_id = Constants.DB_TYPE_ID_THQ;
				objectScreenData.type_name = Constants.DB_TYPE_NAME_THQ;
				loadStructureData(apiResult, objectScreenData);
				objectScreenData.setEmptyValues();

				apiResult = jsonObject.getString(Constants.DB_TYPE_NAME_DHQTEACHING);
				objectScreenData.type_id = Constants.DB_TYPE_ID_DHQTEACHING;
				objectScreenData.type_name = Constants.DB_TYPE_NAME_DHQTEACHING;
				loadStructureData(apiResult, objectScreenData);
				objectScreenData.setEmptyValues();

				apiResult = jsonObject.getString(Constants.DB_TYPE_NAME_NUTRITION);
				objectScreenData.type_id = Constants.DB_TYPE_ID_NUTRITION;
				objectScreenData.type_name = Constants.DB_TYPE_NAME_NUTRITION;
				loadStructureData(apiResult, objectScreenData);
				objectScreenData.setEmptyValues();

				apiResult = jsonObject.getString(Constants.DB_TYPE_NAME_HEPATITIS);
				objectScreenData.type_id = Constants.DB_TYPE_ID_HEPATITIS;
				objectScreenData.type_name = Constants.DB_TYPE_NAME_HEPATITIS;
				loadStructureData(apiResult, objectScreenData);
				objectScreenData.setEmptyValues();

				objectScreenData.setEmptyValues();
				apiResult = jsonObject.getString((Constants.DB_TYPE_NAME_PATIENT_INTERVIEWS));
				objectScreenData.screen_id = Constants.DB_SCREEN_ID_PATIENT_INTERVIEWS;
				objectScreenData.screen_name = Constants.DB_TYPE_NAME_PATIENT_INTERVIEWS;
				loadPatientInterviews(apiResult, objectScreenData);

				String structuralVersion = jsonObject.getString("structural_version");
				sharedPreferencesEditor.setStructuralVersion(structuralVersion);
				// String formType = jsonObject.getString("form_type");
				// sharedPreferencesEditor.setFormType(formType);
				handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_SUCCESS;
			} else {
				handlerAction.handlerActionString = jsonObject.has("message") ? jsonObject.getString("message") : handlerAction.handlerActionString;
				handlerAction.handlerErrorCode = jsonObject.has("error_code") ? Integer.parseInt(jsonObject.getString("error_code")) : -1;
				handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_ERROR;
			}
		} catch (SocketException e) {
			e.printStackTrace();
			handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			handlerAction.handlerActionCode = Constants.AC_INTERNET_NOT_WORKING;
		} catch (Exception e) {
			e.printStackTrace();
			handlerAction.handlerActionCode = Constants.AC_HTTP_REQUEST_EXCEPTION;
		}
		return handlerAction;
	}
	private void loadPatientInterviews(String screenData, ClassScreenData obj) {
		try {
			JSONObject jsonObject = new JSONObject(screenData);
			String valueData = "";

			String key="money_spend_on";
			/*valueData = jsonObject.getString(key);
			loadItems(obj, "", "", valueData,key,"peiq_id", "option" );*/

			key="see_today";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="age_group";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );


//			key="patient_visit_group";
//			valueData = jsonObject.getString(key);
//			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="illness";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="time_spend";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="time_to_reach_facility";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			/*key="transport_to_reach_facility";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );*/


			key="patient_is_visiting";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="wait_for_person";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );



			key="what_money_spend_on";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="opinion_what_changed_at_hospital";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

//			key="recommend_this_hospital";
//			valueData = jsonObject.getString(key);
//			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );


			key="seated_in_waiting_area";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );


			key="systematic_way_in_waiting_area_for_queue";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );



			key="waiting_area_floor_clean_litter_free";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="ward_floor_clean_litter_free";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );


			key="washroom_floor_clean_litter_free";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="sink_in_washroom_clean_soap_available";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );

			key="observed_any_change_in_hospital";
			valueData = jsonObject.getString(key);
			loadStructureItems(obj, "", "", valueData,key,"peiq_id", "option" );


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadStructureData(String screenData, ClassScreenData obj) {
		try {
			JSONObject jsonObject = new JSONObject(screenData);

			String type = "";
			if (jsonObject.has(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_VACANCY_DATA)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_VACANCY_DATA);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_VACANCY_DATA);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_MEDICINES)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_MEDICINES);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_MEDICINES);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_SUPPLIES)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_SUPPLIES);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_SUPPLIES);
			}
			if (jsonObject.has(Constants.DB_SCREEN_NAME_EQUIPMENT)) {
				type = jsonObject.getString(Constants.DB_SCREEN_NAME_EQUIPMENT);
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_EQUIPMENT);
			}

			if (jsonObject.has(Constants.DB_SCREEN_NAME_OTP)) {
				type = jsonObject.has(Constants.DB_SCREEN_NAME_OTP) ? jsonObject.getString(Constants.DB_SCREEN_NAME_OTP) : "";
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_OTP);
			}

			if (jsonObject.has(Constants.DB_SCREEN_NAME_SCS)) {
				type = jsonObject.has(Constants.DB_SCREEN_NAME_SCS) ? jsonObject.getString(Constants.DB_SCREEN_NAME_SCS) : "";
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_SCS);
			}

			if (jsonObject.has(Constants.DB_SCREEN_NAME_WMI)) {
				type = jsonObject.has(Constants.DB_SCREEN_NAME_WMI) ? jsonObject.getString(Constants.DB_SCREEN_NAME_WMI) : "";
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_WMI);
			}

			if (jsonObject.has(Constants.DB_SCREEN_NAME_SENTINEL)) {
				type = jsonObject.has(Constants.DB_SCREEN_NAME_SENTINEL) ? jsonObject.getString(Constants.DB_SCREEN_NAME_SENTINEL) : "";
				loadStructureTypeData(obj, type, Constants.DB_SCREEN_NAME_SENTINEL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadStructureTypeData(ClassScreenData obj, String typeData, String type) {
		try {
			String id = "";
			String name = "";
			String value = "";

			JSONObject jsonObject = new JSONObject(typeData);
			if (type.equals(Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_SELF_REPORTED_INDICATORS;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "sri_id";
					name = "indicator";
					loadStructureItems(obj, type, "", value, "fields", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "location_id";
					name = "location";
					loadStructureItems(obj, type, "", value, "fields", id, name);
					id = "location_id";
					name = "disabled_condition_id";
					loadStructureItems(obj, type, "", value, "disabled_fields", id, name);
				}
				value = jsonObject.getString("dropdown");
				if (value.trim().length() > 0) {
					id = "oc_id";
					name = "condition";
					loadStructureItems(obj, type, "", value, "dropdown", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "mowd_id";
					name = "field_name";
					loadStructureItems(obj, type, "", value, "fields", id, name);
				}
				value = jsonObject.getString("dropdown");
				if (value.trim().length() > 0) {
					id = "dwo_id";
					name = "disposal_option";
					loadStructureItems(obj, type, "", value, "dropdown", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_VACANCY_DATA)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_VACANCY_DATA;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "vp_id";
					name = "position";
					loadStructureItemsMultiple(obj, type, value, "label", "fields", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_DATA_ON_ABSENCES;
				obj.screen_name = type;
				value = jsonObject.getString("designation_dropdown");
				if (value.trim().length() > 0) {
					id = "as_id";
					name = "designation";
					loadStructureItems(obj, type, "", value, "designation_dropdown", id, name);
				}
				value = jsonObject.getString("type_of_absence_dropdown");
				if (value.trim().length() > 0) {
					id = "atoa_id";
					name = "type_of_absence";
					loadStructureItems(obj, type, "", value, "type_of_absence_dropdown", id, name);

				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_MEDICINES)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_MEDICINES;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "medicine_id";
					name = "medicines";
					loadStructureItemsMultiple(obj, type, value, "label", "fields", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_SUPPLIES)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_SUPPLIES;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "supply_id";
					name = "supply_name";
					loadStructureItems(obj, type, "", value, "fields", id, name);
				}
			}
			if (type.equals(Constants.DB_SCREEN_NAME_EQUIPMENT)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_EQUIPMENT;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "eq_id";
					name = "equipment_name";
					loadStructureItemsMultiple(obj, type, value, "label", "fields", id, name);
				}
			}

			if (type.equals(Constants.DB_SCREEN_NAME_WMI)) {
				obj.screen_id = Constants.DB_SCREEN_NAME_WMI;
				obj.screen_name = type;
				/*obj.screen_id = Constants.DB_SCREEN_NAME_WMI;
				obj.screen_name = type;
				value = jsonObject.getString("fields");
				if (value.trim().length() > 0) {
					id = "wm_id";
					name = "wm_name";
					loadStructureItems(obj, type, "", value, "fields", id, name);
				}*/
				for (int i = 1 ; i <= jsonObject.length(); i++){
					value = jsonObject.getString("section"+i);
					JSONObject json_section1 = new JSONObject(value);
					if (value.trim().length() > 0) {
						String section = "section"+i;
						value = json_section1.getString("fields");
						if (value.trim().length() > 0) {
							id = "wm_id";
							name = "wm_name";
							loadStructureItems(obj, type, section, value, "fields", id, name);
						}
					}
				}
			}

			if (type.equals(Constants.DB_SCREEN_NAME_OTP)) {
				obj.screen_id = Constants.DB_SCREEN_ID_OTP;
				obj.screen_name = type;

				/*for (int i = 1 ; i <= jsonObject.length(); i++){
					value = jsonObject.getString("section"+i);
					JSONObject json_section1 = new JSONObject(value);
					if (value.trim().length() > 0) {
						String section = "section"+i;
						value = json_section1.getString("fields");
						if (value.trim().length() > 0) {
							id = "otp_id";
							name = "otp_name";
							loadStructureItems(obj, type, section, value, "fields", id, name);
						}
					}*/

					value = jsonObject.getString("section1");
					JSONObject json_section1 = new JSONObject(value);
					if (value.trim().length() > 0) {
						String section = "section1";
						value = json_section1.getString("fields");
						if (value.trim().length() > 0) {
							id = "otp_id";
							name = "otp_name";
							loadStructureItems(obj, type, section, value, "fields", id, name);
							id = "otp_id";
							name = "non_functional_repairable_enabled";
							loadStructureItems(obj, type, section, value, "non_functional_repairable_enabled", id, name);
						}
					}
				value = jsonObject.getString("section2");
				JSONObject json_section2 = new JSONObject(value);
				if (value.trim().length() > 0) {
					String section = "section2";
					value = json_section2.getString("fields");
					if (value.trim().length() > 0) {
						id = "otp_id";
						name = "otp_name";
						loadStructureItems(obj, type, section, value, "fields", id, name);
					}
				}
				value = jsonObject.getString("section3");
				JSONObject json_section3 = new JSONObject(value);
				if (value.trim().length() > 0) {
					String section = "section3";
					value = json_section3.getString("fields");
					if (value.trim().length() > 0) {
						id = "otp_id";
						name = "otp_name";
						loadStructureItems(obj, type, section, value, "fields", id, name);
						id = "otp_id";
						name = "cured_enabled";
						loadStructureItems(obj, type, section, value, "cured_enabled", id, name);
						id = "otp_id";
						name = "stabilization_enabled";
						loadStructureItems(obj, type, section, value, "stabilization_enabled", id, name);
					}
				}
				value = jsonObject.getString("section4");
				JSONObject json_section4 = new JSONObject(value);
				if (value.trim().length() > 0) {
					String section = "section4";
					value = json_section4.getString("fields");
					if (value.trim().length() > 0) {
						id = "otp_id";
						name = "otp_name";
						loadStructureItems(obj, type, section, value, "fields", id, name);
					}
				}
				value = jsonObject.getString("section5");
				JSONObject json_section5 = new JSONObject(value);
				if (value.trim().length() > 0) {
					String section = "section5";
					value = json_section5.getString("fields");
					if (value.trim().length() > 0) {
						id = "otp_id";
						name = "otp_name";
						loadStructureItems(obj, type, section, value, "fields", id, name);
					}
				}
			}

			if (type.equals(Constants.DB_SCREEN_NAME_SCS)) {
				obj.screen_id = Constants.DB_SCREEN_ID_SCS;
				obj.screen_name = type;

				for (int i = 1 ; i <= jsonObject.length(); i++){
					value = jsonObject.getString("section"+i);
					JSONObject json_section1 = new JSONObject(value);
					if (value.trim().length() > 0) {
						String section = "section"+i;
						value = json_section1.getString("fields");
						if (value.trim().length() > 0) {
							id = "sc_id";
							name = "sc_name";
							loadStructureItems(obj, type, section, value, "fields", id, name);
							id = "sc_id";
							name = "non_functional_repairable_enabled";
							loadStructureItems(obj, type, section, value, "non_functional_repairable_enabled", id, name);
						}
					}
				}

			}

			if (type.equals(Constants.DB_SCREEN_NAME_SENTINEL)) {
				obj.screen_id = Constants.DB_SCREEN_ID_SENTINEL;
				obj.screen_name = type;

				for (int i = 1 ; i <= jsonObject.length(); i++){
					value = jsonObject.getString("section"+i);
					JSONObject json_section1 = new JSONObject(value);
					if (value.trim().length() > 0) {
						String section = "section"+i;
						value = json_section1.getString("fields");
						if (value.trim().length() > 0) {
							id = "sentinel_id";
							name = "sentinel_name";
							loadStructureItems(obj, type, section, value, "fields", id, name);
						}
					}
				}

			}


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadStructureItems(ClassScreenData object, String type, String section, String valueData, String value, String ItemId, String ItemName) {
		ClassScreenData obj = new ClassScreenData();
		if (value.trim().length() > 0) {
			// if(!value.equals("null"))
			{
				if (section.length() > 0) {
					object.section_name = section;
				} else {
					object.section_name = "section1";
				}
				obj.type_id = object.type_id;
				obj.type_name = object.type_name;
				obj.section_name = object.section_name;
				obj.screen_id = object.screen_id;
				obj.screen_name = object.screen_name;
				obj.section_value = value;
				obj = MainContainer.mDbAdapter.insertScreenData(obj);
				try {
					if (!valueData.equals("null")) {
						JSONArray jFieldArray = new JSONArray(valueData);
						for (int i = 0; i < jFieldArray.length(); i++) {
							JSONObject json_data = jFieldArray.getJSONObject(i);
							ClassScreenItem itemObj = new ClassScreenItem();
							itemObj.fk_id = String.valueOf(obj.pk_id).toString();
							itemObj.item_id = json_data.getString(ItemId);
							itemObj.item_name = json_data.getString(ItemName);
							MainContainer.mDbAdapter.insertScreenItem(itemObj);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void loadStructureItemsMultiple(ClassScreenData object, String type, String valueData, String labelName, String valueName, String ItemId, String ItemName) {
		try {
			ClassScreenData obj = new ClassScreenData();
			JSONArray jsonArray = new JSONArray(valueData);
			for (int j = 0; j < jsonArray.length(); j++) {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				obj.section_name = "section" + (j);
				obj.section_value = jsonObject.getString(labelName);
				obj.type_id = object.type_id;
				obj.type_name = object.type_name;
				obj.screen_id = object.screen_id;
				obj.screen_name = object.screen_name;
				obj = MainContainer.mDbAdapter.insertScreenData(obj);
				try {
					if (!jsonObject.getString(valueName).equals("null")) {
						JSONArray jFieldArray = jsonObject.getJSONArray(valueName);
						for (int i = 0; i < jFieldArray.length(); i++) {
							JSONObject json_data = jFieldArray.getJSONObject(i);
							ClassScreenItem itemObj = new ClassScreenItem();
							itemObj.fk_id = String.valueOf(obj.pk_id).toString();
							itemObj.item_id = json_data.getString(ItemId);
							itemObj.item_name = json_data.getString(ItemName);
							MainContainer.mDbAdapter.insertScreenItem(itemObj);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadStructureItemsIfValid(ClassScreenData object, String type, String section, String valueData, String value, String ItemId, String ItemName) {
		ClassScreenData obj = new ClassScreenData();
		if (value.trim().length() > 0) {
			// if(!value.equals("null"))
			{
				if (section.length() > 0) {
					object.section_name = section;
				} else {
					object.section_name = "section1";
				}
				obj.type_id = object.type_id;
				obj.type_name = object.type_name;
				obj.section_name = object.section_name;
				obj.screen_id = object.screen_id;
				obj.screen_name = object.screen_name;
				obj.section_value = value;
				obj = MainContainer.mDbAdapter.insertScreenData(obj);
				try {
					if (!valueData.equals("null")) {
						JSONArray jFieldArray = new JSONArray(valueData);
						for (int i = 0; i < jFieldArray.length(); i++) {
							JSONObject json_data = jFieldArray.getJSONObject(i);
							ClassScreenItem itemObj = new ClassScreenItem();
							itemObj.fk_id = String.valueOf(obj.pk_id).toString();
							itemObj.item_id = json_data.getString(ItemId);
							itemObj.item_name = json_data.getString(ItemName);
							if (!itemObj.item_name.equals("null")) {
								MainContainer.mDbAdapter.insertScreenItem(itemObj);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	private ArrayList<ClassStaff> getListStaff(String jsonArrayData, String keyJson) {
		ArrayList<ClassStaff> listStaff = new ArrayList<ClassStaff>();
		try {
			JSONObject jsonObjectArrays = new JSONObject(jsonArrayData);
			JSONArray jsonArrayStaff = new JSONArray(jsonObjectArrays.has(keyJson) ? jsonObjectArrays.getString(keyJson) : "[]");
			for (int i = 0; i < jsonArrayStaff.length(); i++) {
				JSONObject jsonObject = jsonArrayStaff.getJSONObject(i);
				ClassStaff object = new ClassStaff();
				object.staff_name = jsonObject.has("staff_name") ? jsonObject.getString("staff_name") : "";
				object.staff_designation_id = jsonObject.has("staff_designation_id") ? jsonObject.getString("staff_designation_id") : "";
				object.staff_mobile = jsonObject.has("staff_mobile") ? jsonObject.getString("staff_mobile") : "";
				object.staff_cnic = jsonObject.has("staff_cnic") ? jsonObject.getString("staff_cnic") : "";
				object.staff_role = jsonObject.has("staff_role") ? jsonObject.getString("staff_role") : "";
				if (object.staff_name.equals("null")) {
					object.staff_name = "";
				}
				if (object.staff_designation_id.equals("null")) {
					object.staff_designation_id = "";
				}
				if (object.staff_mobile.equals("null")) {
					object.staff_mobile = "";
				}
				if (object.staff_cnic.equals("null")) {
					object.staff_cnic = "";
				}
				if (object.staff_role.equals("null")) {
					object.staff_role = "";
				}
				listStaff.add(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listStaff;
	}
}