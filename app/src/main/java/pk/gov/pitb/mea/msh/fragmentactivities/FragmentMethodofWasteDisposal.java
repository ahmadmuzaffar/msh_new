package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomSpinnerMultiSelect;

public class FragmentMethodofWasteDisposal extends Fragment implements InterfaceFragmentCallBack {

	private View parentView;
	private ArrayList<ClassScreenItem> arrayListDisposals;
	private ArrayList<ClassScreenItem> arrayListGeneralCondition;

	private LinearLayout linearLayoutMain;
	private TextView textViewGeneralFacility;
	private TableLayout tableLayoutGeneralFacility;

	@Override
	public void parseObject() {
		try {
			JSONArray jsonArrayDisposal = new JSONArray();
			for (int i = 1; i < tableLayoutGeneralFacility.getChildCount(); i++) {
				View child = tableLayoutGeneralFacility.getChildAt(i);
				ClassScreenItem object = arrayListDisposals.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					CustomSpinnerMultiSelect condition = (CustomSpinnerMultiSelect) tableRow.getChildAt(2);

					jsonObjectChild.put("mowd_id", object.item_id);
					jsonObjectChild.put("disposal_option", object.item_name);
					jsonObjectChild.put("dwo_id", condition.getSelectedItemsIDs());

					jsonArrayDisposal.put(jsonObjectChild);
				}
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("disposals", jsonArrayDisposal);
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL);
				JSONArray jsonArray = jsonObject.getJSONArray("disposals");
				int arrayCount = 0;
				for (int i = 1; i < tableLayoutGeneralFacility.getChildCount(); i++) {
					View child = tableLayoutGeneralFacility.getChildAt(i);
					ClassScreenItem object = arrayListDisposals.get(i - 1);
					if (child instanceof TableRow) {
						JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);

						TableRow tableRow = (TableRow) child;
						CustomSpinnerMultiSelect condition = (CustomSpinnerMultiSelect) tableRow.getChildAt(2);
						String conditionId = jsonObjectChild.getString("dwo_id");
						String [] conditionString = conditionId.split(",");
						condition.setSelectedItemsIDs((List<String>) Arrays.asList(conditionString));


						/*jsonObjectChild.put("mowd_id", object.item_id);
						jsonObjectChild.put("disposal_option", object.item_name);
						jsonObjectChild.put("dwo_id", condition.getSelectedItemsIDs());*/
					}
				}

			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_METHOD_OF_WASTE_DISPOSAL;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_METHOD_OF_WASTE_DISPOSAL;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListDisposals = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			objectScreenData.section_value = "dropdown";
			arrayListGeneralCondition = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			textViewGeneralFacility = new TextView(MainContainer.mContext);
			tableLayoutGeneralFacility = new TableLayout(MainContainer.mContext);

			linearLayoutMain.addView(textViewGeneralFacility);
			linearLayoutMain.addView(tableLayoutGeneralFacility);

			TableRow.LayoutParams[] paramsArrayTableRowColumnsCleanliness = new TableRow.LayoutParams[5];
			paramsArrayTableRowColumnsCleanliness[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsCleanliness[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumnsCleanliness.length; i++) {
				paramsArrayTableRowColumnsCleanliness[i].setMargins(2, 0, 0, 0);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsGeneral = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumnsGeneral[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsGeneral[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.46), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsGeneral[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.4), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumnsGeneral.length; i++) {
				paramsArrayTableRowColumnsGeneral[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewGeneralFacility, "Method of Disposal Waste", true);
			uiContainer.generateTableHeader(tableLayoutGeneralFacility, paramsArrayTableRowColumnsGeneral, new String[] { "No.", "Disposal", "Condition" });
			generateTableGeneralFacility(paramsArrayTableRowColumnsGeneral);

			if (tableLayoutGeneralFacility.getChildCount() > 1) {
				textViewGeneralFacility.setVisibility(View.VISIBLE);
				tableLayoutGeneralFacility.setVisibility(View.VISIBLE);
			} else {
				textViewGeneralFacility.setVisibility(View.GONE);
				tableLayoutGeneralFacility.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateTableGeneralFacility(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem item : arrayListDisposals) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextLocation = new CustomEditText(bIsOdd, item.item_name);
			CustomSpinnerMultiSelect spinnerCondition = new CustomSpinnerMultiSelect(bIsOdd, 3, item.item_name);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
			spinnerCondition.customiseSpinner(paramsArrayTableRowColumns[2]);
			spinnerCondition.initData(arrayListGeneralCondition);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextLocation);
			tableRow.addView(spinnerCondition);
			tableLayoutGeneralFacility.addView(tableRow);
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 1; i < tableLayoutGeneralFacility.getChildCount(); i++) {
				View child = tableLayoutGeneralFacility.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					CustomSpinnerMultiSelect condition = (CustomSpinnerMultiSelect) tableRow.getChildAt(2);
					if (condition.getSelectedItemsIDs() == null) {
						bIsValid = false;
					}
				}
			}
			if (!bIsValid) {
				errorMessage = "Please select at least one condition to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}