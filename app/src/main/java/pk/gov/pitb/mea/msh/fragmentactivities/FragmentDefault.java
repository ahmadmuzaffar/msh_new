package pk.gov.pitb.mea.msh.fragmentactivities;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentDefault extends Fragment implements InterfaceFragmentCallBack {

	private View parentView = null;

	@Override
	public void parseObject() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			generateBody();
		}
		return parentView;
	}

	private void generateBody() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			if (!bIsValid) {
				errorMessage = "Please fill all details to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}