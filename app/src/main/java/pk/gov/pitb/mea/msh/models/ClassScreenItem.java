package pk.gov.pitb.mea.msh.models;

public class ClassScreenItem {

	public int pk_id;
	public String item_id, item_name, fk_id;

	public ClassScreenItem() {
		reset();
	}

	public ClassScreenItem(String item_id, String item_name) {
		this.item_id = item_id;
		this.item_name = item_name;
	}

	public void reset() {
		pk_id = -1;
		item_id = "";
		item_name = "";
		fk_id = "";
	}

	@Override
	public String toString() {
		return item_name;
	}
}