package pk.gov.pitb.mea.msh.adapters;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpinnerScreenItem extends ArrayAdapter<ClassScreenItem> {

	private int colorBackground;
	private int colorText;

	public ArrayAdapterSpinnerScreenItem(Context context, ArrayList<ClassScreenItem> objects, int colorText, int colorBackground) {
		super(context, android.R.layout.simple_spinner_item, objects);
		this.colorBackground = colorBackground;
		this.colorText = colorText;
	}

	@Override
	public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
		ClassScreenItem objectFacility = getItem(position);
		if (textView != null) {
			if (position == 0) {
				textView.setTypeface(null, Typeface.BOLD);
				textView.setText(objectFacility.item_name);
			} else {
				textView.setTypeface(null, Typeface.NORMAL);
				textView.setText(objectFacility.item_name + " (" + objectFacility.item_id + ")");
			}
			if (colorBackground != -1) {
				textView.setBackgroundColor(colorBackground);
				textView.setTextColor(colorText);
			} else {
				textView.setBackgroundColor(-1);
				textView.setTextColor(Color.BLACK);
			}
		}
		return textView;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = super.getView(position, view, parent);
		ClassScreenItem objectFacility = getItem(position);
		if (view != null) {
			TextView textView = (TextView) view;
			textView.setTextColor(Color.WHITE);
			textView.setHintTextColor(colorText);
			textView.setText(objectFacility.item_name);
			// if (position == 0) {
			// textView.setHint(textView.getText().toString());
			// textView.setText("");
			// }
		}
		return view;
	}

	@Override
	public boolean isEnabled(int position) {
		return position == 0 ? false : true;
	}
}