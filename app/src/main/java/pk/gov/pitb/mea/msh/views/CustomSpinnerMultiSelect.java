package pk.gov.pitb.mea.msh.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.List;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.bo.ClassScreenItemMultiSelect;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;

public class CustomSpinnerMultiSelect extends Button implements View.OnClickListener {

	private ArrayList<ClassScreenItemMultiSelect> arrayListOriginalItems;
	private int countSelectedItems;

	private ListView listViewResults;
	private ListAdapterMultiSelect listAdapterMultiSelect;

	public CustomSpinnerMultiSelect(boolean bIsOdd, int type, String title) {
		super(MainContainer.mContext);
		changeBackground(bIsOdd, type);
		setHint("Condition(s) for " + title);
	}

	public CustomSpinnerMultiSelect(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomSpinnerMultiSelect(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	private void measureDialogHeight() {
		try {
			if (listAdapterMultiSelect != null) {
				FrameLayout.LayoutParams lpListView;
				View child = listAdapterMultiSelect.getView(0, null, listViewResults);
				child.measure(0, 0);
				int itemHeight = child.getMeasuredHeight();
				int maxCount = (int) ((MainContainer.mScreenHeight * 0.4) / itemHeight);
				if (listAdapterMultiSelect.getCount() > maxCount) {
					lpListView = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) ((maxCount + 0.5) * itemHeight));
				} else {
					lpListView = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
				}
				listViewResults.setLayoutParams(lpListView);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int drawableBackgroundEnable = 0;
	private int drawableBackgroundDisable = 0;
	@SuppressWarnings("unused")
	private int colorBackground = 0;
	private int colorText = 0;

	public void changeBackground(boolean bIsOdd, int type) {
		switch (type) {
			case 1:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_1_1 : R.drawable.spinner_1_1;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_1_1 : R.drawable.spinner_1_1;
			break;
			case 2:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_2 : R.drawable.spinner_even_enable_2;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_2 : R.drawable.spinner_even_disable_2;
			break;
			case 3:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_3 : R.drawable.spinner_even_enable_3;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_3 : R.drawable.spinner_even_disable_3;
			break;
			case 4:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_4 : R.drawable.spinner_even_enable_4;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_4 : R.drawable.spinner_even_disable_4;
			break;
			case 5:
				this.drawableBackgroundEnable = bIsOdd ? R.drawable.spinner_odd_enable_5 : R.drawable.spinner_even_enable_5;
				this.drawableBackgroundDisable = bIsOdd ? R.drawable.spinner_odd_disable_5 : R.drawable.spinner_even_disable_5;
			break;

			default:
			break;
		}
		if (isEnabled()) {
			setBackgroundResource(drawableBackgroundEnable);
		} else {
			setBackgroundResource(drawableBackgroundDisable);
		}
		this.colorBackground = bIsOdd ? MainContainer.mContext.getResources().getColor(R.color.row_body_background_odd) : MainContainer.mContext.getResources().getColor(
				R.color.row_body_background_even);
		this.colorText = MainContainer.mContext.getResources().getColor(R.color.row_body_text);
		this.setTextColor(this.colorText);
	}

	public void customiseSpinner(TableRow.LayoutParams paramsSpinner) {
		try {
			setLayoutParams(paramsSpinner);
			setPadding(0, 0, (int) (paramsSpinner.width * 0.2), 0);
			setBackgroundResource(drawableBackgroundEnable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setButtonText() {
		if (countSelectedItems == 0) {
			setText("Select Condition");
		} else if (countSelectedItems == arrayListOriginalItems.size()) {
			setText("All Conditions Selected");
		} else if (countSelectedItems == 1) {
			setText(countSelectedItems + " Condition Selected");
		} else {
			setText(countSelectedItems + " Conditions Selected");
		}
	}

	public void initData(ArrayList<ClassScreenItem> objects) {
		try {
			ArrayList<ClassScreenItemMultiSelect> data = new ArrayList<ClassScreenItemMultiSelect>();
			for (ClassScreenItem item : objects) {
				data.add(new ClassScreenItemMultiSelect(item, false));
			}
			repopulateData(data);
			setOnClickListener(this);
			setButtonText();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void repopulateData(ArrayList<ClassScreenItemMultiSelect> objects) {
		try {
			arrayListOriginalItems = objects;
			countSelectedItems = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getSelectedItemsCount() {
		return countSelectedItems;
	}

	public ArrayList<?> getSelectedItemsList() {
		ArrayList<Object> arrayListSelectedItems = new ArrayList<Object>();
		try {
			for (ClassScreenItemMultiSelect item : arrayListOriginalItems) {
				if (item.bIsSelected) {
					arrayListSelectedItems.add(item.item);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arrayListSelectedItems;
	}

	public boolean isAllItemsSelected() {
		return countSelectedItems == arrayListOriginalItems.size();
	}

	public String getSelectedItemsIDs() {
		String selectedIDs = null;
		try {
			for (ClassScreenItemMultiSelect item : arrayListOriginalItems) {
				if (item.bIsSelected) {
					if (selectedIDs == null) {
						selectedIDs = item.item.item_id;
					} else {
						selectedIDs = selectedIDs + "," + item.item.item_id;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selectedIDs;
	}

	public void setSelectedItemsIDs(List<String> itemIds) {

		try {
			for (int i = 0 ; i < arrayListOriginalItems.size(); i++) {
				if (itemIds.contains(arrayListOriginalItems.get(i).item.item_id)){
					arrayListOriginalItems.get(i).bIsSelected = true;
					countSelectedItems++;
				}
			}
			setButtonText();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		showOptionsDialog();
	}

	private void showOptionsDialog() {
		try {
			if (arrayListOriginalItems.size() == 0) {
				return;
			}

			AdapterView.OnItemClickListener onItemClicked = new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					ClassScreenItemMultiSelect itemMultiSelect = listAdapterMultiSelect.getItem(position);
					itemMultiSelect.bIsSelected = !itemMultiSelect.bIsSelected;
					if (itemMultiSelect.bIsSelected) {
						countSelectedItems++;
					} else {
						countSelectedItems--;
					}
					((CheckedTextView) listAdapterMultiSelect.getView(position, view, null)).setChecked(itemMultiSelect.bIsSelected);
				}
			};

			DialogInterface.OnClickListener onOKClickListener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					setButtonText();
				}
			};

			DialogInterface.OnShowListener onShowListener = new DialogInterface.OnShowListener() {

				@Override
				public void onShow(DialogInterface dialog) {
					measureDialogHeight();
				}
			};

			listViewResults = new ListView(MainContainer.mContext);
			listAdapterMultiSelect = new ListAdapterMultiSelect();
			listViewResults.setAdapter(listAdapterMultiSelect);
			listViewResults.setScrollbarFadingEnabled(false);
			listViewResults.setOnItemClickListener(onItemClicked);

			AlertDialog.Builder builder = new AlertDialog.Builder(MainContainer.mContext);
			builder.setTitle("Select " + getHint().toString().trim());
			builder.setView(listViewResults);
			builder.setCancelable(false);
			builder.setPositiveButton("OK", onOKClickListener);
			AlertDialog alertDialog = builder.create();
			alertDialog.setOnShowListener(onShowListener);
			alertDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ListAdapterMultiSelect extends ArrayAdapter<ClassScreenItemMultiSelect> {

		public ListAdapterMultiSelect() {
			super(MainContainer.mContext, 0, arrayListOriginalItems);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			if (convertView == null) {
				viewHolder = new ViewHolder();
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
				viewHolder.checkedTextView = (CheckedTextView) convertView;
				viewHolder.checkedTextView.setTextAppearance(MainContainer.mContext, android.R.style.TextAppearance_Medium);
				// viewHolder.checkedTextView.setTextColor(colorText);
				// viewHolder.checkedTextView.setBackgroundColor(colorBackground);
				viewHolder.checkedTextView.setSingleLine(false);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.checkedTextView = (CheckedTextView) convertView;
			ClassScreenItemMultiSelect itemMultiSelect = getItem(position);
			viewHolder.checkedTextView.setText(itemMultiSelect.item.item_name);
			viewHolder.checkedTextView.setChecked(itemMultiSelect.bIsSelected);
			return convertView;
		}

		private class ViewHolder {

			private CheckedTextView checkedTextView;
		}
	}
}