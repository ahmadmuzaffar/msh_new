package pk.gov.pitb.mea.msh.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.helpers.AlertDialogs;


/**
 * @author REHAN created on Jun 1, 2015
 */
public class MultiSelectDialog extends Button implements View.OnClickListener {

    private OnItemClickedListener onItemClickedListener;
    private OnDropDownDismissedListener onDropDownDismissedListener;

    private ArrayList<ItemMultiSelect> arrayListOriginalItems;
    private int countSelectedItems;

    private ListView listViewResults;
    private EditText editText;
    private ListAdapterMultiSelect listAdapterMultiSelect;

    private String dialogTitle;
    private String dialogButtonText;
    private boolean bIsCancelable;
    private int drawableCheckBoxDropdown;
    private Context context;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    public MultiSelectDialog(Context context) {
        super(context);
        this.context = context;
        initDefault();
    }

    public MultiSelectDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initDefault();
    }

    public MultiSelectDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initDefault();
    }

    private void initDefault() {
        dialogTitle = "";
        dialogButtonText = "";
        bIsCancelable = false;
        drawableCheckBoxDropdown = -1;

    }

    public void setCountSelectedItems(int countSelectedItems) {
        this.countSelectedItems = countSelectedItems;
    }

    @Override
    public CharSequence getHint() {
        CharSequence cs = super.getHint();
        return cs == null ? "" : cs;
    }

    public ArrayList<ItemMultiSelect> getArrayListOriginalItems() {
        return arrayListOriginalItems;
    }

    @Override
    public void onClick(View v) {
        showOptionsDialog();
    }

    public void initData(ArrayList<ItemMultiSelect> objects) {
        initData(objects, null, null);
    }

    public void initData(ArrayList<ItemMultiSelect> objects, OnItemClickedListener itemSelectedListener) {
        initData(objects, itemSelectedListener, null);
    }

    public void initData(ArrayList<ItemMultiSelect> objects, OnDropDownDismissedListener dismissedListener) {
        initData(objects, null, dismissedListener);
    }

    public void initData(ArrayList<ItemMultiSelect> objects, OnItemClickedListener itemSelectedListener, OnDropDownDismissedListener dismissedListener) {
        try {
            onItemClickedListener = itemSelectedListener;
            onDropDownDismissedListener = dismissedListener;
            repopulateData(objects);
            setOnClickListener(this);
            setButtonText();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void repopulateData(ArrayList<ItemMultiSelect> objects) {
        try {
            arrayListOriginalItems = objects;
            countSelectedItems = 0;
            for (int i = 0; i < arrayListOriginalItems.size(); i++)
                if (arrayListOriginalItems.get(i).isSelected())
                    countSelectedItems++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDialogTitle(String title) {
        dialogTitle = title;
    }

    public void setDialogButtonText(String text) {
        dialogButtonText = text;
    }

    public void setDialogCancelable(boolean isCancelable) {
        bIsCancelable = isCancelable;
    }

    public void setDrawableCheckBoxDropdown(int resourceIDDrawable) {
        drawableCheckBoxDropdown = resourceIDDrawable;
    }

    public boolean isAllItemsSelected() {
        return countSelectedItems == arrayListOriginalItems.size();
    }

    public int getSelectedItemsCount() {
        return countSelectedItems;
    }

    public ArrayList<ItemMultiSelect> getSelectedItemsList() {
        ArrayList<ItemMultiSelect> arrayListSelectedItems = new ArrayList<ItemMultiSelect>();
        try {
            for (ItemMultiSelect item : arrayListOriginalItems) {
                if (item.isSelected()) {
                    if (item.toString().toLowerCase().contains("other")) {
                        item.setObject(editText.getText().toString().trim());
                    }
                    arrayListSelectedItems.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayListSelectedItems;
    }

    public String getSelectedItemsNames() {

        String selectedNames = null;
        try {
            for (ItemMultiSelect item : arrayListOriginalItems) {
                if (item.isSelected()) {
                    if (selectedNames == null) {
                        if (item.getObject() instanceof String) {
                            if (item.toString().toLowerCase().contains("other")) {
                                item.setObject(editText.getText().toString().trim());
                            }
                            selectedNames = (item.getObject());
                        }
                    } else {
                        if (item.getObject() instanceof String) {
                            if (item.toString().toLowerCase().contains("other")) {
                                item.setObject(editText.getText().toString().trim());
                            }
                            selectedNames = selectedNames + "," + (item.getObject());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return selectedNames;
    }

//    public String getSelectedItemsStrings() {
//
//        String selectedNames = null;
//        try {
//            for (ItemMultiSelect item : arrayListOriginalItems) {
//                if (item.isSelected()) {
//                    if (selectedNames == null) {
//                        if (item.getObject() instanceof SpinnerClassItem)
//                            selectedNames = new Gson().toJson(item.getObject());
//                    } else {
//                        if (item.getObject() instanceof SpinnerClassItem) {
//                            selectedNames = selectedNames + "|" + new Gson().toJson(item.getObject());
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return selectedNames;
//    }
//
//    public String getSelectedItemsIDs() {
//        String selectedIDs = null;
//        try {
//            for (ItemMultiSelect item : arrayListOriginalItems) {
//                if (item.isSelected()) {
//                    if (selectedIDs == null) {
//                        if (item.getObject() instanceof SpinnerClassItem)
//                            selectedIDs = String.valueOf(((SpinnerClassItem) item.getObject()).getObjectId());
//                    } else {
//                        if (item.getObject() instanceof SpinnerClassItem) {
//                            selectedIDs = selectedIDs + "|" + String.valueOf(((SpinnerClassItem) item.getObject()).getObjectId());
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return selectedIDs;
//    }

    private boolean shouldShowEditText;

    private void showOptionsDialog() {
        try {
            if (arrayListOriginalItems.size() == 0) {
                AlertDialogs.getInstance().showDialogOK("Error Showing Data", "No Data to Display", null, true);
                return;
            }

            AdapterView.OnItemClickListener onItemClicked = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    ItemMultiSelect itemMultiSelect = listAdapterMultiSelect.getItem(position);
                    itemMultiSelect.toggleSelection();
                    if (itemMultiSelect.isSelected()) {
                        countSelectedItems++;
                        if (itemMultiSelect.toString().toLowerCase().contains("other")) {
                            editText.setVisibility(VISIBLE);
                            editText.requestFocus();
                            shouldShowEditText = true;
                        }
                    } else {
                        countSelectedItems--;
                        if (itemMultiSelect.toString().toLowerCase().contains("other")) {
                            editText.setVisibility(GONE);
                            shouldShowEditText = false;
                        }
                    }
                    ((CheckedTextView) listAdapterMultiSelect.getView(position, view, null)).setChecked(itemMultiSelect.isSelected());
                    if (onItemClickedListener != null) {
                        onItemClickedListener.onItemClicked();
                    }
                }
            };

            final View.OnClickListener onOKClickListener = new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (editText.getVisibility() == VISIBLE && editText.getText().toString().trim().equals("")) {
                        Toast.makeText(context, "Fill other field", Toast.LENGTH_SHORT).show();
                    } else
                        alertDialog.dismiss();
                }
            };

            DialogInterface.OnShowListener onShowListener = new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
//                    measureDialogHeight();
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(onOKClickListener);
                }
            };

            DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    setButtonText();
                    if (onDropDownDismissedListener != null) {
                        onDropDownDismissedListener.onDropDownDismissed();
                    }
                }
            };

            shouldShowEditText = false;

            View dialogView = LayoutInflater.from(context).inflate(R.layout.multiselect_dialog, null);

            listViewResults = (ListView) dialogView.findViewById(R.id.listview);
            listAdapterMultiSelect = new ListAdapterMultiSelect();
            listViewResults.setAdapter(listAdapterMultiSelect);
            listViewResults.setScrollbarFadingEnabled(false);
            listViewResults.setOnItemClickListener(onItemClicked);

            editText = (EditText) dialogView.findViewById(R.id.edittext_other);
            editText.setTextColor(Color.BLACK);

            builder = new AlertDialog.Builder(context);
            if (dialogTitle != null) {
                builder.setTitle(dialogTitle.length() > 0 ? dialogTitle : "Select " + getHint().toString().trim() + "(s)");
            }
            if (dialogButtonText != null) {
                builder.setPositiveButton(dialogButtonText.length() > 0 ? dialogButtonText : "OK", null);
            } else if (!bIsCancelable) {
                builder.setPositiveButton("OK", null);
            }

            builder.setView(dialogView);
            builder.setCancelable(bIsCancelable);
            alertDialog = builder.create();
            alertDialog.setOnShowListener(onShowListener);
            alertDialog.setOnDismissListener(onDismissListener);
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setButtonText() {
        String hint = getHint().toString().trim();
        if (countSelectedItems == 0) {
            setText("Select " + hint + "(s)");
        } else if (countSelectedItems == arrayListOriginalItems.size()) {
            setText("All " + hint + "s Selected");
        } else if (countSelectedItems == 1) {
            setText(countSelectedItems + " " + hint + " Selected");
        } else {
            setText(countSelectedItems + " " + hint + "s Selected");
        }
    }

    private void measureDialogHeight() {
        try {
            if (listAdapterMultiSelect != null) {
                LinearLayout.LayoutParams lpListView;
                View child = listAdapterMultiSelect.getView(0, null, listViewResults);
                child.measure(0, 0);
//                int itemHeight = child.getMeasuredHeight();
//                int maxCount = (int) ((Globals.getInstance().mScreenHeight * 0.4) / itemHeight);
//                if (listAdapterMultiSelect.getCount() > maxCount) {
//                    lpListView = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) ((maxCount + 0.5) * itemHeight));
//                } else {
                lpListView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            }
                listViewResults.setLayoutParams(lpListView);

//                LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                editTextParams.setMargins(0, 5, 0, 5);
//                editText.setLayoutParams(editTextParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract static class OnItemClickedListener {

        public abstract void onItemClicked();
    }

    public abstract static class OnDropDownDismissedListener {

        public abstract void onDropDownDismissed();
    }

    private class ListAdapterMultiSelect extends ArrayAdapter<ItemMultiSelect> {

        public ListAdapterMultiSelect() {
            super(context, 0, arrayListOriginalItems);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
                viewHolder.checkedTextView = (CheckedTextView) convertView;
                viewHolder.checkedTextView.setTextColor(Color.BLACK);
                viewHolder.checkedTextView.setTextAppearance(context, android.R.style.TextAppearance_Medium);
                viewHolder.checkedTextView.setSingleLine(false);
                if (drawableCheckBoxDropdown != -1) {
                    viewHolder.checkedTextView.setCheckMarkDrawable(drawableCheckBoxDropdown);
                }
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.checkedTextView = (CheckedTextView) convertView;
            ItemMultiSelect itemMultiSelect = getItem(position);
            viewHolder.checkedTextView.setText(itemMultiSelect.toString());
            viewHolder.checkedTextView.setChecked(itemMultiSelect.isSelected());

            if (!shouldShowEditText)
                if (viewHolder.checkedTextView.getText().toString().toLowerCase().contains("other") && viewHolder.checkedTextView.isChecked()) {
                    editText.setVisibility(VISIBLE);
                    editText.requestFocus();
                    shouldShowEditText = true;
                } else {
                    editText.setVisibility(GONE);
                    shouldShowEditText = false;
                }


            return convertView;
        }

        private class ViewHolder {
            private CheckedTextView checkedTextView;
        }
    }

    public static class ItemMultiSelect {

        private String object;
        private boolean isSelected;

        public ItemMultiSelect() {
            this.object = null;
            this.isSelected = false;
        }

        public ItemMultiSelect(String object) {
            this.object = object;
            this.isSelected = false;
        }

        public ItemMultiSelect(String object, boolean isSelected) {
            this.object = object;
            this.isSelected = isSelected;
        }

        public String getObjectID() {
            try {
                if (object != null) {
                    Method method = object.getClass().getMethod("getID");
                    return (String) method.invoke(object);
                }
                return "";
            } catch (Exception e) {
                e.printStackTrace();
                return "-1";
            }
        }

        @Override
        public String toString() {
            return object.toString();
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public void toggleSelection() {
            isSelected = !isSelected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }
    }
}