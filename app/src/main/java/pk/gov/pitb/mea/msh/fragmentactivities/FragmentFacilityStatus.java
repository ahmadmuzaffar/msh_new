package pk.gov.pitb.mea.msh.fragmentactivities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import org.json.JSONException;
import org.json.JSONObject;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;

@SuppressLint("DefaultLocale")
public class FragmentFacilityStatus extends Fragment implements InterfaceFragmentCallBack {

	private View parentView;
	private RadioGroup radioGroupFacilityStatus;
	private RadioButton radioButtonSchoolStatusOpen;
	private RadioButton radioButtonSchoolStatusClosed;

	@Override
	public void parseObject() {
		try {
			JSONObject jsonObjectMain = new JSONObject();
			if (radioGroupFacilityStatus.getCheckedRadioButtonId() == radioButtonSchoolStatusOpen.getId()) {
				jsonObjectMain.put("facility_status", "open");
			} else if (radioGroupFacilityStatus.getCheckedRadioButtonId() == radioButtonSchoolStatusClosed.getId()) {
				jsonObjectMain.put("facility_status", "closed");
			} else {
				jsonObjectMain.put("facility_status", "");
			}

			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_FACILITY_STATUS);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_FACILITY_STATUS, jsonObjectMain);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_FACILITY_STATUS)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_FACILITY_STATUS);
				String facilityStatus = jsonObject.getString("facility_status");
				if (facilityStatus.equals("open")){
					radioButtonSchoolStatusOpen.setChecked(true);
				}
				else if (facilityStatus.equals("closed")){
					radioButtonSchoolStatusClosed.setChecked(true);
				}
			}
		}
		catch (Exception e){

		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			if (parentView == null) {
				parentView = inflater.inflate(R.layout.fragment_facility_status, container, false);
				generateBody();
				loadSavedData();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return parentView;
	}

	private void generateBody() {
		try {
			// linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			radioGroupFacilityStatus = (RadioGroup) parentView.findViewById(R.id.radiogroup_status);
			radioButtonSchoolStatusOpen = (RadioButton) parentView.findViewById(R.id.radiobutton_status_open);
			radioButtonSchoolStatusClosed = (RadioButton) parentView.findViewById(R.id.radiobutton_status_closed);

			radioGroupFacilityStatus.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (group.getCheckedRadioButtonId() == radioButtonSchoolStatusClosed.getId()) {
						MainContainer.bIsOpen = false;
						MainContainer.activityCallBack.changeButtonNextState(false);
						MainContainer.activityCallBack.changeButtonSubmitState(true);
					} else {
						MainContainer.bIsOpen = true;
						MainContainer.activityCallBack.changeButtonSubmitState(false);
						MainContainer.activityCallBack.changeButtonNextState(true);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			if (radioGroupFacilityStatus.getCheckedRadioButtonId() == -1) {
				errorMessage = "Please mark Facility open or closed to continue";
				return false;
			}
			if (!bIsValid) {
				errorMessage = "Please fill all Facility details to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}