package pk.gov.pitb.mea.msh.fragmentactivities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pk.gov.pitb.mea.msh.R;
import pk.gov.pitb.mea.msh.handlers.InterfaceFragmentCallBack;
import pk.gov.pitb.mea.msh.helpers.Constants;
import pk.gov.pitb.mea.msh.helpers.MainContainer;
import pk.gov.pitb.mea.msh.helpers.UIHelper;
import pk.gov.pitb.mea.msh.models.ClassScreenData;
import pk.gov.pitb.mea.msh.models.ClassScreenItem;
import pk.gov.pitb.mea.msh.views.CustomEditText;
import pk.gov.pitb.mea.msh.views.CustomToggleButtonSingleState;

public class FragmentOutlookUtilities extends Fragment implements InterfaceFragmentCallBack {

	private View parentView;
	private ArrayList<ClassScreenItem> arrayListLocations;
	private ArrayList<ClassScreenItem> arrayListCleanlinessCondition;
	private ArrayList<ClassScreenItem> arrayListCleanlinessDisabledCondition;

	private LinearLayout linearLayoutMain;
	private TextView textViewCleanlinessFacility;
	private TableLayout tableLayoutOutlook;

	@Override
	public void parseObject() {
		try {
			JSONArray jsonArrayLocation = new JSONArray();
			for (int i = 1; i < tableLayoutOutlook.getChildCount(); i++) {
				View child = tableLayoutOutlook.getChildAt(i);
				ClassScreenItem object = arrayListLocations.get(i - 1);
				if (child instanceof TableRow) {
					JSONObject jsonObjectChild = new JSONObject();

					TableRow tableRow = (TableRow) child;
					ToggleButton good = (ToggleButton) tableRow.getChildAt(2);
					ToggleButton average = (ToggleButton) tableRow.getChildAt(3);
					ToggleButton poor = (ToggleButton) tableRow.getChildAt(4);

					String condition = "";
					if (good.isChecked()) {
						condition = arrayListCleanlinessCondition.get(0).item_id;
					} else if (average.isChecked()) {
						condition = arrayListCleanlinessCondition.get(1).item_id;
					} else if (poor.isChecked()) {
						condition = arrayListCleanlinessCondition.get(2).item_id;
					}

					jsonObjectChild.put("location_id", object.item_id);
					jsonObjectChild.put("location", object.item_name);
					jsonObjectChild.put("oc_id", condition);

					jsonArrayLocation.put(jsonObjectChild);
				}
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("locations", jsonArrayLocation);
			MainContainer.mJsonObjectFormData.remove(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES);
			MainContainer.mJsonObjectFormData.put(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadSavedData(){
		try {
			if (MainContainer.mJsonObjectFormData.has(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES)){
				JSONObject jsonObject = MainContainer.mJsonObjectFormData.getJSONObject(Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES);
				JSONArray jsonArray = jsonObject.getJSONArray("locations");
				int arrayCount = 0;
				for (int i = 1; i < tableLayoutOutlook.getChildCount(); i++) {
					View child = tableLayoutOutlook.getChildAt(i);
					if (child instanceof TableRow) {
						JSONObject jsonObjectChild = jsonArray.getJSONObject(arrayCount++);

						TableRow tableRow = (TableRow) child;
						ToggleButton good = (ToggleButton) tableRow.getChildAt(2);
						ToggleButton average = (ToggleButton) tableRow.getChildAt(3);
						ToggleButton poor = (ToggleButton) tableRow.getChildAt(4);

						String condition = jsonObjectChild.getString("oc_id");
						if (condition.equals(arrayListCleanlinessCondition.get(0).item_id)) {
							good.setChecked(true);
						} else if (condition.equals(arrayListCleanlinessCondition.get(1).item_id)) {
							average.setChecked(true);
						} else if (condition.equals(arrayListCleanlinessCondition.get(2).item_id)) {
							poor.setChecked(true);
						}
					}
				}
			}
		}
		catch (Exception e){

		}
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (parentView == null) {
			parentView = inflater.inflate(R.layout.fragment_default, container, false);
			initializeData();
			generateBody();
			loadSavedData();
		}
		return parentView;
	}

	private void initializeData() {
		try {
			ClassScreenData objectScreenData = new ClassScreenData();
			objectScreenData.setDefaultValues();
			objectScreenData.screen_id = Constants.DB_SCREEN_ID_OUTLOOK_UTILITIES;
			objectScreenData.screen_name = Constants.DB_SCREEN_NAME_OUTLOOK_UTILITIES;

			objectScreenData.section_id = "1";
			objectScreenData.section_name = "section1";
			objectScreenData.section_value = "fields";
			objectScreenData = MainContainer.mDbAdapter.selectScreenData(objectScreenData);
			arrayListLocations = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			objectScreenData.section_value = "disabled_fields";
			arrayListCleanlinessDisabledCondition = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);

			objectScreenData.section_value = "dropdown";
			arrayListCleanlinessCondition = MainContainer.mDbAdapter.selectScreenItems(objectScreenData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateBody() {
		try {
			linearLayoutMain = (LinearLayout) parentView.findViewById(R.id.linearlayout_main);
			textViewCleanlinessFacility = new TextView(MainContainer.mContext);
			tableLayoutOutlook = new TableLayout(MainContainer.mContext);

			linearLayoutMain.addView(textViewCleanlinessFacility);
			linearLayoutMain.addView(tableLayoutOutlook);

			TableRow.LayoutParams[] paramsArrayTableRowColumnsCleanliness = new TableRow.LayoutParams[5];
			paramsArrayTableRowColumnsCleanliness[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsCleanliness[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.26), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[3] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsCleanliness[4] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.2), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumnsCleanliness.length; i++) {
				paramsArrayTableRowColumnsCleanliness[i].setMargins(2, 0, 0, 0);
			}

			TableRow.LayoutParams[] paramsArrayTableRowColumnsGeneral = new TableRow.LayoutParams[3];
			paramsArrayTableRowColumnsGeneral[0] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.1), TableRow.LayoutParams.MATCH_PARENT);
			paramsArrayTableRowColumnsGeneral[1] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.46), (int) (MainContainer.mScreenWidth * 0.08));
			paramsArrayTableRowColumnsGeneral[2] = new TableRow.LayoutParams((int) (MainContainer.mScreenWidth * 0.4), (int) (MainContainer.mScreenWidth * 0.08));
			for (int i = 1; i < paramsArrayTableRowColumnsGeneral.length; i++) {
				paramsArrayTableRowColumnsGeneral[i].setMargins(2, 0, 0, 0);
			}

			UIHelper uiContainer = new UIHelper();
			uiContainer.generateTableLabel(textViewCleanlinessFacility, "Outlook of Facility");
			uiContainer.generateTableHeader(tableLayoutOutlook, paramsArrayTableRowColumnsCleanliness, new String[] { "No.", "Location", arrayListCleanlinessCondition.get(0).item_name,
					arrayListCleanlinessCondition.get(1).item_name, arrayListCleanlinessCondition.get(2).item_name });
			generateTableCleanlinessFacility(paramsArrayTableRowColumnsCleanliness);

			if (tableLayoutOutlook.getChildCount() > 1) {
				textViewCleanlinessFacility.setVisibility(View.VISIBLE);
				tableLayoutOutlook.setVisibility(View.VISIBLE);
			} else {
				textViewCleanlinessFacility.setVisibility(View.GONE);
				tableLayoutOutlook.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getDisabledID(String id) {
		for (ClassScreenItem item : arrayListCleanlinessDisabledCondition) {
			if (item.item_id.equals(id)) {
				return item.item_name;
			}
		}
		return "";
	}

	private void generateTableCleanlinessFacility(TableRow.LayoutParams[] paramsArrayTableRowColumns) {
		int serialNum = 0;
		int colorBgTableRow = MainContainer.mContext.getResources().getColor(R.color.row_background);
		for (ClassScreenItem item : arrayListLocations) {
			boolean bIsOdd = serialNum % 2 == 0;
			serialNum++;

			TableRow tableRow = new TableRow(MainContainer.mContext);
			CustomEditText editTextSerialNum = new CustomEditText(bIsOdd, "" + serialNum);
			CustomEditText editTextLocation = new CustomEditText(bIsOdd, item.item_name);
			CustomToggleButtonSingleState toggleButtonGood = new CustomToggleButtonSingleState(bIsOdd, 1);
			CustomToggleButtonSingleState toggleButtonAverage = new CustomToggleButtonSingleState(bIsOdd, 1);
			CustomToggleButtonSingleState toggleButtonPoor = new CustomToggleButtonSingleState(bIsOdd, 1);

			editTextSerialNum.customiseEditText(paramsArrayTableRowColumns[0]);
			editTextLocation.customiseEditText(paramsArrayTableRowColumns[1]);
			toggleButtonGood.customiseToggleButton(paramsArrayTableRowColumns[2]);
			toggleButtonAverage.customiseToggleButton(paramsArrayTableRowColumns[3]);
			toggleButtonPoor.customiseToggleButton(paramsArrayTableRowColumns[4]);

			tableRow.setBackgroundColor(colorBgTableRow);
			tableRow.addView(editTextSerialNum);
			tableRow.addView(editTextLocation);
			tableRow.addView(toggleButtonGood);
			tableRow.addView(toggleButtonAverage);
			tableRow.addView(toggleButtonPoor);
			tableLayoutOutlook.addView(tableRow);

			String disabledID = getDisabledID(item.item_id);
			if (disabledID.length() > 0) {
				if (disabledID.equals(arrayListCleanlinessCondition.get(0).item_id)) {
					toggleButtonGood.setClickable(false);
					toggleButtonGood.setEnabled(false);
					toggleButtonGood.setOnClickListener(null);
					toggleButtonGood.setOnLongClickListener(null);
					toggleButtonGood.setBackgroundColor(getActivity().getResources().getColor(R.color.row_body_background_disabled));
				} else if (disabledID.equals(arrayListCleanlinessCondition.get(1).item_id)) {
					toggleButtonAverage.setClickable(false);
					toggleButtonAverage.setEnabled(false);
					toggleButtonAverage.setOnClickListener(null);
					toggleButtonAverage.setOnLongClickListener(null);
					toggleButtonAverage.setBackgroundColor(getActivity().getResources().getColor(R.color.row_body_background_disabled));
				} else if (disabledID.equals(arrayListCleanlinessCondition.get(2).item_id)) {
					toggleButtonPoor.setClickable(false);
					toggleButtonPoor.setEnabled(false);
					toggleButtonPoor.setOnClickListener(null);
					toggleButtonPoor.setOnLongClickListener(null);
					toggleButtonPoor.setBackgroundColor(getActivity().getResources().getColor(R.color.row_body_background_disabled));
				}
			}
		}
	}

	@Override
	public void onFragmentShown() {
	}

	private String errorMessage;

	@Override
	public String onFragmentChanged(int previousPosition) {
		errorMessage = null;
		isFormValid();
		return errorMessage;
	}

	@Override
	public boolean isFormValid() {
		try {
			boolean bIsValid = true;
			for (int i = 1; i < tableLayoutOutlook.getChildCount(); i++) {
				View child = tableLayoutOutlook.getChildAt(i);
				if (child instanceof TableRow) {
					TableRow tableRow = (TableRow) child;
					ToggleButton good = (ToggleButton) tableRow.getChildAt(2);
					ToggleButton average = (ToggleButton) tableRow.getChildAt(3);
					ToggleButton poor = (ToggleButton) tableRow.getChildAt(4);

					if (!good.isChecked() && !average.isChecked() && !poor.isChecked()) {
						bIsValid = false;
					}
				}
			}
			if (!bIsValid) {
				errorMessage = "Please mark all Facilities condition to continue";
			}
			return bIsValid;
		} catch (Exception e) {
			e.printStackTrace();
			MainContainer.showMessage("Error Processing Your Request");
			return false;
		}
	}
}