package pk.gov.pitb.mea.msh.models;

public class ClassIncharge {
    public String incharge_name, incharge_designation, incharge_mobile, incharge_cnic;
    
    public ClassIncharge() {
	reset();
    }
    
    public void reset() {
	incharge_name = "";
	incharge_designation = "";
	incharge_mobile = "";
	incharge_cnic = "";
    }
}