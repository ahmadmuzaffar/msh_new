package pk.gov.pitb.mea.msh.receiver;

import pk.gov.pitb.mea.msh.database.SharedPreferencesEditor;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverOnReboot extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferencesEditor spEditor = new SharedPreferencesEditor(context);
		spEditor.resetOnReboot();
	}
}
// @formatter:off
// ---------------------------------------- USAGE ----------------------------------------
// <receiver android:name="pk.gov.pitb.appsupport_v1.receiver.ReceiverOnReboot" >
// <intent-filter>
// <action android:name="android.intent.action.BOOT_COMPLETED" />
// </intent-filter>
// </receiver>
