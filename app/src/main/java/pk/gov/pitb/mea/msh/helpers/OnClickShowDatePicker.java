package pk.gov.pitb.mea.msh.helpers;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OnClickShowDatePicker implements View.OnClickListener {

    private final String FORMAT_DATE = "dd-MM-yyyy";
    private Context context;
    private EditText editText;
    private DatePicker datePicker;
    private String title;

    public OnClickShowDatePicker(Context context, EditText editText, String title) {
        this.editText = editText;
        this.context = context;
        this.title = title;
        this.editText.setClickable(false);
        this.editText.setFocusable(false);
        this.editText.setFocusableInTouchMode(false);
    }

    public void onClick(View v) {
        showDateDialog();
    }

    private void showDateDialog() {
        try {
            String previousDate = editText.getText().toString();
            Calendar calendar = Calendar.getInstance();
            if (previousDate.length() > 0) {
                calendar.setTime((new SimpleDateFormat(FORMAT_DATE, Locale.ENGLISH)).parse(previousDate));
            }
            datePicker = new DatePicker(context);
            datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setView(datePicker);
            builder.setPositiveButton("OK", new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE, Locale.ENGLISH);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, datePicker.getYear());
                    calendar.set(Calendar.MONTH, datePicker.getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                    String date = dateFormat.format(calendar.getTime());
                    editText.setText(date);
                    editText.setError(null);
                }
            });
            builder.setNegativeButton("Cancel", new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create();
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}