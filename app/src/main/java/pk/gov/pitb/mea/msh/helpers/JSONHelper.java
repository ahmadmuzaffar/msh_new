package pk.gov.pitb.mea.msh.helpers;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import pk.gov.pitb.mea.msh.models.ClassValue;

public class JSONHelper {

	public boolean getArrayFromJSON(ArrayList<ClassValue> arrayListValues, String json, String key_id, String key_value) {
		try {
			JSONArray jsonArray = new JSONArray(json);
			arrayListValues.clear();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				ClassValue objectValue = new ClassValue();
				objectValue.id = jsonObject.has(key_id) ? jsonObject.getString(key_id) : "";
				objectValue.value = jsonObject.has(key_value) ? jsonObject.getString(key_value) : "";
				arrayListValues.add(objectValue);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}